# -*- coding: utf-8 -*-
#
# (c) 2023 CSIRO Space and Astronomy.
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement
# See LICENSE.txt for more info.
"""
FPGA Manager for PSS Personality

This file contains the FpgaMgrPss class which controls the operation of the
PSS FPGA - starting/stopping scans, managing delay polynomials
"""
import copy
import os
import struct
import time
from dataclasses import dataclass

from ska_low_cbf_proc.icl.correlator.constants import SKA_COARSE_SPACING_GHZ
from ska_low_cbf_proc.icl.fpga_mgr import FpgaMgr
from ska_low_cbf_proc.icl.pss.pss_calcs import (
    mhz_to_first_freq_tbls,
    pss_pktzr_beam_nos,
    pss_pktzr_config,
    pss_pktzr_first_chans,
    pss_pktzr_freq_config,
    pss_vct,
)
from ska_low_cbf_proc.icl.pss.pss_fpga import PssFpga
from ska_low_cbf_proc.icl.pss_intl_iface import (
    SigProcParams,
    get_internal_scan_id,
    get_internal_subarray_id,
    get_pipeline_params,
    get_station_beam_pss_info,
    get_subarray_lists,
)
from ska_low_cbf_proc.icl.subscription_monitor import SubscriptionMonitor
from ska_low_cbf_proc.processor.delay_subscription import DelayPolySubscriber


@dataclass
class PssValidity:
    """
    Structure holding values used in FPGA regs for PSS polynomial validity
    """

    start_valid: int
    """Time at which item becomes valid. Units are (currently 53msec) frames"""
    valid_length: int
    """How long item is valid. Units are frames"""
    ns_offset: int
    """True start-of-validity to start_valid in nanoseconds (non-negative)"""

    def to_words(self) -> list[int]:
        """Get fpga representation of validity in words for the registers"""
        return [
            self.start_valid & 0x0_FFFF_FFFF,
            self.start_valid >> 32 & 0x0_FFFF,
            self.valid_length & 0x0_FFFF_FFFF,
            self.ns_offset & 0x0_FFFF_FFFF,
        ]


@dataclass
class JonesValidity:
    """
    Structure holding values used in FPGA regs for Jones validity
    """

    start_valid: int
    """Time at which item becomes valid. Units are (currently 53msec) frames"""
    valid_length: int
    """How long item is valid. Units are frames"""

    def to_words(self) -> list[int]:
        """Get fpga representation of validity in words for the registers"""
        return [
            self.start_valid & 0x0_FFFF_FFFF,
            self.start_valid >> 32 & 0x0_FFFF,
            self.valid_length & 0x0_FFFF_FFFF,
        ]


@dataclass
class PssPolyCoeffs:
    """
    Structure holding delay polynomial coefficients for use in PSS FPGA regs
    """

    coeffs: list[float]
    """six delay polynomial coefficients in order [t^0, t^1, ... t^5]"""

    def to_words(self) -> list[int]:
        """Get FPGA representation of PSS delay polynomial coefficients"""
        as_bytes = struct.pack("<ffffff", *self.coeffs)
        as_ints = struct.unpack("IIIIII", as_bytes)
        return as_ints


class PipelineState:
    """
    Class to hold parameters and state of one signal-processing pipeline
    """

    def __init__(self, params: SigProcParams):
        self.params = params  # processing parameters

        self.stn_bm_dly_sub = None  # stn-beam delay subscription
        self.stn_bm_dly_sub_name = ""  # name of dly sub attribute
        self.was_poly_upated = False
        self.beam_polys_valid = False  # is stn-beam poly valid

        self.pss_bm_subs = []  # list of pss beam delay subscriptions
        self.pss_bm_names = []  # Tango URIs for PSS beam delays
        self.was_pss_updated = []
        self.pss_first_valid_blk = [None, None]  # common PSS starts
        self.pss_bm_polys_valid = False  # are PSS beam polys valid

        self.prev_vchan_pkt_nos = None  # pkt nums in used vchans
        self.highest_pkt_no = 0  # highest pkt number found so far
        self.highest_pkt_secs = 0  # seconds equivalent to pkt number
        self.highest_blk_no = 0  # 53ms filterbank block no eqiv to pkt no
        self.pkt_spread = 0  # earliest-to-latest spread of received pkt nos

        self.ct1_hbm_blk_no = 0  # block (53ms data) being written to CT1 HBM
        self.ct2_hbm_blk_nos = [0, 0]  # blocks in CT2 HBM
        self.pss_poly_sovs = [None, None]  # start-of-valid of polys in Regs

        self.vct_freq_stn = None  # (freq, chan) in vct ordering
        self.packetiser_enabled = False  # is FPGA packetiser enabled flag

    @property
    def subarray_id(self) -> int:
        """Return subarray ID this pipeline is processing"""
        return self.params.subarray_id

    @property
    def beam_id(self) -> int:
        """Return beam ID this pipeline is processing"""
        return self.params.stn_bm_id


class FpgaMgrPss(FpgaMgr):
    """
    A class for PSS FPGAs that monitors and programs FPGA registers

    This class translates parameters specified by the Allocator into register
    updates. It also periodically monitors FPGA registers that indicate the
    quality of the ongoing operation of the PSS FPGA, and updates delays
    """

    PERIODIC_PRINT_SECS = 30.0
    SECS_PER_PKT = 2211840e-9  # packets have 2048 samples @ 1080ns/sample
    PKTS_PER_FILTER_FRAME = (
        24  # FIXME filterbank uses 24pkts (53.084msec) at a time
    )
    FPS_NUMER = 390625  # numerator of PSS filterbank frames per sec fraction
    FPS_DENOM = 20736  # denominator of PSS filterbank frames per sec fraction
    POLY_IS_VALID_BIT = 0x100_0000_0000
    CT1_LAG = 2  # CT1 processing lag behind CT1 HBM frame count

    def __init__(
        self,
        fpga: PssFpga,
        fail_cbk,
        subarray_stats_cbk,
        io_stats_cbk,
        subscr_stats_cbk,
        logger,
    ):
        super().__init__(fpga, fail_cbk, logger)
        self.logger.info("Initialising PSS FPGA interactions")
        self.fpga = fpga

        # Check firmware is recent enough for this code to use
        self.fpga._check_fw(" PSS", ">=0.0.4")
        # Additional check because fw version was not updated but fw regs were
        assert (
            self.fpga.system.args_map_build.value >= 0x24100100
        ), "PSS Firmware not supported. Use VER>=0.0.4 dated >= 2024-10-01"

        # override periodic timer interval for this specific FPGA personality
        self._periodic_secs = 4.0  # faster than 10-sec delay poly updates
        self._subarray_stats_cbk = subarray_stats_cbk
        self._io_stats_cbk = io_stats_cbk

        self._seconds = FpgaMgrPss.PERIODIC_PRINT_SECS
        self._subs_scanning = []  # list of scanning subarray_ids

        num_inst = self.fpga.num_pipelines
        self.logger.info("%d signal processing instances", num_inst)
        self._pipestates = [None] * num_inst
        self._subscr_stats_cbk = subscr_stats_cbk
        self._subscr_monitor = SubscriptionMonitor()

        # Explicitly reset all pipelines in case we've inherited an FPGA
        # image that wasn't actually reset by reloading from the bitfile
        for idx in range(0, self.fpga.num_pipelines):
            pipe = self.fpga.pipelines[idx]
            # FIXME PSS HBM reset not connected in FPGA (2024-08-28)
            pipe["ct2_pss"].hbm_reset_and_wait_for_done()
            pipe["ingest"].hbm_reset_and_wait_for_done(0.005, 100)
            pipe["ct2_pss"].set_num_beams(0)
            pipe["packetiser_psr"].disable_packetiser()

        negate_stn = os.getenv("STN_DELAY_SIGN", "poS")
        self._negate_stn_delay = negate_stn.lower() == "neg"
        self.logger.info("stn_delay_sign string: %s", negate_stn)
        self.logger.info("Station delays negated: %s", self._negate_stn_delay)

        negate_pss = os.getenv("PSS_DELAY_SIGN", "neG")
        self._negate_beam_delay = negate_pss.lower() == "neg"
        self.logger.info("PSS_delay_sign string: %s", negate_pss)
        self.logger.info("PSS delays negated: %s", self._negate_beam_delay)

        differential_pss = os.getenv("PSS_DELAY_FORMAT", "difF")
        self._differential_beam_delay = differential_pss.lower() == "diff"
        self.logger.info("PSS_delay_format string: %s", differential_pss)
        self.logger.info(
            "PSS delays differential: %s", self._differential_beam_delay
        )

    def do_register_changes(self, cmd):
        """
        Called to change FPGA register settings arising from changes to
        allocator attributes that specify the FPGA state
        :param cmd: Dictionary of parameter change commands
        """
        if "subarray" in cmd:
            subarray_descr = cmd["subarray"]
            pipeline_defs = cmd["regs"]
            self.logger.warning(
                "PSS register programming skipped (subarray=%s regs=%s)",
                str(subarray_descr),
                str(pipeline_defs),
            )
            self._program_regs(subarray_descr, pipeline_defs)
        else:
            self.logger.warn("Unrecognised register command")

    def periodic_actions(self):
        """
        Called to periodically check or update FPGA registers
        (period in self._periodic_secs)
        """
        self._seconds += self._periodic_secs
        if self._seconds >= FpgaMgrPss.PERIODIC_PRINT_SECS:
            self._seconds -= FpgaMgrPss.PERIODIC_PRINT_SECS
            self.logger.info("PSS periodic checks")
            fpga_io_status = self.fpga_status()
            self.logger.info("fpga_status: %s", str(fpga_io_status))
            self._io_stats_cbk(fpga_io_status)
        # Update beam delay polynomials if there are scanning subarrays
        for idx, ps in enumerate(self._pipestates):
            if ps is None or ps.params.subarray_id not in self._subs_scanning:
                # pipeline not used, or subarray using is not scanning
                continue
            # These calls modify class variables, need to be done in order...
            self._check_current_pkt_counters(idx, ps)  # read SPS packet time
            self._queue_subscribed_polys(ps)  # get delays from subscriptions
            self._prep_polys_for_use(ps)  # pick the two polys to use
            self._update_stn_beam_delay_registers(idx, ps)  # delays to FPGA
            self._update_pss_delay_registers(idx, ps)  # PSS delays to FPGA
            # check polys, enable data if valid
            self._check_polys_valid(idx, ps)
        # publish stats about delays
        self._update_subscription_valid()
        self._update_delay_stats()

    def _check_current_pkt_counters(self, idx, pipestate):
        """
        Read current ingest packet counters:
            - check SPS data is being received on all in-use virtual channels
            - find highest packet number
            - for debug, log the spread of the incoming packet numbers

        :return: highest packet number seen so far on any virtual channel
        """
        pipe = self.fpga.pipelines[idx]
        all_pkt_ctrs = pipe["ingest"].read_packet_counts()
        num_vchans = len(pipestate.params.freq_ids) * len(
            pipestate.params.stn_ids
        )
        vchan_pkt_cntrs = all_pkt_ctrs[0:num_vchans]
        highest_pkt = max(vchan_pkt_cntrs)
        pkt_spread = 0
        if (pipestate.prev_vchan_pkt_nos is not None) and (
            len(pipestate.prev_vchan_pkt_nos) == num_vchans
        ):
            changes = [
                vchan_pkt_cntrs[i] - pipestate.prev_vchan_pkt_nos[i]
                for i in range(0, num_vchans)
            ]
            max_delta = max(changes)
            min_delta = min(changes)
            if max_delta == 0:
                self.logger.warning(
                    "pkt_no = %s, No new SPS packets!",
                    highest_pkt,
                )
            else:
                # find lowest packet counter that's actually incrementing
                # and how many channels have packet counts that increment
                stns_wi_nonzero_delta = 0
                lowest_nz = highest_pkt
                for cnt in range(0, num_vchans):
                    # is this virtual channel getting any packets?
                    if changes[cnt] > 0:
                        lowest_nz = min(lowest_nz, vchan_pkt_cntrs[cnt])
                        stns_wi_nonzero_delta += 1
                pkt_spread = highest_pkt - lowest_nz
                # log recent SPS packet number info
                log_msg = f"pkt_no={highest_pkt}, spread={pkt_spread}"
                if min_delta == 0:
                    log_msg += f", only {stns_wi_nonzero_delta} stns received"
                    self.logger.warning(log_msg)
                else:
                    self.logger.debug(log_msg)

        # save current pkt nos for comparison at next read
        pipestate.prev_vchan_pkt_nos = vchan_pkt_cntrs
        # save current SPS time as highest packet count
        pipestate.highest_pkt_no = highest_pkt
        # save SPS time as seconds since J2000 epoch
        pipestate.highest_pkt_secs = highest_pkt * self.SECS_PER_PKT
        # save SPS time as integration_block after J2000 epoch
        pipestate.highest_blk_no = highest_pkt / self.PKTS_PER_FILTER_FRAME
        # save earliest-to-latest spread in packet numbers
        pipestate.pkt_spread = pkt_spread
        pipe = self.fpga.pipelines[idx]
        # save CT1 HBM block number being written
        pipestate.ct1_hbm_blk_no = pipe["ct1_psr"].read_ct1_count()
        # save CT2 HBM block numbers
        pipestate.ct2_hbm_blk_nos = pipe["ct2_pss"].read_ct2_counts()

    def _queue_subscribed_polys(self, pipestate) -> None:
        """
        Drain polynomials from subscriptions
        and place in local queue
        Groom queue to contain only relevant polynomials:
        - not too old,
        - not too many,
        - no duplicates
        """
        dly_obj = pipestate.stn_bm_dly_sub
        dly_name = pipestate.stn_bm_dly_sub_name
        dly_obj.queue_subscribed_polys(
            (pipestate.ct1_hbm_blk_no - self.CT1_LAG),
            self.FPS_NUMER,
            self.FPS_DENOM,
            dly_name,
        )
        # now all the pss-beam delays
        for cnt, dly_obj in enumerate(pipestate.pss_bm_subs):
            dly_name = pipestate.pss_bm_names[cnt]
            dly_obj.queue_subscribed_polys(
                min(pipestate.ct2_hbm_blk_nos),
                self.FPS_NUMER,
                self.FPS_DENOM,
                dly_name,
            )

    def _prep_polys_for_use(self, pipestate) -> None:
        """
        Choose the two delay polynomials that should be
        currenly used for the station-beam
        """
        dly_obj = pipestate.stn_bm_dly_sub
        dly_name = pipestate.stn_bm_dly_sub_name
        pipestate.was_poly_upated = dly_obj.prep_polys_for_use(
            dly_name, pipestate.ct1_hbm_blk_no - self.CT1_LAG
        )
        # now all the pss-beam delays
        for cnt, dly_obj in enumerate(pipestate.pss_bm_subs):
            dly_name = pipestate.pss_bm_names[cnt]
            pipestate.was_pss_updated[cnt] = dly_obj.prep_polys_for_use(
                dly_name, min(pipestate.ct2_hbm_blk_nos)
            )

    def _update_stn_beam_delay_registers(self, idx, pipestate) -> None:
        """
        Write the two prepared polynomials to registers. First one
        of the two should be writen to the first hardware buffer, second
        one should be written to the second buffer.

        Both buffers will be completely written, but in some places the data
        written will be the same as the data already in the buffer

        Buffers holding data to be programmed to FPGA are initialised with
        floating-pt zero == integer zero (all zero bits)
        One 20-word entry per Virtual channel * 1024 virt chans = 20k words
        """
        if not pipestate.was_poly_upated:
            return

        dly_obj = pipestate.stn_bm_dly_sub

        words_buffer = [
            [0] * 20 * 1024,
            [0] * 20 * 1024,
        ]

        base_word = 0
        # get delay poly for each entry in the VCT table
        for frq_id, stn, sstn in pipestate.vct_freq_stn:
            polys_avail = dly_obj.get_polys_avail()

            for buffer_num, poly in enumerate(polys_avail):
                if poly is None:
                    continue  # zero present from initialisation of buffer
                # _, blk_num, offset_sec, delay_details, _ = poly
                # find station's delay in poly
                coeffs = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
                pol_offset_ns = 0
                delay_details = poly.poly
                for itm in delay_details:
                    if (
                        itm["station_id"] == stn
                        and itm["substation_id"] == sstn
                    ):
                        coeffs = itm["xypol_coeffs_ns"]
                        # pad polynomials that only have the constant term
                        while len(coeffs) < 6:
                            coeffs.append(0.0)
                        pol_offset_ns = itm["ypol_offset_ns"]
                        break
                # have we been told to negate station delays
                if self._negate_stn_delay:
                    coeffs = [-val for val in coeffs]
                # Collect data elements needed for FPGA programming
                sky_ghz = frq_id * SKA_COARSE_SPACING_GHZ
                offset_sec = poly.offset_sec
                blk_num = poly.first_valid_blk_no
                blk_num = blk_num | self.POLY_IS_VALID_BIT
                # convert fp and int values into 32-bit unsigned for registers
                tmp_bytes = struct.pack(
                    "<dddddddddQ",
                    *coeffs,  # no need for negation. firmware does that
                    sky_ghz,
                    offset_sec,
                    pol_offset_ns,
                    blk_num,
                )
                words_buffer[buffer_num][
                    base_word : base_word + 20
                ] = struct.unpack("<IIIIIIIIIIIIIIIIIIII", tmp_bytes)
            base_word += 20  # 20 words per delay-table entry
        pipe = self.fpga.pipelines[idx]
        pipe["ct1_psr"].update_beam_delay_polys(words_buffer)

    def _update_pss_delay_registers(self, idx, pipestate) -> None:
        """
        Write the two prepared polynomials for each PSS beam to registers.
        :param idx: signal processing pipeline number
        :param pipe: parameters and configuration of pipeline
        """
        # PSS subscriptions share common start-of-validity registers, so
        # registers can only be updated when all have updates available
        if not self._pss_subscriptions_all_updated(pipestate):
            return

        start_time = time.time()

        # There are two delay-polynomial buffers. Compare saved start-of-
        # validity (SOV) for each buffer against start-of-validity for the
        # current polynomials to decide if to update buffer
        dly_obj = pipestate.pss_bm_subs[0]  # all SOVs same, use first one
        polys_avail = dly_obj.get_polys_avail()
        for buf_idx, poly in enumerate(polys_avail):
            if poly is not None and (
                poly.first_valid_blk_no != pipestate.pss_poly_sovs[buf_idx]
            ):
                self._update_pss_beam_polys(idx, buf_idx, pipestate)
                pipestate.pss_poly_sovs[buf_idx] = poly.first_valid_blk_no

        # Log elapsed time for this fairly complex update
        elapsed_time = time.time() - start_time
        elapsed_txt = f"PSS polys updated in {elapsed_time:.3f} seconds"
        self.logger.info(elapsed_txt)

    def _update_pss_beam_polys(self, idx, bufno, pipestate):
        """
        Update one of the two polynomial buffers in a sigproc pipeline

        :param idx: The signal processing pipeline [0,1]
        :param bufno: The polynomial buffer [0,1]
        :param pipe: The global sigproc pipeline parameters
        """
        if not self._differential_beam_delay:
            # The epoch of all the PST beam poly subscriptions (same for all)
            psr_epoch = pipestate.pss_bm_subs[0].get_polys_avail()[bufno].epoch
            # get station-beam coefficients from buffer with matching delay
            stn_coeffs = {}
            stn_polys_avail = pipestate.stn_bm_dly_sub.get_polys_avail()
            for stn_poly in stn_polys_avail:
                if stn_poly is None:
                    continue
                stn_beam_epoch = stn_poly.epoch
                if stn_beam_epoch == psr_epoch:
                    for itm in stn_poly.poly:
                        stn = itm["station_id"]
                        sstn = itm["substation_id"]
                        coffs = itm["xypol_coeffs_ns"]
                        while len(coffs) < 6:
                            coffs.append(0.0)
                        stn_coeffs[(stn, sstn)] = coffs
                    break
            if len(stn_coeffs) == 0:
                self.logger.error("PST-beam and stn-beam epochs don't match ")
                return

        pipe = self.fpga.pipelines[idx]
        # make buffer invalid while we update
        pipe["ct2_pss"].write_dly_invalid(bufno)

        # get order of stations  TODO use pipe.stn_vct_order
        # assumption here that stn, stn same all for all freqs
        # ie that each pipeline handles only one subarray-beam
        stn_order = []
        for _, stn, sstn in pipestate.vct_freq_stn:
            full_stn_id = (stn, sstn)
            if full_stn_id not in stn_order:
                stn_order.append(full_stn_id)

        # update the polys in the buffer for all pss beams
        for beam_idx, dly_obj in enumerate(pipestate.pss_bm_subs):
            polys_avail = dly_obj.get_polys_avail()
            poly = polys_avail[bufno]
            delay_details = poly.poly
            # get poly coeffs as dict keyed by (station, substation)
            coeff_lookup = {}
            for itm in delay_details:
                stn = itm["station_id"]
                sstn = itm["substation_id"]
                coeffs = itm["xypol_coeffs_ns"]
                while len(coeffs) < 6:  # extend to 6 coeffs if necessary
                    coeffs.append(0.0)
                # assert itm["ypol_offset_ns"] should be zero??
                if self._negate_beam_delay:
                    coeff_lookup[(stn, sstn)] = [-c for c in coeffs]
                else:
                    coeff_lookup[(stn, sstn)] = copy.deepcopy(coeffs)

                # Do we need to make pss-beam coefficients into differences?
                if not self._differential_beam_delay:
                    d_coeff = [0.0] * 6
                    for idxx, cff in enumerate(coeff_lookup[(stn, sstn)]):
                        d_coeff[idxx] = cff - stn_coeffs[(stn, sstn)][idxx]
                    coeff_lookup[(stn, sstn)] = d_coeff

            # create list of delays with stations added in order
            # FPGA requires even/odd stations to be separated
            delay_words_even_stns = []
            delay_words_odd_stns = []
            for count, stn_sstn in enumerate(stn_order):
                stn_coeffs = coeff_lookup[stn_sstn]
                nxt_words = PssPolyCoeffs(stn_coeffs).to_words()
                if count % 2 == 0:
                    delay_words_even_stns.extend(nxt_words)
                else:
                    delay_words_odd_stns.extend(nxt_words)
            # Write lists of even/odd station words to registers
            pipe["beamdlys_pss"].write_dly_for_beam(
                bufno,
                beam_idx,
                delay_words_even_stns,
                delay_words_odd_stns,
            )

        # update validity time and set buffer valid for all beams
        first_poly = pipestate.pss_bm_subs[0].get_polys_avail()[bufno]
        validity = PssValidity(
            first_poly.first_valid_blk_no,
            first_poly.valid_blks,
            round(first_poly.offset_sec * 1_000_000_000),  # as nsec
        ).to_words()
        pipe["ct2_pss"].write_dly_validity_for_beams(bufno, validity)

    def _pss_subscriptions_all_updated(self, pipe) -> bool:
        """Check if all PSS subscriptions were updated and have same
        start-of-validity time (ie ready to be written to registers)
        """
        # Perhaps none of the subscriptions were updated
        num_subs_updated = sum([1 for updat in pipe.was_pss_updated if updat])
        if num_subs_updated == 0:
            return False

        # At least one subcription was updated: check that all were
        # updated by ensuring all have same pair of start-of-validity values
        start_of_valid_template = None
        for dly_obj in pipe.pss_bm_subs:
            polys_avail = dly_obj.get_polys_avail()
            # get subscription's pair of start-of-valids as a list
            sov_pair = []
            for poly in polys_avail:
                if poly is None:
                    sov_pair.append(None)
                    continue
                sov_pair.append(poly.first_valid_blk_no)
            # save first one found as the comparison reference
            if start_of_valid_template is None:
                start_of_valid_template = sov_pair
            # Not all updated if we found different to the reference
            if sov_pair != start_of_valid_template:
                return False
        return True

    def _check_polys_valid(self, idx: int, pipestate):
        """
        Check whether there are valid station-beam delay polynomials and
        enable or disable the packetiser to output beam data

        :param idx: which signal processing pipeline to check
        """
        # Check if station beam has valid delay poly
        ct1_output_time = pipestate.ct1_hbm_blk_no - self.CT1_LAG
        dly_obj1 = pipestate.stn_bm_dly_sub
        stn_bm_valid = self._has_valid_poly(ct1_output_time, dly_obj1)
        pipestate.beam_polys_valid = stn_bm_valid

        # Check if PSS beams have valid delay polys
        # (Only check one since all are updated simulataneously)
        ct2_output_time = min(pipestate.ct2_hbm_blk_nos)
        dly_obj2 = pipestate.pss_bm_subs[0]
        pss_bm_valid = self._has_valid_poly(ct2_output_time, dly_obj2)
        pipestate.pss_bm_polys_valid = pss_bm_valid

        # Currently we allow pss data output with invalid pss polynomials
        # otherwise we would need:
        # if stn_bm_valid and pss_bm_valid:
        pipe = self.fpga.pipelines[idx]
        if stn_bm_valid:
            # Turn on output if not already on: polys have become valid
            if not pipestate.packetiser_enabled:
                pipe["packetiser_psr"].packetiser_enable(True)
                pipestate.packetiser_enabled = True
                self.logger.info("Pipe %d: Valid stn beam poly", idx)
            return

        # Left this code in case we change our mind
        # Desired new behaviour:
        #     If beam poly becomes invalid, leave beam running (do nothing)
        #
        # Turn output off if currently on, since polys have gone invalid
        # if pipestate.packetiser_enabled:
        #     pipe["packetiser_psr"].packetiser_enable(False)
        #     pipestate.packetiser_enabled = False
        #     self.logger.error("Pipe %d: No valid stn beam poly", idx)

    def _has_valid_poly(self, ct_output_time, dly_obj) -> bool:
        """
        Check if the delay object contains polynomials that are valid for the
        corner turner output time. Delay object polys are the polys currently
        in the two FPGA buffers. valid if current corner turn time is
        after their start-of-validity and before their expiry

        :param ct_output_time: Current corner turn output time (in blocks)
        :param dly_obj: Delay subscription object (DelayPolySubscriber class)
        """
        polys_avail = dly_obj.get_polys_avail()
        # Valid if either of the delay polys in polys_avail is valid
        stn_bm_valid = False
        for poly_info in polys_avail:
            if poly_info is None:
                continue
            start_time = (poly_info.epoch * self.FPS_NUMER) / self.FPS_DENOM
            valid_t = (poly_info.valid_secs * self.FPS_NUMER) / self.FPS_DENOM
            if start_time <= ct_output_time <= (start_time + valid_t):
                stn_bm_valid = True
                break
        return stn_bm_valid

    def _update_delay_stats(self):
        """Update Tango attribute with delay polynomial details"""
        subarr_info = []
        sb_idx_map = {}  # map subarray ID to index in the above list
        for pstate in self._pipestates:
            if not pstate:
                continue
            # make list of start-of-validity epoch seconds
            epochs_programmed = []
            earliest_poly_idx = None
            fpga_polys = pstate.stn_bm_dly_sub.get_polys_avail()
            for i, poly_info in enumerate(fpga_polys):
                if poly_info is None:
                    epochs_programmed.append(None)
                    continue
                start_time = poly_info.epoch
                epochs_programmed.append(start_time)
                # Is this polynomial the earliest epoch (ie likely in-use)
                if (
                    earliest_poly_idx is None
                    or start_time < epochs_programmed[earliest_poly_idx]
                ):
                    earliest_poly_idx = i
            # make list of delay values for each station in subarray
            # corresponding to the start of the earliest delay poly (ie in use)
            stn_delay_ns = []
            if earliest_poly_idx is not None:
                poly_in_use = fpga_polys[earliest_poly_idx]
                self.logger.info(
                    f"USING:{poly_in_use.epoch}  PKT:{pstate.highest_pkt_secs:.1f}  VALID:{pstate.beam_polys_valid}"
                )
                for stn_dly_info in poly_in_use.poly:
                    stn_dly = {
                        # TODO: shouldn't depend on polynomial internal names
                        "stn": stn_dly_info["station_id"],
                        "ns": stn_dly_info["xypol_coeffs_ns"][0],
                    }
                    stn_delay_ns.append(stn_dly)
            # copy details to published structure
            # structure is list-of-dicts, one dict in list per subarray
            subarray_id, beam_id = pstate.subarray_id, pstate.beam_id
            if subarray_id not in sb_idx_map:
                # a new entry for subarray not seen so far
                subarr_info.append({"subarray_id": subarray_id, "beams": []})
                # map subarray to last entry in top level list (subarr_info)
                sb_idx_map[subarray_id] = len(subarr_info) - 1
            list_index = sb_idx_map[subarray_id]
            subscription_ok = self._subscr_monitor.is_ok(subarray_id, beam_id)
            beams = subarr_info[list_index]["beams"]
            beams.append(
                {
                    "beam_id": beam_id,
                    "valid_delay": pstate.beam_polys_valid,
                    "subscription_valid": subscription_ok,
                    "delay_start_secs": epochs_programmed,
                    "stn_delay_ns": stn_delay_ns,
                }
            )
            beams[-1]["pss"] = []
            pss_info = beams[-1]["pss"]
            for i, subscriber in enumerate(pstate.pss_bm_subs):
                pss_info.append(
                    {
                        "name": pstate.pss_bm_names[i],
                        "valid_delay": pstate.pss_bm_polys_valid,
                        "subscription_valid": not subscriber.failed,
                    }
                )

        # update Tango attribute via the callback
        self._subarray_stats_cbk(subarr_info)

    def _update_subscription_valid(self):
        """Keep track of delay polynomial subscription failures and recoveries"""
        modified = False
        for pipestate in self._pipestates:
            if (
                pipestate is None
                or pipestate.subarray_id not in self._subs_scanning
                or pipestate.stn_bm_dly_sub is None
            ):
                continue
            subarray, beam = pipestate.subarray_id, pipestate.beam_id
            if pipestate.stn_bm_dly_sub.failed:
                modified |= self._subscr_monitor.add(subarray, beam)
                continue
            # otherwise clear this subarray-beam in case it failed previously
            modified |= self._subscr_monitor.remove(subarray, beam)
        # push the change to interested parties
        if modified:
            self._subscr_stats_cbk(self._subscr_monitor.get())

    def cleanup(self):
        """
        Final actions before thread exit
        """
        # Set all pipelines unused to stop any processing that may be
        # happening in FPGA and then deconfigure all pipelines
        self._program_regs({}, [{}] * self.fpga.num_pipelines)

    def _program_regs(self, intl_subarray_info: dict, intl_alveo_defs: list):
        """
        Handle changes in configured and scanning subarrays.
        Steps are:
        - Configure: prepare pipeline for use (subscribe to delays, clear regs)
        - StartScan: FPGA pipeline regs programmed, output packets flow
        - Stopscan: FPGA pipeline regs cleared, output packets stopped
        - Deconfigure: delays are unsubscribed, etc

        Note: each PSS pipeline handles only ONE subarray (not like correlator)

        :param intl_subarray_info: dict of all subarray info from allocator's
            "internal_subarray" atrribute
        :param intl_alveo_defs: list of PSS beam definition dictionaries
            from allocator's "internal_alveo" attribute. Each list entry
            is either empty or contains keys:
                "subarray_id"
                "pss_beam_id"
                "delay_poly"
                "jones"
                "destinations"
                ( and more: "stn_weights", "rfi_enable", ...)
        """

        subs_configured, subs_scanning_now = get_subarray_lists(
            intl_subarray_info
        )
        self.logger.info("Subarrays configured: %s", subs_configured)
        self.logger.info("Subarrays scanning: %s", subs_scanning_now)
        self.logger.info("PSS pipeline descriptions: %s", intl_alveo_defs)

        # For each PSS signal processing "pipeline" in FPGA ...
        for index in range(0, len(intl_alveo_defs)):
            subarray_id = get_internal_subarray_id(intl_alveo_defs, index)

            # pipeline is not allocated for use by any subarray
            if subarray_id is None:
                if self._pipestates[index] is None:  # already deconfigured
                    continue
                self.deconfigure_pipeline(index)
                continue

            if subarray_id not in subs_configured:
                # When a new subarray is added to the FPGA, internal_aveo (regs)
                # must be published by allocator before internal_subarray.
                # Next update will have subarray updated, do nothing with regs
                # (internal_alveo_defs) yet
                continue

            # pipeline IS allocated for use by a subarray, but the pipeline
            # isn't yet configured for use and ready for scan
            if self._pipestates[index] is None:
                self.configure_pipeline(
                    intl_subarray_info, intl_alveo_defs, index
                )

            # Pipeline is configured but not scanning, hold logic inactive
            if subarray_id not in subs_scanning_now:
                self.stop_pipeline_scan(index)
                continue

            # Pipeline is configured and already scanning, nothing to do
            if subarray_id in self._subs_scanning:
                txt = f"PSS pipeline {index} subarray {subarray_id} continues"
                self.logger.info(txt)
                continue

            # Pipeline is configured, and neets to start scanning
            self.start_pipeline_scan(intl_subarray_info, index, subarray_id)

        self._subs_scanning = subs_scanning_now

    def start_pipeline_scan(self, intl_subarray_info, index, subarray_id):
        """
        Configure one of the signal-processing pipelines to run & produce beams

        :param intl_subarray_info: dict of all subarray info from allocator's
            "internal_subarray" atrribute
        :param index: Which signal processing pipeline to program [0,1,2]
        :param subarray_id: ID of subarray to start
        """
        pipe = self.fpga.pipelines[index]

        txt = f"PSS pipeline {index} start for subarray {subarray_id}"
        self.logger.info(txt)
        # Ensure pipeline is stopped before making changes
        pipe["ct2_pss"].hbm_reset_and_wait_for_done()
        pipe["ingest"].hbm_reset_and_wait_for_done()

        pipestate = self._pipestates[index]
        params = pipestate.params

        # VHDL suggests this should be done before configuring. Correct??
        # self.fpga.ct2_temporary_reset(index)
        # program VCT entries
        vct_words, pipestate.vct_freq_stn = pss_vct(
            subarray_id,
            params.stn_bm_id,
            params.freq_ids,
            params.stn_ids,
            params.stn_vct_order,
        )
        first_pkt_mhz = pss_pktzr_freq_config(params.freq_ids)
        first_chans = pss_pktzr_first_chans(params.freq_ids)
        tot_coarse = len(params.freq_ids)
        tot_stns = len(params.stn_ids)
        tot_vchans = tot_coarse * tot_stns
        self.logger.info("program VCT")
        pipe["ingest"].prgm_vct(vct_words, tot_coarse, tot_stns, tot_vchans)
        self.logger.info("program VCT done")

        timing_beam_info = get_station_beam_pss_info(
            intl_subarray_info, subarray_id, params.stn_bm_id
        )
        pktzr_beam_nos = pss_pktzr_beam_nos(timing_beam_info)
        tot_beams = len(timing_beam_info)

        # Initialise pss beam delays in FPGA as invalid & zero coefficient
        # and initialise FPGA Jones as unity with unity station weights
        # (Will be updated as delay polynomials and jones are received)
        for buf in range(0, 2):
            pipe["ct2_pss"].write_dly_invalid(buf, wait=False)
        for bm in range(0, tot_beams):
            self.set_zero_pss_dlys(index, bm, tot_stns)

        # Write initial Unity jones (station+beam), and station weights
        for buf in range(0, 2):
            # Initialise station jones and station weights
            self.set_weighted_unity_stn_jones(
                index,
                buf,
                tot_vchans,
                tot_stns,
                params.stn_weights[bm],  # last beam weight is used for all
                params.stn_vct_order,
            )
            # initialise beam jones for all channels of each beam
            for bm in range(0, tot_beams):
                self.write_beam_unity_jones(
                    index, buf, tot_beams, tot_coarse, bm
                )
            # Mark the unity jones as valid forever
            self.set_jones_inf_valid(index, buf)

        # write sky frequencies used in pss-delay phase calcs (one per vchan)
        # actually has to be first PSS channel
        freq_ghz = []
        for frq in pipestate.params.freq_ids:
            freq_ghz.extend(
                [(frq - 108 / 216) * SKA_COARSE_SPACING_GHZ]
                * len(pipestate.params.stn_ids)
            )
        pipe["beamdlys_pss"].write_sky_freqs(freq_ghz)
        self.logger.info("program zero-delays and unity Jones done")

        # configure packetiser
        words1, words2 = mhz_to_first_freq_tbls(first_pkt_mhz)
        # self.logger.info("program packetiser freq map")
        pipe["packetiser_psr"].prgm_packetiser_first_freqs(words1, words2)
        pipe["packetiser_psr"].prgm_packetiser_first_chans(first_chans)
        self.logger.info("program packetiser freq map done")

        scan_id = get_internal_scan_id(intl_subarray_info, subarray_id)
        pktzr_data = pss_pktzr_config(timing_beam_info, scan_id)
        # self.logger.info("program packetiser")
        pipe["packetiser_psr"].prgm_packetiser_beam_nos(pktzr_beam_nos)
        pipe["packetiser_psr"].disable_and_prgm_packetiser(pktzr_data)
        pipestate.beam_polys_valid = False
        self.logger.info("program packetiser (disabled) done")

        # configure corner turn
        # self.logger.info("program corner turn")
        pipe["ct2_pss"].set_num_beams(tot_beams)
        pipe["ct2_pss"].clear_ct2_counts()
        pipestate.pss_poly_sovs = [None, None]  # no dly polys written yet
        self.logger.info("program corner turn done")

        # Finally, start pipeline running
        # self.logger.info("start ingest")
        pipe["ingest"].hbm_and_ingest_run()
        pipe["ct2_pss"].hbm_run()
        self.logger.info("start ingest done")
        # (Note: packetiser enabled later, when delays become valid)

    def set_zero_pss_dlys(self, idx, bm, num_stns):
        """
        Initialise a pss-beam delay poly to have all zero coefficients
        for all stations
        """
        # (6 x 32-bit floating point zeroes converted to integers)
        zero_delays = PssPolyCoeffs([0.0] * 6).to_words()
        delay_words_even_stns = []
        delay_words_odd_stns = []
        for count in range(0, num_stns):
            if count % 2 == 0:
                delay_words_even_stns.extend(zero_delays)
            else:
                delay_words_odd_stns.extend(zero_delays)
        pipe = self.fpga.pipelines[idx]
        for buf in range(0, 2):
            pipe["beamdlys_pss"].write_dly_for_beam(
                buf, bm, delay_words_even_stns, delay_words_odd_stns
            )

    # def set_unity_beam_jones(self, index, bm, num_vchans):
    #     """
    #     Initialise a pss-beam Jones matricies to unity for all stations

    #     :param index: fpga signal processing pipeline number [0,1]
    #     :param bm: pss beam index [0..number_of_pss_beams-1]
    #     :param num_vchans: number of virtual channels being beamformed
    #     """
    #     jones_unity = [
    #         0x00007FFF,
    #         0x00000000,
    #         0x00000000,
    #         0x00007FFF,
    #     ] * num_vchans

    #     wts_unity = [0x8000] * num_vchans

    #     pipe = self.fpga.pipelines[index]
    #     for buf in range(0, 2):
    #         pipe["beamjones_pss"].write_jones_for_beam(buf, bm, jones_unity)

    def set_weighted_unity_stn_jones(
        self, index, buf, tot_vchans, tot_stns, stn_weights, stn_order
    ):
        """
        Initialise a pss-beam Jones matricies to unity for all stations

        :param index: fpga signal processing pipeline number [0,1,2]
        :param buf: whichof the two fpga buffers to write
        :param tot_vchans: number of virtual channels being beamformed
        :param tot_stns: number of stations being beamformed
        :param stn_weights: list of station weights
        :param stn_order: where in list each weight goes
        """
        self.logger.info("stn_order: %s", str(stn_order))
        # if no weights supplied, use all unity weights
        if stn_weights is None:
            stn_weights = [1.0] * tot_stns
        # normalise so maximum weight is 1.0
        max_weight = max(stn_weights)
        rel_wts = [wt / max_weight for wt in stn_weights]
        # extend weights using last weight value in list if necessary
        if len(rel_wts) < tot_stns:
            rel_wts.extend([rel_wts[-1]] * (tot_stns - len(rel_wts)))

        jones_unity = [
            0x00004000,
            0x00000000,
            0x00000000,
            0x00004000,
        ]
        wts_unity = 0x8000

        jones = []
        wts = []
        for _ in range(0, tot_vchans // tot_stns):  # for each coarse channel
            # FPGA expects station weights in VCT ordering
            for stn_idx in stn_order:
                jval = [round(ju * rel_wts[stn_idx]) for ju in jones_unity]
                jones.extend(jval)
                wts.append(round(wts_unity * rel_wts[stn_idx]))

        pipe = self.fpga.pipelines[index]
        pipe["stnjones_pss"].write_weighted_stn_jones(buf, jones)
        pipe["stnjones_pss"].write_stn_weights(buf, wts)

    def set_jones_inf_valid(self, index: int, bufno: int) -> None:
        """
        Set Jones validity info to indicate Jones data is valid forever

        :param index: the signal processing pipeline number [0,1,2]
        :param bufno: the jones buffer number [0,1]
        """
        inf_valid = JonesValidity(0, 0xFFFF_FFFF).to_words()
        pipe = self.fpga.pipelines[index]
        pipe["ct2_pss"].write_station_jones_validity_for_buffer(
            bufno, inf_valid
        )
        pipe["ct2_pss"].write_beam_jones_validity_for_buffer(bufno, inf_valid)

    def write_beam_unity_jones(
        self, index: int, bufno: int, n_beams: int, n_chans: int, beamno: int
    ):
        """
        Write a unity Jones matrix for a beam

        :param index: the signal processing pipeline number [0,1,2]
        :param bufno: the jones buffer number [0,1]
        :param n_beams: number of PSS beams calculated from the station-beam
        :param n_chans: number of coarse channels computed for station-beam
        :param beamno: the beam index
        """

        jones_unity = [
            0x00004000,
            0x00000000,
            0x00000000,
            0x00004000,
        ]
        pipe = self.fpga.pipelines[index]
        for channo in range(0, n_chans):
            pipe["beamjones_pss"].write_beam_jones(
                bufno, n_beams, channo, beamno, jones_unity
            )

    def stop_pipeline_scan(self, index):
        """
        Stop a pipeline processing scan data by updating FPGA registers
        """
        self.logger.info("PSS pipeline %s is not scanning", str(index))
        pipe = self.fpga.pipelines[index]
        pipe["ingest"].clear_all_vcts()
        pipe["ct2_pss"].hbm_reset_and_wait_for_done()
        pipe["ingest"].hbm_reset_and_wait_for_done()
        pipe["packetiser_psr"].packetiser_enable(False)

    def configure_pipeline(self, intl_subarray, intl_alveo, index):
        """
        Prepare a pipeline for use by a subarray (before scanning)
        by retrieving pipeline params & subscribing to polynomials, gettin
        """
        pipe_params = get_pipeline_params(intl_alveo, intl_subarray, index)
        new_pipestate = PipelineState(pipe_params)
        # Zero VCSTATS TODO

        # Subscribe to the station-beam delay polynomial
        dev, attr = pipe_params.stn_bm_dly_src.rsplit("/", 1)
        new_pipestate.stn_bm_dly_sub = DelayPolySubscriber(self.logger)
        new_pipestate.stn_bm_dly_sub.subscribe(dev, attr)
        new_pipestate.stn_bm_dly_sub_name = pipe_params.stn_bm_dly_src
        self.logger.info(
            "pipe %d subscribing stn beam delay %s/%s",
            index,
            dev,
            attr,
        )
        # subscribe to the pss-beam polynomials
        for cnt, pss_dly_uri in enumerate(pipe_params.pss_dly_srcs):
            new_pipestate.pss_bm_subs.append(DelayPolySubscriber(self.logger))
            dev, attr = pss_dly_uri.rsplit("/", 1)
            new_pipestate.pss_bm_subs[cnt].subscribe(dev, attr)
            new_pipestate.pss_bm_names.append(pss_dly_uri)
            new_pipestate.was_pss_updated.append(False)
            self.logger.info(
                "pipe %d subscribing PSS beam delay %s/%s",
                index,
                dev,
                attr,
            )
        # add the new pipeline parameters structure
        self._pipestates[index] = new_pipestate

    def deconfigure_pipeline(self, index):
        """
        Pipeline no longer used by any subarray. Deconfigure by unsubscribing
        from all delay polynomials and deleting the pipeline params
        """
        self.stop_pipeline_scan(index)  # (required for thread exit case)

        if self._pipestates[index].stn_bm_dly_sub is not None:
            pipeline = self._pipestates[index]
            # remove health monitoring of attributes
            subarray, beam = pipeline.subarray_id, pipeline.beam_id
            if self._subscr_monitor.remove(subarray, beam):
                self._subscr_stats_cbk(self._subscr_monitor.get())
            # unsubscribe from station-beam polynomials
            pipeline.stn_bm_dly_sub.unsubscribe()
            pipeline.stn_bm_dly_sub = None
            self.logger.info("pipe %d unsubscribed stn-beam delay", index)
            # unsubscribe from pss-beam polynomials
            for cnt, sub in enumerate(pipeline.pss_bm_subs):
                if sub is not None:
                    sub.unsubscribe()
                    sub = None
                    self.logger.info(
                        "pipe %d unsubscribed PSS delay: %s",
                        index,
                        pipeline.pss_bm_names[cnt],
                    )
            # Free pipeline parameters struct for garbage collection
            self._pipestates[index] = None

    def fpga_status(self) -> dict:
        """
        Gather status indication for FPGA processing operations
        """
        summary = {}
        summary["fpga_uptime"] = self.fpga["system"]["time_uptime"].value
        summary["total_pkts_in"] = self.fpga["system"][
            "eth100g_rx_total_packets"
        ].value
        summary["spead_pkts_in"] = self.fpga["lfaadecode100g"][
            "spead_packet_count"
        ].value
        summary["total_pkts_out"] = self.fpga["system"][
            "eth100g_tx_total_packets"
        ].value
        # Total good pkt output across all signal processing pipelines
        good_pkts = 0
        for idx in range(0, self.fpga.num_pipelines):
            pipe = self.fpga.pipelines[idx]
            good_pkts += pipe["packetiser_psr"].get_num_valid_pkts()
        summary["pss_pkts_out"] = good_pkts
        return summary

# -*- coding: utf-8 -*-
#
# Copyright (c) 2022 CSIRO Space and Astronomy.
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more information.


import numpy as np
from ska_low_cbf_fpga import FpgaPeripheral


class FpgaBeamJonesPss:
    """
    An interface to PSS beam Jones peripherals for one SigProc pipeline
        **** FPGA Peripheral name: pssbeamformerP_beamjones ****

    Each signal processing pipeline (P) has 4 beamformer peripherals that operate
    in parallel, each forming 1/4 of the required beams. All four get their
    beam Jones data from the RAMs in this block

    PSS beam jones consists of 4 peripherals each holding a RAM block
    RAM contents are:
      2kwords Beam Jones buffer 0  (512 beams x 4 32-bit words)
      2kwords Beam Jones buffer 1  (512 beams x 4 32-bit words)
    Concept is that one Jones buffer will be in active use and the other
    buffer can be updated with newer Jones information.
    """

    JONES_BUF_OFFSET = 0  # BEAM Jones starts at base of RAM
    JONES_BUF_ENTRIES = 2048  # 2k entries in each buffer
    WORDS_PER_JONES_ENTRY = 4  # 4x 32-bit words represent a Jones Matrix
    JONES_BUF_WORDS = JONES_BUF_ENTRIES * WORDS_PER_JONES_ENTRY

    def __init__(self, fpga_periphs: FpgaPeripheral, logger):
        self.regs = fpga_periphs  # Access to FPGA peripheral registers
        self.logger = logger

    def write_beam_jones(
        self,
        bufno: int,
        beams_en: int,
        chan: int,
        beamno: int,
        words: list[int],
    ) -> None:
        """
        Write new beam-jones data to one of the buffers
        [Note could change to write all beams for a chan in one call - delete beamno]

        Each jones matrix is 4 words (defined same as station jones matrices)
        addr = 4 * (bufno*JONES_BUF_ENTRIES + BeamsEnabled*coarse_chan + beam)
        bufno is 0 or 1

        :param bufno: which buffer to write to (0 or 1)
        :param beams_en: how many PSS beams currently enabled
        :param chan: which coarse channel (range 0..n_chans-1)
        :param beamno: which beam to write
        :param words: list containing 4 integers for beam jones
        """
        assert len(words) == 4, f"Got {len(words)} Jones words, but expected 4"

        # FPGA driver requires numpy array of data
        np_words = np.asarray(words, dtype=np.uint32)

        if bufno != 0:
            bufno = 1
        start = (
            self.JONES_BUF_OFFSET
            + bufno * self.JONES_BUF_WORDS
            + (beams_en * chan + beamno) * self.WORDS_PER_JONES_ENTRY
        )
        end = start + len(np_words)

        if self.regs is not None:
            self.regs.data[start:end] = np_words
        else:
            print(f"[{start}:{end}]: {np_words}")

# -*- coding: utf-8 -*-
#
# Copyright (c) 2022 CSIRO Space and Astronomy.
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more information.

import time

import numpy as np
from ska_low_cbf_fpga import FpgaPeripheral


class FpgaIngest:
    """This class is the interface to fpga ingest peripherals"""

    VCT_BASE = 8192
    """Word in RAM at which the VC_TABLEs start (2 tables)"""
    VCT_ENTRIES = 1024
    """Number of entries in FPGA's Virtual Channel Table"""
    WORDS_PER_VCT_ENTRY = 2
    """Number of 32-bit words for each VCT entry in FPGA"""

    # VC_STATS table parameters
    STATS_WORDS_PER_VCHAN = 8
    """Number of 32-bit words in each VC_STATS table entry"""
    STATS_PKT_CNT_LO_OFFSET = 4
    STATS_PKT_CNT_HI_OFFSET = 5

    def __init__(self, fpga_periph: FpgaPeripheral, logger):
        self.regs = fpga_periph  # Access to FPGA peripheral registers
        self.logger = logger

    def read_vct(self, bufno: int, num_entries: int) -> list:
        """
        Read VCT from FPGA and decode into a list of VCT entries

        :param bufno: Which of the two buffers to read (0 or 1)
        :param num_entries: how many VCT entries to read
        :return: list of decoded fields for each VCT entry
        """
        if bufno != 0:  # Only 2 VCT buffers
            bufno = 1

        if num_entries > self.VCT_ENTRIES:
            num_entries = self.VCT_ENTRIES

        # read raw VCT words from FPGA
        num_words = num_entries * self.WORDS_PER_VCT_ENTRY
        start = (
            self.VCT_BASE + self.VCT_ENTRIES * self.WORDS_PER_VCT_ENTRY * bufno
        )
        vct_raw = self.regs.data[start : start + num_words]

        # decode the raw words into meaningful fields, one per VCT entry
        vct_decoded = []
        for idx in range(0, len(vct_raw), self.WORDS_PER_VCT_ENTRY):
            val = vct_raw[idx]
            subarray = (val >> 16) & 0x01F
            beam = (val >> 9) & 0x0F
            freq = val & 0x0FF
            stn = (val >> 21) & 0x03FF
            sstn = (val >> 13) & 0x07
            vchan = vct_raw[idx + 1]
            vct_decoded.append([stn, sstn, subarray, beam, freq, vchan])
        return vct_decoded

    def write_vct(self, bufno: int, words: list[int]) -> None:
        """
        Write a list of words into one of the two VC_Tables

        :param bufno: Which VC_Table buffer (0 or 1) ?
        :param words: List of 32-bit unsigned integers to write
        """
        if bufno != 0:  # Only 2 VCT buffers
            bufno = 1

        if len(words) > self.VCT_ENTRIES * self.WORDS_PER_VCT_ENTRY:
            words = words[0 : self.VCT_ENTRIES * self.WORDS_PER_VCT_ENTRY]

        start = (
            self.VCT_BASE + self.VCT_ENTRIES * self.WORDS_PER_VCT_ENTRY * bufno
        )
        self.regs.data[start : start + len(words)] = np.asarray(
            words, dtype=np.uint32
        )

    def prgm_vct(self, vct_words, tot_coarse, tot_stns, tot_vchans) -> None:
        """
        Write a new VCT and associated params to a PSS pipeline

        :param vct_words: list of VCT entry values
        :param tot_coarse: number of coarse channels beamformed
        :param tot_stns: number of (sub-)stations beamformed
        :param tot_vchans: number of FPGA virtual channels used
        """
        # which of the two VCT buffers is not-in-use?
        tbl = self.regs.table_select.value
        nxt_tbl = tbl ^ 1
        # write data to not-in-use VCT buffer
        self.write_vct(nxt_tbl, vct_words)
        # Temporarily (@ 2024-02-22) use single-register writes for these regs
        self.regs.total_stations.value = tot_stns
        self.regs.total_coarse.value = tot_coarse
        self.regs.total_channels.value = tot_vchans
        # Flip buffers to use the new VCT
        self.regs.table_select.value = nxt_tbl

    def clear_vct(self, bufno: int) -> None:
        """
        Write a VCT_Table to zero (all invalid entries)

        :param bufno: Which VC_Table to clear (0 or 1)?
        """
        self.write_vct(
            bufno, [0] * (self.VCT_ENTRIES * self.WORDS_PER_VCT_ENTRY)
        )

    def clear_all_vcts(self) -> None:
        """
        Clear all Virtual Channel Table Buffers
        """
        for bufno in range(0, 2):
            self.clear_vct(bufno)

    def read_packet_counts(self) -> list[int]:
        """
        Read latest packet counter (SPS timestamp) for all Virtual Channels

        :return: list of integer values, ordered by virtual channel number
        """
        n_words = self.VCT_ENTRIES * self.STATS_WORDS_PER_VCHAN
        np_pkt_cnts = self.regs.data[0:n_words]
        pkt_cnt_low = np_pkt_cnts[
            self.STATS_PKT_CNT_LO_OFFSET : n_words : self.STATS_WORDS_PER_VCHAN
        ]
        pkt_cnt_hi = np_pkt_cnts[
            self.STATS_PKT_CNT_HI_OFFSET : n_words : self.STATS_WORDS_PER_VCHAN
        ]
        full_pkt_cnt = (pkt_cnt_hi << 32) + pkt_cnt_low
        return full_pkt_cnt.tolist()

    def zero_packet_counts(self) -> None:
        """Clear all packet counts in VCSTATS"""
        n_words = self.VCT_ENTRIES * self.STATS_WORDS_PER_VCHAN
        self.regs.data[0:n_words] = np.asarray([0] * n_words, dtype=np.uint32)

    def hbm_reset_and_wait_for_done(
        self, sleep_secs=0.005, timeout_count=100
    ) -> None:
        """
        Hold HBM & Ingest in reset to allow VCT modifications to be made

        :param idx: PSS firmware pipeline number
        """
        self.regs.hbm_reset.value = 0x1  # reset HBM

        cnt = 0
        hbm_status = self.regs.hbm_reset_status.value
        while ((hbm_status & 0x1) == 0) and (cnt < timeout_count):
            time.sleep(sleep_secs)
            cnt += 1
            hbm_status = self.regs.hbm_reset_status.value
        if cnt >= timeout_count:
            self.logger.error(
                "HBM reset did not complete. HBM_reset_status: %s",
                hex(hbm_status),
            )
        # This comes after HBM reset so HBM has data flow to reach reset state
        self.regs.lfaa_decode_reset.value = 0x1  # reset ingest

    def hbm_and_ingest_run(self) -> None:
        """
        Enable pipeline to start producing PSS beam output after a config change

        """
        # This comes first so HBM is ready to accept ingest data
        self.regs.hbm_reset.value = 0x0  # run HBM
        self.regs.lfaa_decode_reset.value = 0x0  # run ingest

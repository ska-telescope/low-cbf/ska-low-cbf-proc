# -*- coding: utf-8 -*-
#
# (c) 2023 CSIRO Space and Astronomy.
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement
# See LICENSE.txt for more info.
"""FPGA Manager for PST Personality"""
import copy
import os
import struct
import time

from ska_low_cbf_proc.icl.correlator.constants import SKA_COARSE_SPACING_GHZ
from ska_low_cbf_proc.icl.fpga_mgr import FpgaMgr
from ska_low_cbf_proc.icl.pst.pst_calcs import (
    mhz_to_first_freq_tbls,
    pst_pktzr_beam_nos,
    pst_pktzr_config,
    pst_pktzr_first_chans,
    pst_pktzr_freq_config,
    pst_vct,
    zero_vct,
)
from ska_low_cbf_proc.icl.pst.pst_fpga import (
    JonesValidity,
    PstFpga,
    PstPolyCoeffs,
    PstValidity,
)
from ska_low_cbf_proc.icl.pst_intl_iface import (
    SigProcParams,
    get_internal_scan_id,
    get_internal_subarray_id,
    get_pipeline_params,
    get_station_beam_pst_info,
    get_subarray_lists,
)
from ska_low_cbf_proc.icl.subscription_monitor import SubscriptionMonitor
from ska_low_cbf_proc.processor.delay_subscription import DelayPolySubscriber


class PipelineState:
    """
    Class to hold parameters and state of one signal-processing pipeline
    """

    def __init__(self, params: SigProcParams):
        self.params = params  # processing parameters

        self.stn_bm_dly_sub = None  # stn-beam delay subscription
        self.stn_bm_dly_sub_name = ""  # name of dly sub attribute
        self.was_poly_upated = False
        self.beam_polys_valid = False  # is stn-beam poly valid

        self.pst_bm_subs = []  # list of pst beam delay subscriptions
        self.pst_bm_names = []  # Tango URIs for PST beam delays
        self.was_pst_updated = []
        self.pst_first_valid_blk = [None, None]  # common PST starts
        self.pst_bm_polys_valid = False  # are PST beam polys valid

        self.prev_vchan_pkt_nos = None  # pkt nums in used vchans
        self.highest_pkt_no = 0  # highest pkt number found so far
        self.highest_pkt_secs = 0  # seconds equivalent to pkt number
        self.highest_blk_no = 0  # 53ms filterbank block no eqiv to pkt no
        self.pkt_spread = 0  # earliest-to-latest spread of received pkt nos

        self.ct1_hbm_blk_no = 0  # block (53ms data) being written to CT1 HBM
        self.ct2_hbm_blk_nos = [0, 0]  # blocks in CT2 HBM
        self.ct2_poly_sovs = [None, None]  # start-of-valid of polys in Regs

        self.vct_freq_stn = None  # (freq, chan) in vct ordering

        self.is_scanning = False

    @property
    def subarray_id(self) -> int:
        """Return subarray ID this pipeline is processing"""
        return self.params.subarray_id

    @property
    def beam_id(self) -> int:
        """Return beam ID this pipeline is processing"""
        return self.params.stn_bm_id


class FpgaMgrPst(FpgaMgr):
    """
    A class for PST FPGAs that monitors and programs FPGA registers

    This class translates parameters specified by the Allocator into register
    updates. It also periodically monitors FPGA registers that indicate the
    quality of the ongoing operation of the PST FPGA
    """

    PERIODIC_PRINT_SECS = 30.0
    SECS_PER_PKT = 2211840e-9  # packets have 2048 samples @ 1080ns/sample
    PKTS_PER_FILTER_FRAME = 24  # filterbank uses 24pkts (53.084msec) at a time
    FPS_NUMER = 390625  # numerator of PST filterbank frames per sec fraction
    FPS_DENOM = 20736  # denominator of PST filterbank frames per sec fraction
    POLY_IS_VALID_BIT = 0x100_0000_0000
    CT1_LAG = 2  # CT1 processing lag behind CT1 HBM frame count

    def __init__(
        self,
        fpga: PstFpga,
        fail_cbk,
        subarray_stats_cbk,
        io_stats_cbk,
        subscr_stats_cbk,
        logger,
    ):
        super().__init__(fpga, fail_cbk, logger)
        self.logger.info("Initialising PST FPGA interactions")
        self.fpga = fpga
        # Check firmware is recent enough for this code to use
        self.fpga._check_fw(" PST", ">=1.0.3")
        # Additional check when fw version was not updated but registers were
        assert (
            self.fpga.system.args_map_build.value >= 0x25022511
        ), "PST Firmware not supported. Use VER>=1.0.3 dated after 2025-02-25"

        # override periodic timer interval for this specific FPGA personality
        self._periodic_secs = 4.0  # faster than 10-sec delay poly updates
        self._subarray_stats_cbk = subarray_stats_cbk
        self._io_stats_cbk = io_stats_cbk

        self._seconds = FpgaMgrPst.PERIODIC_PRINT_SECS
        self._subs_scanning = []  # list of scanning subarray_ids

        num_inst = self.fpga.num_sigproc_instances()
        self.logger.info("%d signal processing instances", num_inst)
        self._pipelines = [None] * num_inst
        self._subscr_stats_cbk = subscr_stats_cbk
        self._subscr_monitor = SubscriptionMonitor()

        negate_stn = os.getenv("STN_DELAY_SIGN", "poS")
        self._negate_stn_delay = negate_stn.lower() == "neg"
        self.logger.info("stn_delay_sign string: %s", negate_stn)
        self.logger.info("Station delays negated: %s", self._negate_stn_delay)

        negate_pst = os.getenv("PST_DELAY_SIGN", "neG")
        self._negate_beam_delay = negate_pst.lower() == "neg"
        self.logger.info("PST_delay_sign string: %s", negate_pst)
        self.logger.info("PST delays negated: %s", self._negate_beam_delay)

        differential_pst = os.getenv("PST_DELAY_FORMAT", "difF")
        self._differential_beam_delay = differential_pst.lower() == "diff"
        self.logger.info("pst_delay_format string: %s", differential_pst)
        self.logger.info(
            "PST delays differential: %s", self._differential_beam_delay
        )

        self._clear_delays = "CLEAR_PST_DELAYS_BEFORE_SCANS" in os.environ

    def do_register_changes(self, cmd):
        """
        Called to change FPGA register settings arising from changes to
        allocator attributes that specify the FPGA state
        :param cmd: Dictionary of parameter change commands
        """
        if "subarray" in cmd:
            subarray_descr = cmd["subarray"]
            pipeline_defs = cmd["regs"]
            self._program_regs(subarray_descr, pipeline_defs)
        else:
            self.logger.warn("Unrecognised register command")

    def periodic_actions(self):
        """
        Called to periodically check or update FPGA registers
        (period in self._periodic_secs)
        """
        self._seconds += self._periodic_secs
        if self._seconds >= FpgaMgrPst.PERIODIC_PRINT_SECS:
            self._seconds -= FpgaMgrPst.PERIODIC_PRINT_SECS
            self.logger.info("PST periodic checks")
            fpga_io_status = self.fpga.fpga_status()
            self.logger.info("fpga_status: %s", str(fpga_io_status))
            self._io_stats_cbk(fpga_io_status)
        # Update beam delay polynomials if there are scanning subarrays
        for idx, itm in enumerate(self._pipelines):
            if (
                itm is None
                or itm.params.subarray_id not in self._subs_scanning
            ):
                # pipeline not used, or subarray using is not scanning
                continue
            # These calls modify class variables, need to be done in order...
            self._check_current_pkt_counters(idx, itm)  # read SPS packet time
            self._queue_subscribed_polys(itm)  # get delays from subscriptions
            self._prep_polys_for_use(itm)  # pick the two polys to use
            self._update_stn_beam_delay_registers(idx, itm)  # delays to FPGA
            self._update_pst_delay_registers(idx, itm)  # PST delays to FPGA
            # check polys, enable data if valid
            self._check_polys_valid(idx, itm)
        # publish stats about delays
        self._update_subscription_valid()
        self._update_delay_stats()

    def _check_current_pkt_counters(self, idx, pipe):
        """
        Read current ingest packet counters:
            - check SPS data is being received on all in-use virtual channels
            - find highest packet number
            - for debug, log the spread of the incoming packet numbers

        :return: highest packet number seen so far on any virtual channel
        """
        all_pkt_ctrs = self.fpga.read_packet_counts(idx)
        num_vchans = len(pipe.params.freq_ids) * len(pipe.params.stn_ids)
        vchan_pkt_cntrs = all_pkt_ctrs[0:num_vchans]
        highest_pkt = max(vchan_pkt_cntrs)
        pkt_spread = 0
        if (pipe.prev_vchan_pkt_nos is not None) and (
            len(pipe.prev_vchan_pkt_nos) == num_vchans
        ):
            changes = [
                vchan_pkt_cntrs[i] - pipe.prev_vchan_pkt_nos[i]
                for i in range(0, num_vchans)
            ]
            max_delta = max(changes)
            min_delta = min(changes)
            if max_delta == 0:
                self.logger.warning(
                    "pkt_no = %s, No new SPS packets!",
                    highest_pkt,
                )
            else:
                # find lowest packet counter that's actually incrementing
                # and how many channels have packet counts that increment
                stns_wi_nonzero_delta = 0
                lowest_nz = highest_pkt
                for cnt in range(0, num_vchans):
                    # is this virtual channel getting any packets?
                    if changes[cnt] > 0:
                        lowest_nz = min(lowest_nz, vchan_pkt_cntrs[cnt])
                        stns_wi_nonzero_delta += 1
                pkt_spread = highest_pkt - lowest_nz
                # log recent SPS packet number info
                log_msg = f"pkt_no={highest_pkt}, spread={pkt_spread}"
                if min_delta == 0:
                    log_msg += f", only {stns_wi_nonzero_delta} stns received"
                    self.logger.warning(log_msg)
                else:
                    self.logger.debug(log_msg)

        # save current pkt nos for comparison at next read
        pipe.prev_vchan_pkt_nos = vchan_pkt_cntrs
        # save current SPS time as highest packet count
        pipe.highest_pkt_no = highest_pkt
        # save SPS time as seconds since J2000 epoch
        pipe.highest_pkt_secs = highest_pkt * self.SECS_PER_PKT
        # save SPS time as integration_block after J2000 epoch
        pipe.highest_blk_no = highest_pkt / self.PKTS_PER_FILTER_FRAME
        # save earliest-to-latest spread in packet numbers
        pipe.pkt_spread = pkt_spread
        # save CT1 HBM block number being written
        pipe.ct1_hbm_blk_no = self.fpga.read_ct1_count(idx)
        # save CT2 HBM block numbers
        pipe.ct2_hbm_blk_nos = self.fpga.read_ct2_counts(idx)

    def _queue_subscribed_polys(self, pipe) -> None:
        """
        Drain polynomials from subscriptions
        and place in local queue
        Groom queue to contain only relevant polynomials:
        - not too old,
        - not too many,
        - no duplicates
        """
        dly_obj = pipe.stn_bm_dly_sub
        dly_name = pipe.stn_bm_dly_sub_name
        dly_obj.queue_subscribed_polys(
            (pipe.ct1_hbm_blk_no - self.CT1_LAG),
            self.FPS_NUMER,
            self.FPS_DENOM,
            dly_name,
        )
        # now all the pst-beam delays
        for cnt, dly_obj in enumerate(pipe.pst_bm_subs):
            dly_name = pipe.pst_bm_names[cnt]
            dly_obj.queue_subscribed_polys(
                min(pipe.ct2_hbm_blk_nos),
                self.FPS_NUMER,
                self.FPS_DENOM,
                dly_name,
            )

    def _prep_polys_for_use(self, pipe) -> None:
        """
        Choose the two delay polynomials that should be
        currenly used for the station-beam
        """
        dly_obj = pipe.stn_bm_dly_sub
        dly_name = pipe.stn_bm_dly_sub_name
        pipe.was_poly_upated = dly_obj.prep_polys_for_use(
            dly_name, pipe.ct1_hbm_blk_no - self.CT1_LAG
        )
        # now all the pst-beam delays
        for cnt, dly_obj in enumerate(pipe.pst_bm_subs):
            dly_name = pipe.pst_bm_names[cnt]
            pipe.was_pst_updated[cnt] = dly_obj.prep_polys_for_use(
                dly_name, min(pipe.ct2_hbm_blk_nos)
            )

    def _update_stn_beam_delay_registers(self, idx, pipe) -> None:
        """
        Write the two prepared polynomials to registers. First one
        of the two should be writen to the first hardware buffer, second
        one should be written to the second buffer.

        Both buffers will be completely written, but in some places the data
        written will be the same as the data already in the buffer

        Buffers holding data to be programmed to FPGA are initialised with
        floating-pt zero == integer zero (all zero bits)
        One 20-word entry per Virtual channel * 1024 virt chans = 20k words
        """
        if not pipe.was_poly_upated:
            return

        dly_obj = pipe.stn_bm_dly_sub

        words_buffer = [
            [0] * 20 * 1024,
            [0] * 20 * 1024,
        ]

        base_word = 0
        # get delay poly for each entry in the VCT table
        for frq_id, stn, sstn in pipe.vct_freq_stn:
            polys_avail = dly_obj.get_polys_avail()

            for buffer_num, poly in enumerate(polys_avail):
                if poly is None:
                    continue  # zero present from initialisation of buffer
                # _, blk_num, offset_sec, delay_details, _ = poly
                # find station's delay in poly
                coeffs = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
                pol_offset_ns = 0
                delay_details = poly.poly
                for itm in delay_details:
                    if (
                        itm["station_id"] == stn
                        and itm["substation_id"] == sstn
                    ):
                        coeffs = itm["xypol_coeffs_ns"]
                        # pad polynomials that only have the constant term
                        while len(coeffs) < 6:
                            coeffs.append(0.0)
                        pol_offset_ns = itm["ypol_offset_ns"]
                        break
                # have we been told to negate station delays
                if self._negate_stn_delay:
                    coeffs = [-val for val in coeffs]
                # Collect data elements needed for FPGA programming
                sky_ghz = frq_id * SKA_COARSE_SPACING_GHZ
                offset_sec = poly.offset_sec
                blk_num = poly.first_valid_blk_no
                blk_num = blk_num | self.POLY_IS_VALID_BIT
                # convert fp and int values into 32-bit unsigned for registers
                tmp_bytes = struct.pack(
                    "<dddddddddQ",
                    *coeffs,  # no need for negation. firmware does that
                    sky_ghz,
                    offset_sec,
                    pol_offset_ns,
                    blk_num,
                )
                words_buffer[buffer_num][
                    base_word : base_word + 20
                ] = struct.unpack("<IIIIIIIIIIIIIIIIIIII", tmp_bytes)
            base_word += 20  # 20 words per delay-table entry
        self.fpga.update_beam_delay_polys(words_buffer, idx)

    def _update_pst_delay_registers(self, idx, pipe) -> None:
        """
        Write the two prepared polynomials for each PST beam to registers.
        :param idx: signal processing pipeline number
        :param pipe: parameters and configuration of pipeline
        """
        # PST subscriptions share common start-of-validity registers, so
        # registers can only be updated when all have updates available
        if not self._pst_subscriptions_all_updated(pipe):
            return

        start_time = time.time()

        # There are two delay-polynomial buffers. Compare saved start-of-
        # validity (SOV) for each buffer against start-of-validity for the
        # current polynomials to decide if to update buffer
        dly_obj = pipe.pst_bm_subs[0]  # all SOVs same, use first one
        polys_avail = dly_obj.get_polys_avail()
        for buf_idx, poly in enumerate(polys_avail):
            if poly is not None and (
                poly.first_valid_blk_no != pipe.ct2_poly_sovs[buf_idx]
            ):
                self._update_ct2_poly(idx, buf_idx, pipe)
                pipe.ct2_poly_sovs[buf_idx] = poly.first_valid_blk_no

        # Log elapsed time for this fairly complex update
        elapsed_time = time.time() - start_time
        elapsed_txt = f"PST polys updated in {elapsed_time:.3f} seconds"
        self.logger.info(elapsed_txt)

    def _update_ct2_poly(self, idx, bufno, pipe):
        """
        Update one of the two polynomial buffers in a sigproc pipeline

        :param idx: The signal processing pipeline [0,1,2]
        :param bufno: The polynomial buffer [0,1]
        :param pipe: The global sigproc pipeline parameters
        """

        if not self._differential_beam_delay:
            # The epoch of all the PST beam poly subscriptions (same for all)
            psr_epoch = pipe.pst_bm_subs[0].get_polys_avail()[bufno].epoch
            # get station-beam coefficients from buffer with matching delay
            stn_coeffs = {}
            stn_polys_avail = pipe.stn_bm_dly_sub.get_polys_avail()
            for stn_poly in stn_polys_avail:
                if stn_poly is None:
                    continue
                stn_beam_epoch = stn_poly.epoch
                if stn_beam_epoch == psr_epoch:
                    for itm in stn_poly.poly:
                        stn = itm["station_id"]
                        sstn = itm["substation_id"]
                        coffs = itm["xypol_coeffs_ns"]
                        while len(coffs) < 6:
                            coffs.append(0.0)
                        stn_coeffs[(stn, sstn)] = coffs
                    break
            if len(stn_coeffs) == 0:
                self.logger.error("PST-beam and stn-beam epochs don't match ")
                return

        # make buffer invalid while we update
        self.fpga.write_dly_invalid(idx, bufno)

        # get order of stations  TODO use pipe.stn_vct_order
        # assumption here that stn, stn same all for all freqs
        # ie that each pipeline handles only one subarray-beam
        stn_order = []
        for _, stn, sstn in pipe.vct_freq_stn:
            full_stn_id = (stn, sstn)
            if full_stn_id not in stn_order:
                stn_order.append(full_stn_id)

        # update the polys in the buffer for all pst beams
        for beam_idx, dly_obj in enumerate(pipe.pst_bm_subs):
            polys_avail = dly_obj.get_polys_avail()
            poly = polys_avail[bufno]
            delay_details = poly.poly
            # get poly coeffs as dict keyed by (station, substation)
            coeff_lookup = {}
            for itm in delay_details:
                stn = itm["station_id"]
                sstn = itm["substation_id"]
                coeffs = itm["xypol_coeffs_ns"]
                while len(coeffs) < 6:  # extend to 6 coeffs if necessary
                    coeffs.append(0.0)
                # assert itm["ypol_offset_ns"] should be zero??
                if self._negate_beam_delay:
                    coeff_lookup[(stn, sstn)] = [-c for c in coeffs]
                else:
                    coeff_lookup[(stn, sstn)] = copy.deepcopy(coeffs)

                # Do we need to make pst-beam coefficients into differences?
                if not self._differential_beam_delay:
                    d_coeff = [0.0] * 6
                    for idxx, cff in enumerate(coeff_lookup[(stn, sstn)]):
                        d_coeff[idxx] = cff - stn_coeffs[(stn, sstn)][idxx]
                    coeff_lookup[(stn, sstn)] = d_coeff

            # create list of delays with stations added in order
            delay_words = []
            for stn_sstn in stn_order:
                stn_coeffs = coeff_lookup[stn_sstn]
                nxt_words = PstPolyCoeffs(stn_coeffs).to_words()
                delay_words.extend(nxt_words)

            # Write list to registers
            self.fpga.write_dly_for_beam(idx, bufno, beam_idx, delay_words)

            # update validity time and set valid
            first_poly = pipe.pst_bm_subs[0].get_polys_avail()[bufno]
            validity = PstValidity(
                first_poly.first_valid_blk_no,
                first_poly.valid_blks,
                round(first_poly.offset_sec * 1_000_000_000),  # as nsec
            ).to_words()
            self.fpga.write_dly_validity_for_beam(idx, bufno, validity)

    def _pst_subscriptions_all_updated(self, pipe) -> bool:
        """Check if all PST subscriptions were updated and have same
        start-of-validity time (ie ready to be written to registers)
        """
        # Perhaps none of the subscriptions were updated
        num_subs_updated = sum([1 for updat in pipe.was_pst_updated if updat])
        if num_subs_updated == 0:
            return False

        # At least one subcription was updated: check that all were
        # updated by ensuring all have same pair of start-of-validity values
        start_of_valid_template = None
        for dly_obj in pipe.pst_bm_subs:
            polys_avail = dly_obj.get_polys_avail()
            # get subscription's pair of start-of-valids as a list
            sov_pair = []
            for poly in polys_avail:
                if poly is None:
                    sov_pair.append(None)
                    continue
                sov_pair.append(poly.first_valid_blk_no)
            # save first one found as the comparison reference
            if start_of_valid_template is None:
                start_of_valid_template = sov_pair
            # Not all updated if we found different to the reference
            if sov_pair != start_of_valid_template:
                return False
        return True

    def _check_polys_valid(self, idx: int, pipe):
        """
        Check whether there are valid station-beam delay polynomials and
        enable or disable the packetiser to output beam data

        :param idx: which signal processing pipeline to check
        """
        # Check if station beam has valid delay poly
        ct1_output_time = pipe.ct1_hbm_blk_no - self.CT1_LAG
        dly_obj1 = pipe.stn_bm_dly_sub
        stn_bm_valid = self._has_valid_poly(ct1_output_time, dly_obj1)
        pipe.beam_polys_valid = stn_bm_valid

        # Check if PST beams have valid delay polys
        # (Only check one since all are updated simulataneously)
        ct2_output_time = min(pipe.ct2_hbm_blk_nos)
        dly_obj2 = pipe.pst_bm_subs[0]
        pst_bm_valid = self._has_valid_poly(ct2_output_time, dly_obj2)
        pipe.pst_bm_polys_valid = pst_bm_valid

    def _has_valid_poly(self, ct_output_time, dly_obj) -> bool:
        """
        Check if the delay object contains polynomials that are valid for the
        corner turner output time. Delay object polys are the polys currently
        in the two FPGA buffers. valid if current corner turn time is
        after their start-of-validity and before their expiry

        :param ct_output_time: Current corner turn output time (in blocks)
        :param dly_obj: Delay subscription object (DelayPolySubscriber class)
        """
        polys_avail = dly_obj.get_polys_avail()
        # Valid if either of the delay polys in polys_avail is valid
        stn_bm_valid = False
        for poly_info in polys_avail:
            if poly_info is None:
                continue
            start_time = (poly_info.epoch * self.FPS_NUMER) / self.FPS_DENOM
            valid_t = (poly_info.valid_secs * self.FPS_NUMER) / self.FPS_DENOM
            if start_time <= ct_output_time <= (start_time + valid_t):
                stn_bm_valid = True
                break
        return stn_bm_valid

    def _update_delay_stats(self):
        """Update Tango attribute with delay polynomial details"""
        subarr_info = []
        sb_idx_map = {}  # map subarray ID to index in the above list
        for pipe in self._pipelines:
            if not pipe:
                continue
            # make list of start-of-validity epoch seconds
            epochs_programmed = []
            earliest_poly_idx = None
            fpga_polys = pipe.stn_bm_dly_sub.get_polys_avail()
            for i, poly_info in enumerate(fpga_polys):
                if poly_info is None:
                    epochs_programmed.append(None)
                    continue
                start_time = poly_info.epoch
                epochs_programmed.append(start_time)
                # Is this polynomial the earliest epoch (ie likely in-use)
                if (
                    earliest_poly_idx is None
                    or start_time < epochs_programmed[earliest_poly_idx]
                ):
                    earliest_poly_idx = i
            # make list of delay values for each station in subarray
            # corresponding to the start of the earliest delay poly (ie in use)
            stn_delay_ns = []
            if earliest_poly_idx is not None:
                poly_in_use = fpga_polys[earliest_poly_idx]
                self.logger.info(
                    f"USING:{poly_in_use.epoch}  PKT:{pipe.highest_pkt_secs:.1f}  VALID:{pipe.beam_polys_valid}"
                )
                for stn_dly_info in poly_in_use.poly:
                    stn_dly = {
                        # TODO: shouldn't depend on polynomial internal names
                        "stn": stn_dly_info["station_id"],
                        "ns": stn_dly_info["xypol_coeffs_ns"][0],
                    }
                    stn_delay_ns.append(stn_dly)
            # copy details to published structure
            # structure is list-of-dicts, one dict in list per subarray
            subarray_id, beam_id = pipe.subarray_id, pipe.beam_id
            if subarray_id not in sb_idx_map:
                # a new entry for subarray not seen so far
                subarr_info.append({"subarray_id": subarray_id, "beams": []})
                # map subarray to last entry in top level list (subarr_info)
                sb_idx_map[subarray_id] = len(subarr_info) - 1
            list_index = sb_idx_map[subarray_id]
            subscription_ok = self._subscr_monitor.is_ok(subarray_id, beam_id)
            beams = subarr_info[list_index]["beams"]
            beams.append(
                {
                    "beam_id": beam_id,
                    "valid_delay": pipe.beam_polys_valid,
                    "subscription_valid": subscription_ok,
                    "delay_start_secs": epochs_programmed,
                    "stn_delay_ns": stn_delay_ns,
                }
            )
            beams[-1]["pst"] = []
            pst_info = beams[-1]["pst"]
            for i, subscriber in enumerate(pipe.pst_bm_subs):
                pst_info.append(
                    {
                        "name": pipe.pst_bm_names[i],
                        "valid_delay": pipe.pst_bm_polys_valid,
                        "subscription_valid": not subscriber.failed,
                    }
                )

        # update Tango attribute via the callback
        self._subarray_stats_cbk(subarr_info)

    def _update_subscription_valid(self):
        """Keep track of delay polynomial subscription failures and recoveries"""
        modified = False
        for pipeline in self._pipelines:
            if (
                pipeline is None
                or pipeline.subarray_id not in self._subs_scanning
                or pipeline.stn_bm_dly_sub is None
            ):
                continue
            subarray, beam = pipeline.subarray_id, pipeline.beam_id
            if pipeline.stn_bm_dly_sub.failed:
                modified |= self._subscr_monitor.add(subarray, beam)
                continue
            # otherwise clear this subarray-beam in case it failed previously
            modified |= self._subscr_monitor.remove(subarray, beam)
        # push the change to interested parties
        if modified:
            self._subscr_stats_cbk(self._subscr_monitor.get())

    def cleanup(self):
        """
        Final actions before thread exit
        """
        # Set all pipelines unused to stop any processing that may be
        # happening in FPGA and then deconfigure all pipelines
        self._program_regs({}, [{}] * self.fpga.num_sigproc_instances())

    def _program_regs(self, intl_subarray_info: dict, intl_alveo_defs: list):
        """
        Handle changes in configured and scanning subarrays.
        Steps are:
        - Configure: prepare pipeline for use (subscribe to delays, clear regs)
        - StartScan: FPGA pipeline regs programmed, output packets flow
        - Stopscan: FPGA pipeline regs cleared, output packets stopped
        - Deconfigure: delays are unsubscribed, etc

        Note: each PST pipeline handles only ONE subarray (not like correlator)

        :param intl_subarray_info: dict of all subarray info from allocator's
            "internal_subarray" atrribute
        :param intl_alveo_defs: list of PST beam definition dictionaries
            from allocator's "internal_alveo" attribute. Each list entry
            is either empty or contains keys:
                "subarray_id"
                "pst_beam_id"
                "delay_poly"
                "jones"
                "destinations"
                ( and more: "stn_weights", "rfi_enable", ...)
        """

        subs_configured, subs_scanning_now = get_subarray_lists(
            intl_subarray_info
        )
        self.logger.info("Subarrays configured: %s", subs_configured)
        self.logger.info("Subarrays scanning: %s", subs_scanning_now)
        self.logger.info("PST pipeline descriptions: %s", intl_alveo_defs)

        # Check for stop-scan, or end-configuration conditions
        for index in range(0, len(intl_alveo_defs)):
            subarray_id = get_internal_subarray_id(intl_alveo_defs, index)

            # Pipeline is configured but needs to stop scanning
            if subarray_id not in subs_scanning_now:
                if (self._pipelines[index] is not None) and self._pipelines[
                    index
                ].is_scanning:
                    # Turn off FPGA
                    self.stop_pipeline_scan(index)
                    self._pipelines[index].is_scanning = False

            # pipeline not allocated to a subarray, needs deconfiguration
            if (subarray_id is None) and (self._pipelines[index] is not None):
                self.deconfigure_pipeline(index)

        # Check for configuration or start-scan conditions
        for index in range(0, len(intl_alveo_defs)):
            subarray_id = get_internal_subarray_id(intl_alveo_defs, index)
            if subarray_id is None:
                continue

            # pipeline allocated to a subarray, but needs to be configured
            if self._pipelines[index] is None:
                self.configure_pipeline(
                    intl_subarray_info, intl_alveo_defs, index
                )

            # Pipeline is configured, but needs to start scanning
            if (subarray_id in subs_scanning_now) and not self._pipelines[
                index
            ].is_scanning:
                # Turn on FPGA
                self.start_pipeline_scan(
                    intl_subarray_info,
                    index,
                    subarray_id,
                    self._pipelines[index],
                )
                self._pipelines[index].is_scanning = True

        self._subs_scanning = subs_scanning_now

    def start_pipeline_scan(
        self, intl_subarray_info, index, subarray_id, pipe
    ):
        """
        Configure one of the signal-processing pipelines to run & produce beams

        :param intl_subarray_info: dict of all subarray info from allocator's
            "internal_subarray" atrribute
        :param index: Which signal processing pipeline to program [0,1,2]
        :param subarray_id: ID of subarray to start
        :param pipe: structure containing pipeline processing parameters
        """

        txt = f"PST pipeline {index} start scan for subarray {subarray_id}"
        self.logger.info(txt)
        # TODO Ensure pipeline is stopped before making changes

        params = pipe.params

        # program VCT entries
        vct_words, self._pipelines[index].vct_freq_stn = pst_vct(
            subarray_id,
            params.stn_bm_id,
            params.freq_ids,
            params.stn_ids,
            params.stn_vct_order,
        )
        first_pkt_mhz = pst_pktzr_freq_config(params.freq_ids)
        first_chans = pst_pktzr_first_chans(params.freq_ids)
        tot_coarse = len(params.freq_ids)
        tot_stns = len(params.stn_ids)
        tot_vchans = tot_coarse * tot_stns

        timing_beam_info = get_station_beam_pst_info(
            intl_subarray_info, subarray_id, params.stn_bm_id
        )
        pktzr_beam_nos = pst_pktzr_beam_nos(timing_beam_info)

        # Initialise pst beam delays in FPGA as invalid & zero coefficient
        # and initialise FPGA Jones as unity with unity station weights
        # (Will be updated as delay polynomials and jones are received)
        if self._clear_delays:
            for buf in range(0, 2):
                self.fpga.write_dly_invalid(index, buf, wait=False)
        for bm in range(0, len(timing_beam_info)):
            if self._clear_delays:
                self.set_zero_pst_dlys(index, bm, tot_stns)
            # self.set_unity_jones(index, bm, tot_vchans)
            self.set_weighted_unity_jones(
                index,
                bm,
                tot_vchans,
                tot_stns,
                params.stn_weights[bm],
                params.stn_vct_order,
            )
        # Mark the unity jones as valid forever
        for buf in range(0, 2):
            self.set_jones_inf_valid(index, buf)
        # write sky frequencies used in pst-delay phase calcs (one per vchan)
        # actually has to be first PST channel
        freq_ghz = []
        for frq in pipe.params.freq_ids:
            freq_ghz.extend(
                [(frq - 108 / 216) * SKA_COARSE_SPACING_GHZ]
                * len(pipe.params.stn_ids)
            )
        self.fpga.write_sky_frqs(index, freq_ghz)

        # configure packetiser
        words1, words2 = mhz_to_first_freq_tbls(first_pkt_mhz)
        # self.logger.info("program packetiser freq map")
        self.fpga.prgm_packetiser_first_freqs(index, words1, words2)
        self.fpga.prgm_packetiser_first_chans(index, first_chans)

        scan_id = get_internal_scan_id(intl_subarray_info, subarray_id)
        pktzr_data = pst_pktzr_config(timing_beam_info, scan_id)
        # self.logger.info("program packetiser")
        self.fpga.prgm_packetiser_beam_nos(index, pktzr_beam_nos)
        self.fpga.disable_and_prgm_packetiser(index, pktzr_data)
        pipe.beam_polys_valid = False

        # configure corner turn
        n_timing_beams = len(timing_beam_info)
        # self.logger.info("program corner turn")
        self.fpga.configure_corner_turn(index, n_timing_beams)
        pipe.ct2_poly_sovs = [None, None]  # no dly polys written yet

        # Finally, start pipeline running
        self.fpga.packetiser_enable(index, True)
        self.fpga.update_vct(
            index, vct_words, tot_coarse, tot_stns, tot_vchans
        )
        self.logger.info("new VCT active")

    def set_zero_pst_dlys(self, idx, bm, num_stns):
        """
        Initialise a pst-beam delay poly to have all zero coefficients
        for all stations
        """
        # (6 x 32-bit floating point zeroes converted to integers)
        zero_delays = PstPolyCoeffs([0.0] * 6).to_words()
        all_zero_delays = zero_delays * num_stns
        for buf in range(0, 2):
            self.fpga.write_dly_for_beam(idx, buf, bm, all_zero_delays)

    def set_unity_jones(self, index, bm, num_vchans):
        """
        Initialise a pst-beam Jones matricies to unity for all stations

        :param index: fpga signal processing pipeline number [0,1,2]
        :param bm: pst beam index [0..number_of_pst_beams-1]
        :param num_vchans: number of virtual channels being beamformed
        """
        jones_unity = [
            0x00007FFF,
            0x00000000,
            0x00000000,
            0x00007FFF,
        ] * num_vchans

        wts_unity = [0x8000] * num_vchans

        for buf in range(0, 2):
            self.fpga.write_jones_for_beam(index, buf, bm, jones_unity)
            self.fpga.write_weights_for_beam(index, buf, bm, wts_unity)

    def set_weighted_unity_jones(
        self, index, bm, tot_vchans, tot_stns, stn_weights, stn_order
    ):
        """
        Initialise a pst-beam Jones matricies to unity for all stations

        :param index: fpga signal processing pipeline number [0,1,2]
        :param bm: pst beam index [0..number_of_pst_beams-1]
        :param tot_vchans: number of virtual channels being beamformed
        :param tot_stns: number of stations being beamformed
        :param stn_weights: list of station weights
        :param stn_order: where in list each weight goes
        """
        self.logger.info("beam_idx %d stn_wts: %s", bm, str(stn_weights))
        self.logger.info("stn_order: %s", str(stn_order))
        # if no weights supplied, use all unity weights
        if stn_weights is None:
            stn_weights = [1.0] * tot_stns
        # normalise so maximum weight is 1.0
        max_weight = max(stn_weights)
        rel_wts = [wt / max_weight for wt in stn_weights]
        # extend weights using last weight value in list if necessary
        if len(rel_wts) < tot_stns:
            rel_wts.extend([rel_wts[-1]] * (tot_stns - len(rel_wts)))

        jones_unity = [
            0x00007FFF,
            0x00000000,
            0x00000000,
            0x00007FFF,
        ]
        wts_unity = 0x8000

        jones = []
        wts = []
        for _ in range(0, tot_vchans // tot_stns):  # for each coarse channel
            # FPGA expects station weights in VCT ordering
            for stn_idx in stn_order:
                jval = [round(ju * rel_wts[stn_idx]) for ju in jones_unity]
                jones.extend(jval)
                wts.append(round(wts_unity * rel_wts[stn_idx]))

        for buf in range(0, 2):
            self.fpga.write_jones_for_beam(index, buf, bm, jones)
            self.fpga.write_weights_for_beam(index, buf, bm, wts)

    def set_jones_inf_valid(self, sigproc_no: int, bufno: int) -> None:
        """
        Set Jones validity info to indicate Jones data is valid forever

        :param sigproc_no: the signal processing pipeline number [0,1,2]
        :param bufno: the jones buffer number [0,1]
        """
        inf_valid = JonesValidity(0, 0xFFFF_FFFF).to_words()
        self.fpga.write_jones_validity_for_buffer(sigproc_no, bufno, inf_valid)

    def stop_pipeline_scan(self, index):
        """
        Stop a pipeline processing scan data by updating FPGA registers
        """
        self.logger.info("PST pipeline %s stop scanning", str(index))
        vct_words, self._pipelines[index].vct_freq_stn = zero_vct()
        self.fpga.update_vct(index, vct_words, 0, 0, 0)
        # wait for stop before disabling packetiser
        _ = self.fpga.wait_for_flip_get_next_buf(index)
        self.fpga.packetiser_enable(index, False)
        self.logger.info("zeroed VCT: inactive")

    def configure_pipeline(self, intl_subarray, intl_alveo, index):
        """
        Prepare a pipeline for use by a subarray (before scanning)
        by retrieving pipeline params & subscribing to polynomials
        """
        pipe_params = get_pipeline_params(intl_alveo, intl_subarray, index)
        new_pipe = PipelineState(pipe_params)
        # Zero VCSTATS TODO

        # Subscribe to the station-beam delay polynomial
        dev, attr = pipe_params.stn_bm_dly_src.rsplit("/", 1)
        new_pipe.stn_bm_dly_sub = DelayPolySubscriber(self.logger)
        new_pipe.stn_bm_dly_sub.subscribe(dev, attr)
        new_pipe.stn_bm_dly_sub_name = pipe_params.stn_bm_dly_src
        self.logger.info(
            "pipe %d subscribing stn beam delay %s/%s",
            index,
            dev,
            attr,
        )
        # subscribe to the pst-beam polynomials
        for cnt, pst_dly_uri in enumerate(pipe_params.pst_dly_srcs):
            new_pipe.pst_bm_subs.append(DelayPolySubscriber(self.logger))
            dev, attr = pst_dly_uri.rsplit("/", 1)
            new_pipe.pst_bm_subs[cnt].subscribe(dev, attr)
            new_pipe.pst_bm_names.append(pst_dly_uri)
            new_pipe.was_pst_updated.append(False)
            self.logger.info(
                "pipe %d subscribing PST beam delay %s/%s",
                index,
                dev,
                attr,
            )
        # add the new pipeline parameters structure
        self._pipelines[index] = new_pipe

    def deconfigure_pipeline(self, index):
        """
        Pipeline no longer used by any subarray. Deconfigure by unsubscribing
        from all delay polynomials and deleting the pipeline params
        """
        if self._pipelines[index] is None:
            self.logger.warning("Deconfigure non-configured pipe[%d]", index)
            return
        pipeline = self._pipelines[index]
        # remove health monitoring of attributes
        subarray, beam = pipeline.subarray_id, pipeline.beam_id
        if self._subscr_monitor.remove(subarray, beam):
            self._subscr_stats_cbk(self._subscr_monitor.get())
        # unsubscribe from station-beam polynomials
        pipeline.stn_bm_dly_sub.unsubscribe()
        pipeline.stn_bm_dly_sub = None
        self.logger.info("pipe %d unsubscribed stn-beam delay", index)
        # unsubscribe from pst-beam polynomials
        for cnt, sub in enumerate(pipeline.pst_bm_subs):
            if sub is not None:
                sub.unsubscribe()
                sub = None
                self.logger.info(
                    "pipe %d unsubscribed PST delay: %s",
                    index,
                    pipeline.pst_bm_names[cnt],
                )
        # Free pipeline parameters struct for garbage collection
        self._pipelines[index] = None

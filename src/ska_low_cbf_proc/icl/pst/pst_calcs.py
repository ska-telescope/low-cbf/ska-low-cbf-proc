# -*- coding: utf-8 -*-
#
# (c) 2023 CSIRO Space and Astronomy.
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement
# See LICENSE.txt for more info.

import socket
import struct

VCT_ENTRIES = 1024
VCT_WORDS_PER_ENTRY = 2
VCT_VALID_ENTRY = 0x80000000
PST_FREQ_TABLE_LEN = 2048  # entries in beam-redirect and frequency tables
PST_FREQ_TABLE_BLK = 16  # size of each coarse-chan block in the tables
PST_BEAMID_TABLE_ENTRIES = 16  # slots in FPGA's "beam rewrite" table
PST_UDP_SRC_PORT = 2122
PST_UDP_DEST_MARKER = 9510  # This value marks PST packets for P4 routing
PST_SPACING_MILLIHZ = int(round((800e6 * 32 * 1e3) / (1024 * 27 * 256), 0))
PST_CHANS_PER_COARSE = int(
    256 * 27 / 32
)  # 256 chan PST filterbank, but only central non-overlapped chans kept
SPS_COARSE_SPACING_MILLIHZ = 781_250_000  # SPS coarse channel spacing
PST_MAGIC_WORD = 0xBEADFEED  # As advised by PST team, not in ICD
# Calculate UDP length field value
UDP_HDR_BYTES = 8
PST_METADATA_BYTES = 96
PST_CHANS_IN_PKT = 24
PST_SAMPLS_IN_PKT = 32
PST_BYTES_PER_SAMPL = 8
PST_BYTES_WEIGHTS = 2
PST_UDP_LEN_BYTES = (
    UDP_HDR_BYTES
    + PST_METADATA_BYTES
    + PST_CHANS_IN_PKT
    * (PST_BYTES_WEIGHTS + PST_SAMPLS_IN_PKT * PST_BYTES_PER_SAMPL)
)


def pst_vct(
    subarray_id: int,
    stn_bm_id: int,
    freq_ids: list[int],
    sstns: list,
    stn_vct_order: list,
) -> tuple[list, list]:
    """
    Calculate PST VCT table entries for one of the FPGA pipelines

    :return: list of VCT entries and list of corresponding frequencies
    """
    vct_vals = []
    next_avail_vchan = 0
    vct_frq_stn = []

    # FPGA logic depends on order of these two loops
    for frq_id in freq_ids:
        for idx in stn_vct_order:
            stn, sstn = sstns[idx]
            val = (
                VCT_VALID_ENTRY
                | ((subarray_id & 0x1F) << 26)  # 5 bits
                | ((frq_id & 0x1FF) << 17)  #
                | ((stn_bm_id & 0xF) << 13)
                | ((stn & 0x3FF) << 3)
                | (sstn & 0x7)
            )
            vct_vals.append(val)  # First word of a VCT entry
            vct_vals.append(next_avail_vchan)  # Second word of a VCT entry
            vct_frq_stn.append((frq_id, stn, sstn))
            next_avail_vchan += 1

    # fill any remaining VCT space with zero ("invalid entry")
    remaining_words = VCT_ENTRIES * VCT_WORDS_PER_ENTRY - len(vct_vals)
    if remaining_words > 0:
        vct_vals.extend([0x0] * remaining_words)

    return vct_vals, vct_frq_stn


def zero_vct():
    """
    Get the representation of zeroed VCT table entries
    """
    vct_vals = []
    vct_words = [0] * VCT_ENTRIES * VCT_WORDS_PER_ENTRY
    return vct_vals, vct_words


def pst_pktzr_freq_config(freq_ids: list[int]) -> list[int]:
    """
    Calculate programming for packetiser "first chan freq redir ram"
    ie the frequency in millihertz of the first channel in each PST packet
    that will be output at a timestep

    216 pst channels per coarse channel, 24 pst channels per packet ->
    9 packets for each coarse channel being processed
    -> 9 entries in table per coarse channel
    :return: list of VCT frequencies in mHz
    """
    pst_pkt_per_coarse = int(PST_CHANS_PER_COARSE / PST_CHANS_IN_PKT)
    pst_pkt_freq_mhz = []
    for coarse_chan_id in freq_ids:
        for pst_pktno in range(0, pst_pkt_per_coarse):
            mhz = int(
                round(
                    coarse_chan_id * SPS_COARSE_SPACING_MILLIHZ
                    + (
                        pst_pktno * PST_CHANS_IN_PKT
                        - (PST_CHANS_PER_COARSE / 2)
                    )
                    * PST_SPACING_MILLIHZ,
                    0,
                )
            )
            pst_pkt_freq_mhz.append(mhz)
        # fill unused region with error indicator
        start_loc = len(pst_pkt_freq_mhz)
        unused_addr_space = [
            0xEEEE_0000_0000_0000 + (start_loc + idx)
            for idx in range(0, PST_FREQ_TABLE_BLK - pst_pkt_per_coarse)
        ]
        pst_pkt_freq_mhz.extend(unused_addr_space)
    # fill unused end of table with error indicators
    remaining_words = PST_FREQ_TABLE_LEN - len(pst_pkt_freq_mhz)
    if remaining_words > 0:
        start_loc = len(pst_pkt_freq_mhz)
        unused_addr_space = [
            0xEEEE_0000_0000_0000 + (start_loc + idx)
            for idx in range(0, remaining_words)
        ]
        pst_pkt_freq_mhz.extend(unused_addr_space)
    return pst_pkt_freq_mhz


def pst_pktzr_first_chans(freq_ids: list[int]) -> list:
    """
    Calculate programming for packetiser "first chan number"
    ie the first channel number in each PST packet
    that will be output at a timestep

    216 pst channels per coarse channel, 24 pst channels per packet ->
    9 packets for each coarse channel being processed
    -> 9 entries in table per coarse channel
    :return: list of pst channel numbers (chan 0 is first in coarse 64)
    """
    pst_pkt_per_coarse = int(PST_CHANS_PER_COARSE / PST_CHANS_IN_PKT)
    pst_pkt_chan_nos = []
    for coarse_chan_id in freq_ids:
        for pst_pktno in range(0, pst_pkt_per_coarse):
            chan_no = (
                coarse_chan_id - 64
            ) * PST_CHANS_PER_COARSE + pst_pktno * PST_CHANS_IN_PKT
            pst_pkt_chan_nos.append(chan_no)
        # fill unused region with error indicator
        start_loc = len(pst_pkt_chan_nos)
        unused_addr_space = [
            0xE000 + (start_loc + idx)
            for idx in range(0, PST_FREQ_TABLE_BLK - pst_pkt_per_coarse)
        ]
        pst_pkt_chan_nos.extend(unused_addr_space)
    # fill unused end of table with error indicators
    remaining_words = PST_FREQ_TABLE_LEN - len(pst_pkt_chan_nos)
    if remaining_words > 0:
        start_loc = len(pst_pkt_chan_nos)
        unused_addr_space = [
            0xE000 + (start_loc + idx) for idx in range(0, remaining_words)
        ]
        pst_pkt_chan_nos.extend(unused_addr_space)
    return pst_pkt_chan_nos


def pst_pktzr_config(tbeam_info, scan_id: int) -> list[int]:
    """
    Calculate packetiser programming words for one of the FPGA pipelines

    Some values come from CBF-PST ICD document: SKA-TEL-CSP-0000291
    arising from ECP-200044 (not yet approved)

    A FPGA pipeline handles one subarray-beam, but there may be multiple
    PST beams formed from the subarray-beam, each with own destination

    :param tbeam_info: list of PstBeam objects defining PST beams
    to be generated by the FPGA pipeline
    :param scan_id: 64-bit integer value
    :return: list of integers for packetiser RAM programming
    (see comments in FPGA packetiser YAML file for order & meaning)
    """
    pktzr_vals = []

    pktzr_vals.append(0xAE92A3C5)  # 0 dest mac lo
    pktzr_vals.append(0xB000946D)  # 1 dest mac hi 94:6d:ae:92:a3:c5
    pktzr_vals.append(0x53547788)  # 2 src mac lo
    pktzr_vals.append(0xC000A250)  # 3 src mac hi
    pktzr_vals.append(0x00000800)  # 4 eth type (to be RO)
    pktzr_vals.append(0x453318AC)  # 5 IPv4 version RO, lth=6316, RO, TOS
    pktzr_vals.append(0x1DEA0000)  # 6 IPv4 ID, flags RO, frag RO
    pktzr_vals.append(0x2F110000)  # 7 IPv4 TTL, proto=UDP, hdr_cksum RO
    pktzr_vals.append(0x0A7BE60C)  # 8 IPv4 Src Addr 10.123.230.12

    # TODO FIXME at present FPGA has only one IP dest per pst-beam. Needs More
    ip_as_int = struct.unpack(
        "!L", socket.inet_aton(tbeam_info[0].server_dests[0].ip_addr)
    )[0]
    pktzr_vals.append(ip_as_int)  # 9 IPv4 Dest Addr

    # PST UDP dest port is used by P4 to identify PST beam packets
    src_dst_port = (PST_UDP_SRC_PORT << 16) + PST_UDP_DEST_MARKER
    pktzr_vals.append(src_dst_port)  # 10 UDP src + dest ports

    pktzr_vals.append(PST_UDP_LEN_BYTES << 16 + 0x0)  # 11 UDP len + UDP cksum
    pktzr_vals.append(0x00000000)  # 12 PST pkt-seq-no hi  TODO: fw calculates?
    pktzr_vals.append(0x00000000)  # 13 PST pkt-seq-no lo  TODO: fw calculates?
    pktzr_vals.append(0x00000000)  # 14 PST timestamp attosec hi  TODO: fw?
    pktzr_vals.append(0x00000000)  # 15 PST timestamp attosec lo  TODO: fw?
    pktzr_vals.append(0x00000000)  # 16 PST timestamp sec  TODO: fw calculates?
    pktzr_vals.append(PST_SPACING_MILLIHZ)  # 17 PST chanl spacing

    pktzr_vals.append(0x00000000)  # 18 PST first_freq hi (RO)
    pktzr_vals.append(0x00000000)  # 19 PST first_freq lo (RO)
    pktzr_vals.append(0x00000000)  # 20 PST scale_1 (float, voltage scale, RO)
    pktzr_vals.append(0x00000000)  # 21 PST scale_2 unused
    pktzr_vals.append(0x00000000)  # 22 PST scale_3 unused
    pktzr_vals.append(0x00000000)  # 23 PST scale_4 unused
    pktzr_vals.append(0x00000000)  # 24 PST first_chanl (RO)
    pktzr_vals.append(0x00000000)  # 25 PST chan_per_pkt (RO)
    pktzr_vals.append(0x00000000)  # 26 PST valid_chan_per_pkt (RO)
    pktzr_vals.append(0x00000000)  # 27 PST no_of_time_samples (RO)

    beam_no = tbeam_info[0].pst_beam_id
    pktzr_vals.append(beam_no)  # 28 PST beam TODO FIXME need to allow >1

    pktzr_vals.append(PST_MAGIC_WORD)  # 29 PST magic word
    pktzr_vals.append(0x00000002)  # 30 PST packet dest (ICD says: pst_low = 2)
    pktzr_vals.append(0x00000010)  # 31 PST data precision (ICD says 16)
    pktzr_vals.append(
        0x00000000
    )  # 32 PST pwr samples averaged (MID only, unused)
    pktzr_vals.append(
        0x00000020
    )  # 33 PST time samples per weight (ICD says 32)
    pktzr_vals.append(0x00000004)  # 34 PST oversampling numerator (RO)
    pktzr_vals.append(0x00000003)  # 35 PST oversampling denominator (RO)
    pktzr_vals.append(0x00000000)  # 36 PST beamformer version (RO)
    scan_hi = (scan_id >> 32) & 0x0_FFFF_FFFF
    pktzr_vals.append(scan_hi)  # 37 PST scan_id hi
    scan_lo = scan_id & 0x0_FFFF_FFFF
    pktzr_vals.append(scan_lo)  # 38 PST scan_id lo
    pktzr_vals.append(0x00000000)  # 39 PST stokes offset_1 (PSS only, unused)
    pktzr_vals.append(0x00000000)  # 40 PST stokes offset_2 (PSS only, unused)
    pktzr_vals.append(0x00000000)  # 41 PST stokes offset_3 (PSS only, unused)
    pktzr_vals.append(0x00000000)  # 42 PST stokes offset_4 (PSS only, unused)
    pktzr_vals.append(0x00000000)  # 43 pkt gen no-to-send (unused)
    pktzr_vals.append(0x00000000)  # 44 PST gen cyc-btw-pkts (unused)
    pktzr_vals.append(0x00000000)  # 45 PST gen beams-to-create (unused)

    return pktzr_vals


def pst_pktzr_beam_nos(tbeam_info) -> list[int]:
    """
    Calculate packetiser programming words for the "beam rewrite" table
    of one of the FPGA pipelines. Each entry indicates the PST beam number
    to attach to packets for that beam

    :param tbeam_info: list of PstBeam objects defining each PST beam
    """
    n_beams = len(tbeam_info)
    beam_nos_table = []
    for idx in range(0, min(PST_BEAMID_TABLE_ENTRIES, n_beams)):
        beam_nos_table.append(tbeam_info[idx].pst_beam_id)
    if n_beams < PST_BEAMID_TABLE_ENTRIES:
        beam_nos_table.extend([0] * (PST_BEAMID_TABLE_ENTRIES - n_beams))

    return beam_nos_table


def mhz_to_first_freq_tbls(vct_mhz: list[int]) -> tuple[list[int], list[int]]:
    """
    Split the 64-bit VCT first frequency table into two 32-bit tables in prep
    for programming into the two 32-bit FPGA packetiser tables

    :param vct_mhz: Frequency in milli-Hertz of first PST channel in the
    coarse channel that each VCT entry handles
    :return: two tables containing hi-32-bits and lo-32-bits
    """
    words_low = []  # low 32-bits of 64-bit first channel milliHertz value
    words_hi = []  # high 32-bits of 64-bit milliHertz value
    for freq in vct_mhz:
        words_low.append(freq & 0x0_FFFF_FFFF)
        words_hi.append((freq >> 32) & 0x0_FFFF_FFFF)
    return (words_low, words_hi)

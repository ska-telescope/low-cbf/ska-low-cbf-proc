# -*- coding: utf-8 -*-
#
# Copyright (c) 2022 CSIRO Space and Astronomy.
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more information.
"""
Interface to Pulsar Timing (PST) FPGA Image

This file exists to confine PST register names and data structures to one file
It provides methods to read and write the registers in any of the
three signal processing pipelines in a PST FPGA
"""
import struct
import time
from dataclasses import dataclass

import numpy as np
from ska_low_cbf_fpga import FpgaPersonality
from ska_low_cbf_fpga.args_fpga import WORD_SIZE

from ska_low_cbf_proc.icl.no_attributes import NoAttributes
from ska_low_cbf_proc.icl.no_data import NoData
from ska_low_cbf_proc.icl.pst.packetiser import Packetiser


@dataclass
class PstValidity:
    """
    Structure holding values used in FPGA regs for PST polynomial validity
    """

    start_valid: int
    """Time at which item becomes valid. Units are (currently 53msec) frames"""
    valid_length: int
    """How long item is valid. Units are frames"""
    ns_offset: int
    """True start-of-validity to start_valid in nanoseconds (non-negative)"""

    def to_words(self) -> list[int]:
        """Get fpga representation of validity in words for the registers"""
        return [
            self.start_valid & 0x0_FFFF_FFFF,
            self.start_valid >> 32 & 0x0_FFFF,
            self.valid_length & 0x0_FFFF_FFFF,
            self.ns_offset & 0x0_FFFF_FFFF,
        ]


@dataclass
class JonesValidity:
    """
    Structure holding values used in FPGA regs for Jones validity
    """

    start_valid: int
    """Time at which item becomes valid. Units are (currently 53msec) frames"""
    valid_length: int
    """How long item is valid. Units are frames"""

    def to_words(self) -> list[int]:
        """Get fpga representation of validity in words for the registers"""
        return [
            self.start_valid & 0x0_FFFF_FFFF,
            self.start_valid >> 32 & 0x0_FFFF,
            self.valid_length & 0x0_FFFF_FFFF,
        ]


@dataclass
class PstPolyCoeffs:
    """
    Structure holding delay polynomial coefficients for use in PST FPGA regs
    """

    coeffs: list[float]
    """six delay polynomial coefficients in order [t^0, t^1, ... t^5]"""

    def to_words(self) -> list[int]:
        """Get FPGA representation of PST delay polynomial coefficients"""
        as_bytes = struct.pack("<ffffff", *self.coeffs)
        as_ints = struct.unpack("IIIIII", as_bytes)
        return as_ints


class PstFpga(FpgaPersonality):
    """
    Class providing access to Pulsar Timing (PST) FPGA by translating
    high-level operations (such as updating VCT) into lower-level register
    reads and writes

    The implementation of the class methods depends intimately on the
    register names in the FPGA VHDL, plus the function and interaction of
    the registers in the VHDL design. If the VHDL design changes, this code
    may also need to change
    """

    _peripheral_class = {
        "packetiser": Packetiser,
        "filterbanks": NoData,
        "pstbeamformer_dp": NoData,
        # "pstbeamformer_dp_2": NoData,
        # "pstbeamformer_dp_3": NoData,
        "vitis_shared": NoData,
        # One ingest block for each of 3 signalprocessing paths
        "lfaadecode100g": NoData,
        # "lfaadecode100g_2": NoData,
        # "lfaadecode100g_3": NoData,
        "cmac_b": NoAttributes,
        "drp": NoAttributes,
    }

    _not_user_methods = {
        "num_sigproc_instances",
        "read_packet_counts",
    }
    """These have returns that aren't compatible with FPGA values"""

    PKTZR_EN_BIT = 0x01
    PKTZR_BEAM_TBL_BASE = 512 * WORD_SIZE
    FRQ_MAP_LO_BASE = 2048 * WORD_SIZE  # low 32-bits of freq word
    FRQ_MAP_HI_BASE = 4096 * WORD_SIZE  # hi 32-bits of freq word
    FIRST_CHAN_TBL_BASE = 6144 * WORD_SIZE  # pkt first chnl no
    POLY_BUF_SIZE = 20 * 1024 * WORD_SIZE  # 20 words per vchan

    # Address offsets between beamformers
    BF_OFFSET_BYTES = 64 * 1024

    # Address offsets for Jones & Weights in each beamformer
    JONES0_OFFSET_BYTES = 0
    JONES1_OFFSET_BYTES = JONES0_OFFSET_BYTES + 16 * 1024
    WEIGHT0_OFFSET_BYTES = JONES1_OFFSET_BYTES + 16 * 1024
    WEIGHT1_OFFSET_BYTES = WEIGHT0_OFFSET_BYTES + 2 * 1024

    # Address offsets for PST delay polynomial buffers
    DLY0_OFFSET_BYTES = 0
    DLY1_OFFSET_BYTES = 16 * 512 * 24  # 16beams, 512stns, 24bytes/poly
    FRQ_OFFSET_BYTES = DLY1_OFFSET_BYTES + 16 * 512 * 24

    FPGA_VCT_ENTRIES = 1024
    """Number of entries in FPGA's Virtual Channel Table"""
    WORDS_PER_VCT_ENTRY = 2
    """Number of 32-bit words for each VCT entry in FPGA"""
    INGEST_STATS_WORDS_PER_VCHAN = 8
    """Number of 32-bit words in each VCT Stats entry"""
    INGEST_STATS_PKT_CNT_LO_OFFSET = 4
    """Which word in VCT Stats is the packet count Low 32-bits"""
    INGEST_STATS_PKT_CNT_HI_OFFSET = 5
    """Which word in VCT Stats is the packet count High 16 bits"""
    VCT_BASE = 8192
    """VCT offset within VCSTATS RAM"""

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        # Suffixes for the 3x replicated sigproc chains in the PST ARGS/VHDL
        self._instance_suffixes = ["", "_2", "_3"]
        self._scan = [False for _ in self._instance_suffixes]

        # TODO check all expected peripherals are present

        # vct addresses we'll access
        self._vct_addr = []  # base of VirtualChannelTables
        self._vcstats_addr = []  # base of VC stats
        self._tot_stns = []
        self._tot_coarse = []
        self._tot_chans = []
        self._lfaa_reset = []
        self._pktzr_cfg_addr = []
        self._pktzr_en_addr = []  # packetiser control vector
        self._ct_out_beamsenabled = []
        self._stn_beam_poly_addr = []  # station beam polynomial data
        self._ct1_framecount_addr = []  # CT1 frame counter
        self._ct1_table_sel = []
        self._last_table_flip_time = []  # time prior table flip was started
        self._ct1_input_err = []
        self._ct2_bof_err = []
        self._ct2_rd_err = []
        self._ct2_framecounts_addr = []  # CT2 HBM frame counters
        self._beamformer_addr = []  # PST beam Jones and Weights
        self._jones0_validity_info_addr = []  # Jones validity data
        self._jones1_validity_info_addr = []
        self._poly0_valid_addr = []
        self._poly0_validity_info_addr = []  # pst beam validity data
        self._poly1_valid_addr = []
        self._poly1_validity_info_addr = []  # pst beam validity data
        self._pst_poly_addr = []  # PST beam polynomial data

        for suffix in self._instance_suffixes:
            periph_name = f"lfaadecode100g{suffix}"
            periph = self[periph_name]

            self._vcstats_addr.append(periph["data"].address)
            vct_addr = periph["data"].address + WORD_SIZE * self.VCT_BASE
            self._vct_addr.append(vct_addr)
            self._tot_stns.append(
                [
                    periph["total_stations_table0"].address,
                    periph["total_stations_table1"].address,
                ]
            )
            self._tot_coarse.append(
                [
                    periph["total_coarse_table0"].address,
                    periph["total_coarse_table1"].address,
                ]
            )
            self._tot_chans.append(
                [
                    periph["total_channels_table0"].address,
                    periph["total_channels_table1"].address,
                ]
            )
            self._lfaa_reset.append(periph["lfaa_decode_reset"].address)

            periph_name = f"pst_ct1{suffix}"
            periph = self[periph_name]
            self._ct1_framecount_addr.append(periph["frame_count_low"].address)
            self._stn_beam_poly_addr.append(periph["data"].address)
            self._ct1_table_sel.append(periph["table_select"].address)
            self._last_table_flip_time.append(time.time())
            self._ct1_input_err.append(periph["error_input_overflow"].address)

            periph_name = f"pstbeamformer_dp{suffix}"
            periph = self[periph_name]
            self._beamformer_addr.append(periph["data"].address)

            periph_name = f"ct2{suffix}"
            periph = self[periph_name]
            self._ct_out_beamsenabled.append(periph["beamsenabled"].address)
            self._poly0_valid_addr.append(
                periph["poly_buffer0_info_valid"].address
            )
            self._poly1_valid_addr.append(
                periph["poly_buffer1_info_valid"].address
            )
            self._poly0_validity_info_addr.append(
                periph["poly_buffer0_valid_frame_low"].address
            )
            self._poly1_validity_info_addr.append(
                periph["poly_buffer1_valid_frame_low"].address
            )
            self._jones0_validity_info_addr.append(
                periph["jones_buffer0_valid_frame_low"].address
            )
            self._jones1_validity_info_addr.append(
                periph["jones_buffer1_valid_frame_low"].address
            )
            self._ct2_framecounts_addr.append(
                periph["hbmbuf0cornerturncount_low"].address
            )
            self._ct2_bof_err.append(periph["bufferoverflowerror"].address)
            self._ct2_rd_err.append(periph["readouterror"].address)

            # PST beam peripheral instance names have no underscore in suffix!
            periph_name = f"pstbeampolynomials{suffix[1:]}"
            periph = self[periph_name]
            self._pst_poly_addr.append(periph["data"].address)

            periph_name = f"packetiser{suffix}"
            periph = self[periph_name]
            self._pktzr_en_addr.append(periph["control_vector"].address)
            self._pktzr_cfg_addr.append(periph["data"].address)

        # ensure any "used" FPGA that K8s gives us gets properly initialised
        for idx in range(0, len(self._instance_suffixes)):
            self.disable_packetiser(idx)
            # write zero VCT to force FPGA to stop
            words = [0] * self.FPGA_VCT_ENTRIES * self.WORDS_PER_VCT_ENTRY
            self.update_vct(idx, words, 0, 0, 0)
            # wait until actually stopped
            _ = self.wait_for_flip_get_next_buf(idx)
            # reset to bring back to known state (as far as possible)
            self.driver.write(self._lfaa_reset[idx], 1)
            self.driver.write(self._lfaa_reset[idx], 0)
            # all pst-beam delays invalid until updated from subscription
            for buf in range(0, 2):
                self.write_dly_invalid(idx, buf, wait=False)
            # all station-beam delays invalid until updated from subscription
            words_buffer = [[0] * 20 * 1024, [0] * 20 * 1024]
            self.update_beam_delay_polys(words_buffer, idx)

    def __del__(self):
        "Cleanup as Alveo card may be left locked when using XRT driver"
        super().__del__()

    def num_sigproc_instances(self) -> int:
        """Get number of signal processing pipelines in FPGA image"""
        return len(self._instance_suffixes)

    def write_jones_for_beam(
        self, sigproc_inst_no: int, buf: int, pst_beam_idx: int, jones_vals
    ):
        """
        Write Jones values for sigproc and beam to a FPGA buffer

        Each beamformer has two 16kbyte Jones buffers that are located in
        the first 32k of a 36kbyte RAM associated with each beamformer.
        RAM for beamformer N is at offset of N*64kbytes = N*16kWords

        :param buf: Which of the two fpga buffers to write to
        :param pst_beam_count: PST beam to program, range 0 to N_PST_BEAMS-1
        :param jones_vals: Jones constants (incl weights) to program
        """
        jvals_numpy = np.asarray(jones_vals, dtype=np.uint32)

        # Select which buffer to write to
        offset = pst_beam_idx * self.BF_OFFSET_BYTES + self.JONES0_OFFSET_BYTES
        if buf != 0:
            offset = (
                pst_beam_idx * self.BF_OFFSET_BYTES + self.JONES1_OFFSET_BYTES
            )

        self.driver.write(
            self._beamformer_addr[sigproc_inst_no] + offset, jvals_numpy
        )

    def write_jones_validity_for_buffer(
        self, sigproc_inst_no: int, bufno: int, validity_words: list
    ) -> None:
        """
        Write time-of-validity for Jones buffer

        :param sigproc_inst_no: which signal processing pipeline to write to
        :param buf: Which of the two FPGA Jones buffers to write to
        :param validity_words: 3x 32-bit integers representing validity
        """
        words_numpy = np.asarray(validity_words, dtype=np.uint32)
        if bufno == 0:
            self.driver.write(
                self._jones0_validity_info_addr[sigproc_inst_no], words_numpy
            )
        else:
            self.driver.write(
                self._jones1_validity_info_addr[sigproc_inst_no], words_numpy
            )

    def write_weights_for_beam(
        self, sigproc_inst_no: int, buf: int, pst_beam_idx: int, wts_vals
    ):
        """
        Write relative weights values for a beam of a sigprocessing instance
        to a FPGA buffer

        Each beamformer has two 6kbyte Weights buffers that are located in
        the last 4k of a 36kbyte RAM associated with each beamformer.
        RAM for beamformer N is at offset of N*64kbytes = N*16kWords

        The weights are used to calculate fraction of data stats if flagged

        :param buf: Which of the two fpga buffers to write to
        :param pst_beam_count: PST beam to program, range 0 to N_PST_BEAMS-1
        :param wts_vals: up to 1024 16-bit relative weights (for each vchan)
        """
        # FPGA expects pairs of station weights in each 32-bit register so add
        # a final pad value if there is an odd number of weights
        if len(wts_vals) % 2 != 0:
            wts_vals.append(0)
        # convert to numpy array as required by the FPGA driver
        wts_numpy = np.asarray(wts_vals, dtype=np.uint16)

        offset = (
            pst_beam_idx * self.BF_OFFSET_BYTES + self.WEIGHT0_OFFSET_BYTES
        )
        if buf != 0:
            offset = (
                pst_beam_idx * self.BF_OFFSET_BYTES + self.WEIGHT1_OFFSET_BYTES
            )

        self.driver.write(
            self._beamformer_addr[sigproc_inst_no] + offset,
            # FPGA expects first weight in low 16-bits, next in high 16-bits
            # numpy.view conversion to uint32 for registers works that way
            wts_numpy.view(dtype=np.uint32),
        )

    def write_dly_for_beam(
        self,
        sigproc_inst_no: int,
        buf: int,
        pst_beam_idx: int,
        fpga_words: list[int],
    ) -> None:
        """
        Write Delay values for sigproc and beam one of the FPGA buffers
        This writes delays for all stations in the beam at once

        :param sigproc_inst_no: Which signal processing pipeline [0,1,2]
        :param buf: Which of the two fpga buffers to write to [0,1]
        :param pst_beam_idx: Which PST beam to program [0 to N_PST_BEAMS-1]
        :param fpga_words: Delay polynomial coefficient words (6 words/station)
        """
        dvals = np.asarray(fpga_words, dtype=np.uint32)

        # select which of the FPGA buffers to write to
        offset = self.DLY0_OFFSET_BYTES + pst_beam_idx * 12 * 1024
        if buf != 0:
            offset = self.DLY1_OFFSET_BYTES + pst_beam_idx * 12 * 1024

        self.driver.write(self._pst_poly_addr[sigproc_inst_no] + offset, dvals)

    def write_sky_frqs(
        self,
        sigproc_inst_no: int,
        frqs_ghz: list[float],
    ) -> None:
        """
        Write sky frequencies for a pst beam in a signal processing instance
        Used in calculating phase correction for PST beam steering
        Only one dataset, expected common to both buffers & all beams

        :param sigproc_inst_no: Which signal processing pipeline [0,1,2]
        :param frqs_ghz: sky frequency for each VCHAN in ghz
        """
        # FPGA wants each frequency in the list as 32-bit floating point value
        # with an unused 32-bit word padding after each frequency
        frq_words = []
        for frq in frqs_ghz:
            as_bytes = struct.pack("<fI", frq, 0)
            as_words = struct.unpack("II", as_bytes)
            frq_words.extend(as_words)
        fvals = np.asarray(frq_words, dtype=np.uint32)
        offset = self.FRQ_OFFSET_BYTES
        self.driver.write(self._pst_poly_addr[sigproc_inst_no] + offset, fvals)

    def write_dly_validity_for_beam(
        self,
        sigproc_inst_no: int,
        buf: int,
        fpga_words: list[int],
        set_valid: bool = True,
    ) -> None:
        """
        Write start/length of validity info for one of the polynomial buffers

        :param sigproc_inst_no: Which signal processing pipeline [0,1,2]
        :param buf: Which of the two fpga buffers to write to [0,1]
        :param fpga_words: list of 32-bit ints to go in FPGA regs
        """
        numpy_vals = np.asarray(fpga_words, dtype=np.uint32)
        # Which buffer are we changing?
        info_addr = self._poly0_validity_info_addr[sigproc_inst_no]
        valid_addr = self._poly0_valid_addr[sigproc_inst_no]
        if buf != 0:
            info_addr = self._poly1_validity_info_addr[sigproc_inst_no]
            valid_addr = self._poly1_valid_addr[sigproc_inst_no]
        # write the validity info
        self.driver.write(info_addr, numpy_vals)

        if set_valid:
            # Mark the polynomial buffer as useable
            # ( ie the buffer validity_info and polys for all beams)
            self.driver.write(valid_addr, 0x1)

    def write_dly_invalid(
        self, sigproc_inst_no: int, buf: int, wait: bool = True
    ) -> None:
        """
        Set the flag that marks one of the polynomial buffers as invalid

        By default wait >53ms afterwards so the FPGA is guaranteed to pick up
        the change and avoid modifying the polynomials while in use by FPGA

        :param sigproc_inst_no: Which signal processing pipeline [0,1,2]
        :param buf: Which of the two fpga buffers to write to [0,1]
        :param wait: Whether to wait and ensure FPGA stopped using buffer
        """
        if buf == 0:
            self.driver.write(self._poly0_valid_addr[sigproc_inst_no], 0)
        else:
            self.driver.write(self._poly1_valid_addr[sigproc_inst_no], 0)
        if wait:
            time.sleep(0.060)

    def configure_corner_turn(self, idx, n_beams) -> None:
        """
        # TODO FIX this code skeleton
        """
        self._driver.write(self._ct_out_beamsenabled[idx], n_beams)

    def disable_packetiser(self, idx) -> None:
        """
        Disable a particular pipeline's packetiser

        :param idx: which pipeline to disable [0,1,2]
        """
        enable_word = 0x0
        self._driver.write(self._pktzr_en_addr[idx], enable_word)

    def prgm_packetiser_first_freqs(
        self, idx: int, words1: list[int], words2: list[int]
    ) -> None:
        """
        Write new VCT-to-freq mapping tables for a pipeline

        Two tables, containing 1024 32-bit entries
        :param idx: which signal-processing pipeline to update
        """
        w1_as_np = np.asarray(words1, dtype=np.uint32)
        self.driver.write(
            self._pktzr_cfg_addr[idx] + self.FRQ_MAP_LO_BASE, w1_as_np
        )
        w2_as_np = np.asarray(words2, dtype=np.uint32)
        self.driver.write(
            self._pktzr_cfg_addr[idx] + self.FRQ_MAP_HI_BASE, w2_as_np
        )

    def prgm_packetiser_beam_nos(self, idx: int, datablob: list[int]) -> None:
        """
        write new packetiser beam-numbers table (16 values) for a pipeline
        :param idx: which signal-processing pipeline to update [0,1,2]
        """
        data_as_np = np.asarray(datablob, dtype=np.uint32)
        self._driver.write(
            self._pktzr_cfg_addr[idx] + self.PKTZR_BEAM_TBL_BASE, data_as_np
        )

    def prgm_packetiser_first_chans(
        self, idx: int, chan_nos: list[int]
    ) -> None:
        """
        write new packetiser first-channel-no table (16 values) for a pipeline
        :param idx: which signal processing pipeline to update [0,1,2]
        """
        data_as_np = np.asarray(chan_nos, dtype=np.uint32)
        self._driver.write(
            self._pktzr_cfg_addr[idx] + self.FIRST_CHAN_TBL_BASE, data_as_np
        )

    def disable_and_prgm_packetiser(
        self, idx: int, datablob: list[int]
    ) -> None:
        """
        Disable packetiser and write new packetiser params for a pipeline
        :param idx: which pipeline to update [0,1,2]
        """
        self._driver.write(self._pktzr_en_addr[idx], 0x0)
        data_as_np = np.asarray(datablob, dtype=np.uint32)
        self._driver.write(self._pktzr_cfg_addr[idx], data_as_np)

    def packetiser_enable(self, idx: int, enable: bool) -> None:
        """
        Enable or disable packetiser.
        FPGA requires a rising edge to enable & load new parameters

        :param idx: which pipeline's packetiser to enable
        """
        self._driver.write(self._pktzr_en_addr[idx], 0x0)
        if enable:
            self._driver.write(self._pktzr_en_addr[idx], self.PKTZR_EN_BIT)

    def update_vct(
        self, idx, vct_words, tot_coarse, tot_stns, tot_vchans
    ) -> None:
        """
        Write VCT params to PST pipeline & run new config

        :param idx: PST firmware pipeline number
        :param vct_words: list of VCT entry values
        :param tot_coarse: number of coarse channels beamformed
        :param tot_stns: number of (sub-)stations beamformed
        :param tot_vchans: number of FPGA virtual channels used
        """
        # Ensure any prior table swap is complete
        nxt_tbl = self.wait_for_flip_get_next_buf(idx)
        nxt_tbl_byte_ofset = (
            nxt_tbl
            * self.FPGA_VCT_ENTRIES
            * self.WORDS_PER_VCT_ENTRY
            * WORD_SIZE
        )
        # Write new settings to unused VCT
        vct_as_np = np.asarray(vct_words, dtype=np.uint32)
        self._driver.write(self._vct_addr[idx] + nxt_tbl_byte_ofset, vct_as_np)
        # write stations,chans, vchans for new table
        self._driver.write(self._tot_stns[idx][nxt_tbl], tot_stns)
        self._driver.write(self._tot_coarse[idx][nxt_tbl], tot_coarse)
        self._driver.write(self._tot_chans[idx][nxt_tbl], tot_vchans)
        # Flip to new table
        self._logger.info("set table_select[%d] := %d", idx, nxt_tbl)
        self._driver.write(self._ct1_table_sel[idx], nxt_tbl)
        self._last_table_flip_time[idx] = time.time()

    def wait_for_flip_get_next_buf(self, idx) -> int:
        """Wait for previous buffer flip complete and get next buffer to use"""
        # FPGA guarantees a flip is done within 2 corner turns = 2x53msec
        while time.time() < (self._last_table_flip_time[idx] + 0.106):
            time.sleep(0.01)  # expected slow operation
        table_sel = self._driver.read(self._ct1_table_sel[idx], 1) & 0x01
        self._logger.info("read table_select[%d] = %d", idx, table_sel)
        next_table = 1 if table_sel == 0 else 0
        return next_table

    def log_firmware_errs(self, idx):
        """
        Log error counters.
        Non-zero is fatal to FPGA operation, requires firmware fix.
        Only work-around: wipe FPGA (xbutil reset) and reload firmware.
        """
        in_err = self._driver.read(self._ct1_input_err[idx], 1)
        if in_err != 0:
            txt = f"FPGA pipeline[{idx}] CT1 input error {in_err}"
            self._logger.error(txt)
        bof_err = self._driver.read(self._ct2_bof_err[idx], 1)
        if bof_err != 0:
            txt = f"FPGA pipeline[{idx}] CT2 buffer overflow error {bof_err}"
            self._logger.error(txt)
        rd_err = self._driver.read(self._ct2_rd_err[idx], 1)
        if rd_err != 0:
            txt = f"FPGA pipeline[{idx}] CT2 readout error {rd_err}"
            self._logger.error(txt)

    def fpga_status(self) -> dict:
        """
        Gather status indication for FPGA processing operations
        """
        summary = {}
        summary["fpga_uptime"] = self["system"]["time_uptime"].value
        summary["total_pkts_in"] = self["system"][
            "eth100g_rx_total_packets"
        ].value
        summary["spead_pkts_in"] = self["lfaadecode100g"][
            "spead_packet_count"
        ].value
        summary["total_pkts_out"] = self["system"][
            "eth100g_tx_total_packets"
        ].value
        # Total good pkt output across all signal processing pipelines
        total_of_good_pkts = 0
        pipe_names = ["packetiser", "packetiser_2", "packetiser_3"]
        for pipe_no in range(0, len(pipe_names)):
            total_of_good_pkts += self[pipe_names[pipe_no]][
                "stats_packets_rx_sig_proc_valid"
            ].value
        summary["pst_pkts_out"] = total_of_good_pkts
        return summary

    def read_packet_counts(self, idx):
        """
        Read most recent packet count received for each VirtualChannel
        in the particular signal processing pipeline

        :param idx: which signal processing pipeline 0..2

        :return: list of counters, one per virtual chan
        """
        n_words = self.FPGA_VCT_ENTRIES * self.INGEST_STATS_WORDS_PER_VCHAN
        pkt_cnts = self._driver.read(self._vcstats_addr[idx], n_words)
        pkt_cnt_low = pkt_cnts[
            self.INGEST_STATS_PKT_CNT_LO_OFFSET : n_words : self.INGEST_STATS_WORDS_PER_VCHAN
        ]
        pkt_cnt_hi = pkt_cnts[
            self.INGEST_STATS_PKT_CNT_HI_OFFSET : n_words : self.INGEST_STATS_WORDS_PER_VCHAN
        ]
        full_pkt_cnt = (pkt_cnt_hi << 32) + pkt_cnt_low
        return full_pkt_cnt.tolist()

    def zero_pkt_counts(self, idx):
        """Clear packet counters in FPGA registers"""
        n_words = self.FPGA_VCT_ENTRIES * self.INGEST_STATS_WORDS_PER_VCHAN
        np_data = np.zeros(n_words, dtype=np.uint32)
        self.driver.write(self._vcstats_addr[idx], np_data)

    def read_ct1_count(self, idx):
        """
        Read current frame number being written to CT1.
        Filterbank processing may be up to 2 frames behind this
        """
        np_words = self._driver.read(self._ct1_framecount_addr[idx], 4)
        return np_words[0] + (np_words[1] << 32)

    def read_ct2_counts(self, idx):
        """
        Read the HBM buffer frame counts from CT2.

        Highest one of the two will be the current frame being written in.
        Lowest one will be the one being output to the beamformers.
        """
        np_words = self._driver.read(self._ct2_framecounts_addr[idx], 4)
        cnt0 = np_words[0] + (np_words[1] << 32)
        cnt1 = np_words[2] + (np_words[3] << 32)
        return [cnt0, cnt1]

    def update_beam_delay_polys(self, buffer_pair: list, idx) -> None:
        """
        Write delay polynomial data into firmware registers

        :param buffer_pair: Two-element list, each element a 20kword list
        :param idx: which pipeline is to be updated
        of integers to be written to FPGA delay polynomial buffers as uint32
        """
        for cnt, buffer in enumerate(buffer_pair):
            self.driver.write(
                self._stn_beam_poly_addr[idx] + self.POLY_BUF_SIZE * cnt,
                np.asarray(buffer, dtype=np.uint32),
            )
        return

# -*- coding: utf-8 -*-
#
# (c) 2023 CSIRO Space and Astronomy.
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement
# See LICENSE.txt for more info.

"""FPGA Manager class for Correlator Personality"""

import os
import struct

from ska_low_cbf_proc.icl.correlator.constants import SKA_COARSE_SPACING_GHZ
from ska_low_cbf_proc.icl.correlator.register_tables import RegTables, VctEntry
from ska_low_cbf_proc.icl.fpga_mgr import FpgaMgr
from ska_low_cbf_proc.icl.subscription_monitor import SubscriptionMonitor
from ska_low_cbf_proc.processor.delay_subscription import DelayPolySubscriber

POLY_IS_VALID_BIT = 0x1_0000_0000


def get_configured_subarrays(compressed_regs) -> list[int]:
    """
    From compressed registers get a list of subarrays this FPGA
    is configured to calculate

    :param compressed_regs: list of dictionaries, one per entry in the
    'subarray_beams' table in firmware. List has register values in
    a condensed format
    :return: list of subarray_ids

    """
    subarrays = set()
    for sa_entry in compressed_regs:
        subarrays.add(sa_entry["sa_id"])
    return sorted(subarrays)


def get_subarray_lists(subarray_info):
    """
    From global subarray information (published by allocator), extract
    - sorted list of all subarray_ids
    - sorted list of scanning subarray_ids
    """
    all_subarray_ids = set()
    scan_subs = set()
    for (
        subarray_id_txt,
        subarray_description,
    ) in subarray_info.items():
        subarray_id = int(subarray_id_txt)
        all_subarray_ids.add(subarray_id)

        if subarray_description["scanning"]:
            scan_subs.add(subarray_id)
    return sorted(all_subarray_ids), sorted(scan_subs)


def get_delay_uris(subarray_info, subs_cfgd, logger) -> list[tuple]:
    """
    From global subarray info (published by allocator), extract a list
    of tuples describing delay polynomial subscription URIs

    :param subarray_info: list from allocator.internal_subarray
    :param subs_cfgd: list of subarray IDs that this FPGA handles
    :return: list of (subarray_id, beam_id, tango_device, attribute) tuples
    """
    dly_uri_info = []
    for (
        subarray_id_txt,
        subarray_description,
    ) in subarray_info.items():
        subarray_id = int(subarray_id_txt)
        if subarray_id not in subs_cfgd:
            # skip any subarrays that this FPGA doesn't compute
            continue
        # get delay URI for each beam a subarray has
        for beam in subarray_description["stn_beams"]:
            beam_id = int(beam["beam_id"])
            delay_poly_src = beam["delay_poly"]
            try:
                dev, attr = delay_poly_src.rsplit("/", 1)
            except ValueError:
                logger.error(
                    "Not a valid Tango delay polynomial uri: %s",
                    delay_poly_src,
                )
                continue
            dly_uri_info.append((subarray_id, beam_id, dev, attr))
    return dly_uri_info


def is_pktzr_stopping(prev_sbt, this_sbt) -> bool:
    """
    Will any packetisers cease being used due to this change?

    :param prev_sbt: previous subarray-beam table
    :param this_sbt: new subarray-beam table
    :return: True if a packetiser will stop being used
    """
    for mxc_before, mxc_after in zip(prev_sbt, this_sbt):
        if (len(mxc_before) != 0) and (len(mxc_after) == 0):
            return True
    return False


def is_pktzr_starting(prev_sbt, this_sbt) -> bool:
    """
    Will any packetiser start being used due to this change.

    :param prev_sbt: previous subarray-beam table
    :param this_sbt: new subarray-beam table
    :return: True if a packetiser will start being used
    """
    if prev_sbt is None:
        return True
    for mxc_before, mxc_after in zip(prev_sbt, this_sbt):
        if (len(mxc_before) == 0) and (len(mxc_after) != 0):
            return True
    return False


class FpgaMgrCor(FpgaMgr):
    """
    A class for Correlator FPGAs that monitors and programs FPGA registers

    This class translates parameters specified by the Allocator into register
    updates. It also periodically monitors FPGA registers that indicate the
    quality of the ongoing operation of the PST FPGA
    """

    PERIODIC_PRINT_SECS = 30.0
    MAX_DELAY_POLY_QUEUE = 12
    SECS_PER_PKT = 2211840e-9  # packets have 2048 samples @ 1080ns/sample
    PKTS_PER_INTEGRATION = 384
    FPS_NUMER = 390625  # numerator of correlator filterbank frames per sec
    FPS_DENOM = 331776  # denominator of correlator filterbank frames per sec
    CT1_LAG = 1  # Frames CT1 count is in front of filterbank processing

    def __init__(
        self,
        fpga_instance,
        fail_cbk,
        delay_stats_cbk,
        io_stats_cbk,
        subscr_stats_cbk,
        logger,
    ):
        """
        :param delay_stats_cbk: A function expecting a list argument.
        (See processor_device:update_delay_stats)

        The list will be published via Tango attribute to provide
        information about subarray processing in the FPGA.
        One list entry per subarray-beam.
        """
        super().__init__(fpga_instance, fail_cbk, logger)
        # override periodic timer interval for this specific FPGA personality
        self._periodic_secs = 4.0  # i.e. faster than 10-sec delay update

        self.logger.info("Initialising CORRELATOR FPGA interactions")
        self.fpga = fpga_instance

        self._subs_configured = []  # list of configured subarray_ids
        self._subs_scanning = []  # list of scanning subarray_ids

        self._seconds = FpgaMgrCor.PERIODIC_PRINT_SECS
        self._subarray_delay_subs = {}  # subarray-beam delay-subscriptons
        self._poly_q = {}  # queue of (epoch, poly) from subscriptions
        self._polys_avail = {}  # key=subarray.beam, pair of polys for use
        self._was_any_poly_updated = False
        self._prev_vchan_pkt_nos = None  # pkt numbers for 1024 vchans

        self._saved_sbt_info = []  # SBT entries by MXC number
        for _ in range(0, self.fpga.get_num_mxc()):
            self._saved_sbt_info.append([])  # one empty entry per mxc
        self._vchan_table: list[VctEntry] = []

        self._ct1_frame_count = 0  # CT1 frame being written to HBM by FPGA
        # Initially publish empty statistics list since no subarrays exist
        self._stats_cbk = delay_stats_cbk
        delay_stats_cbk([])
        self._highest_pkt_no = 0
        self._highest_pkt_secs = 0
        self._pkt_spread = 0  # diff btw earliest & latest received pkt nos
        self._io_stats_cbk = io_stats_cbk
        self._subscr_stats_cbk = subscr_stats_cbk
        self._subscr_monitor = SubscriptionMonitor()

        negate_stn = os.getenv("STN_DELAY_SIGN", "poS")
        self._negate_stn_delay = negate_stn.lower() == "neg"
        self.logger.info("stn_delay_sign string: %s", negate_stn)
        self.logger.info("Station delays negated: %s", self._negate_stn_delay)

        self._reg_tab = RegTables()

        # Used when starting from no subarrays and no SPS data is available
        self._waiting_for_sps_data = False
        self._unstarted_all_subs = None
        self._unstarted_cpr_regs = None

    def do_register_changes(self, cmd):
        """
        Called to change FPGA register settings arising from changes to
        allocator attributes that specify the FPGA state
        :param cmd: Dictionary of parameter change commands
        """
        if "subarray" in cmd:
            subarray_descr = cmd["subarray"]
            cpr_regs = cmd["regs"]
            self._program_regs(subarray_descr, cpr_regs)
        else:
            self.logger.warning("Unrecognised register command")

    def periodic_actions(self):
        """
        Called to periodically check or update FPGA registers
        (period in self._periodic_secs)
        """

        # Print liveness message
        self._seconds += self._periodic_secs
        if self._seconds >= FpgaMgrCor.PERIODIC_PRINT_SECS:
            self._seconds -= FpgaMgrCor.PERIODIC_PRINT_SECS
            self.logger.info("Correlator periodic checks")
            fpga_io_status = self.fpga.fpga_status()
            self.logger.info("fpga_status: %s", str(fpga_io_status))
            self._io_stats_cbk(fpga_io_status)

        if self._waiting_for_sps_data:
            self._program_regs(
                self._unstarted_all_subs, self._unstarted_cpr_regs
            )

        # Update delay polynomials if there are scanning subarrays
        if len(self._subs_scanning) != 0:
            # These calls modify class variables, need to be done in order...
            self._check_current_pkt_counters()  # read SPS packet time
            self._queue_subscribed_polys()  # get delays from subscriptions
            self._prep_polys_for_use()  # pick the two polys to use
            self._update_delay_registers()  # write delay polys to FPGA
            self._update_subscription_valid()
            self._update_delay_stats()  # publish stats about delays

    def _check_current_pkt_counters(self):
        """
        Read current ingest packet counters:
            - check SPS data is being received on all in-use virtual channels
            - find highest packet number
            - for debug, log the spread of the incoming packet numbers

        :return: highest packet number seen so far on any virtual channel
        """
        # Which virtual channel numbers are currently in use?
        vchans_active = [itm.vchan for itm in self._vchan_table]
        num_vchans = len(vchans_active)
        if num_vchans == 0:
            return 0

        all_pkt_ctrs = self.fpga.read_packet_counts()
        # only consider virtual channels that are in use
        vchan_pkt_cntrs = [
            all_pkt_ctrs[i]
            for i in range(0, len(all_pkt_ctrs))
            if i in vchans_active
        ]

        highest_pkt = max(vchan_pkt_cntrs)
        pkt_spread = 0
        if (self._prev_vchan_pkt_nos is not None) and (
            len(self._prev_vchan_pkt_nos) == len(vchan_pkt_cntrs)
        ):
            changes = [
                vchan_pkt_cntrs[i] - self._prev_vchan_pkt_nos[i]
                for i in range(0, num_vchans)
            ]
            max_delta = max(changes)
            min_delta = min(changes)
            if max_delta == 0:
                self.logger.warning(
                    "No new SPS packets (pkt_no=%s)",
                    highest_pkt,
                )
            elif min_delta == 0:
                self.logger.warning(
                    "pkt_no=%s, some missing packets",
                    highest_pkt,
                )
            else:
                # find lowest packet counter that's actually incrementing
                lowest = highest_pkt
                for cnt in range(0, num_vchans):
                    # is this virtual channel getting any packets?
                    if min_delta > 0:
                        lowest = min(lowest, vchan_pkt_cntrs[cnt])
                pkt_spread = highest_pkt - lowest
                # log recent SPS packet number info
                self.logger.debug(
                    "pkt_no=%s (spread %s)",
                    highest_pkt,
                    pkt_spread,
                )
        # save current pkt nos for comparison at next read
        self._prev_vchan_pkt_nos = vchan_pkt_cntrs

        # earliest possible frame being read from HBM by filterbank
        self._ct1_frame_count = self.fpga.get_ct1_frame_count() - self.CT1_LAG

        # save current SPS time as highest packet count
        self._highest_pkt_no = highest_pkt
        # save SPS time as seconds since J2000 epoch
        self._highest_pkt_secs = highest_pkt * self.SECS_PER_PKT
        # save earliest-to-latest spread in packet numbers
        self._pkt_spread = pkt_spread

    def _update_subscription_valid(self) -> None:
        """Update local record keeping track whether delay poly. subscriptions
        have failed.
        """
        modified = False
        for name, subscriber in self._subarray_delay_subs.items():
            # decompose into subarray / beam
            subarray, beam = [int(_) for _ in name.split(".")]
            if subscriber.failed:
                modified |= self._subscr_monitor.add(subarray, beam)
                continue
            # otherwise purge this subarray-beam if it failed previously
            modified |= self._subscr_monitor.remove(subarray, beam)

        if modified:
            self._subscr_stats_cbk(self._subscr_monitor.get())

    def _queue_subscribed_polys(self) -> None:
        """
        Drain polynomials from subscriptions (self._subarray_delay_subs)
        and place in local queue (self._poly_q).
        Groom queue to contain only relevant polynomials:
        - not too old,
        - not too many,
        - no duplicates
        """

        for name, dly_obj in self._subarray_delay_subs.items():
            # newly added subarray-beams need storage
            if name not in self._poly_q:
                self._poly_q[name] = []
                self._polys_avail[name] = [None, None]

            dly_obj.queue_subscribed_polys(
                self._ct1_frame_count, self.FPS_NUMER, self.FPS_DENOM, name
            )

    def _prep_polys_for_use(self) -> None:
        """
        Choose the two delay polynomials (from self._poly_q) that should be
        currenly used for each beam and update/save in self._polys_avail
        This is done for all configured subarray_beams, even if not scanning
        so that polynomials are ready whenever scan may be commanded
        """

        for beam_name, dly_obj in self._subarray_delay_subs.items():
            was_poly_updated = dly_obj.prep_polys_for_use(
                beam_name, self._ct1_frame_count
            )

            self._was_any_poly_updated = (
                was_poly_updated or self._was_any_poly_updated
            )

    def _update_delay_registers(self) -> None:
        """
        Write the two prepared polynomials in self._polys_avail to registers.

        self._polys_avail contains two latest polynomials for each beam. First
        one of the two should be writen to the first hardware buffer, second
        one should be written to the second buffer.

        Both buffers will be completely written, but in some places the data
        written will be the same as the data already in the buffer

        Buffers holding data to be programmed to FPGA are initialised with
        floating-pt zero == integer zero (all zero bits)
        One 20-word entry per Virtual channel * 1024 virt chans = 20k words
        """
        if not self._was_any_poly_updated:
            return

        words_buffer = [
            [0] * 20 * 1024,
            [0] * 20 * 1024,
        ]

        all_blk_nos = [set(), set()]
        for itm in self._vchan_table:
            name = f"{itm.sa_id}.{itm.stn_bm}"
            missing_names = []
            if (name not in self._subarray_delay_subs) and (
                name not in missing_names
            ):
                self.logger.error(
                    "No delay poly for subarray %s beam %s",
                    itm.sa_id,
                    itm.stn_bm,
                )
                missing_names.append(name)

            polys_avail = self._subarray_delay_subs[name].get_polys_avail()
            base_word = 20 * itm.vchan  # first data word for this VCT entry
            for buffer_num, poly in enumerate(polys_avail):
                if poly is None:
                    continue  # zero present from initialisation of buffer
                # _, blk_num, offset_sec, delay_details, _ = poly
                # find station's delay in poly
                coeffs = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
                pol_offset_ns = 0
                delay_details = poly.poly
                for dly in delay_details:
                    if (
                        dly["station_id"] == itm.stn
                        and dly["substation_id"] == itm.sstn
                    ):
                        coeffs = dly["xypol_coeffs_ns"]
                        # pad polynomials that only have the constant term
                        while len(coeffs) < 6:
                            coeffs.append(0.0)
                        pol_offset_ns = dly["ypol_offset_ns"]
                        break
                # have we been told to negate station delays
                if self._negate_stn_delay:
                    coeffs = [-val for val in coeffs]
                # Collect data elements needed for FPGA programming
                sky_ghz = itm.frq_id * SKA_COARSE_SPACING_GHZ
                offset_sec = poly.offset_sec
                blk_num = poly.first_valid_blk_no
                all_blk_nos[buffer_num].add(blk_num)
                blk_num = blk_num | POLY_IS_VALID_BIT
                # convert fp and int values into 32-bit unsigned for registers
                tmp_bytes = struct.pack(
                    "<dddddddddQ",
                    *coeffs,
                    sky_ghz,
                    offset_sec,
                    pol_offset_ns,
                    blk_num,
                )
                words_buffer[buffer_num][
                    base_word : base_word + 20
                ] = struct.unpack("<IIIIIIIIIIIIIIIIIIII", tmp_bytes)
        ct1_pre = self.fpga.get_ct1_frame_count()
        self.fpga.update_delay_polys(words_buffer)
        ct1_post = self.fpga.get_ct1_frame_count()
        txt = f"Polynomial Update: ct1_before {ct1_pre}"
        txt += f", first blks [{str(all_blk_nos[0])}, {str(all_blk_nos[1])}]"
        txt += f", ct1_after {ct1_post}"
        self.logger.info(txt)

    def _update_delay_stats(self):
        """
        Get info about the two delay-polynomials programmed into FPGA registers
        Note: Only scanning subarrays have will have register entries
        - polynomial start times
        - validity (valid if either is in use)
        - Delay nanoseconds (NOT poly) for each station (evaluated at T=0)
        """
        if not self._was_any_poly_updated:
            return
        self._was_any_poly_updated = False

        sb_start_times = []
        sb_idx = {}
        sps_pkt_time = self._highest_pkt_secs
        # for each polynomial currently ready to be programmed into FPGA
        for name, dly_obj in self._subarray_delay_subs.items():
            subarray_id, beam_id = name.split(".")
            subarray_id = int(subarray_id)
            beam_id = int(beam_id)
            # only show polynomial stats for scanning subarrays
            if subarray_id not in self._subs_scanning:
                continue
            # make list of start-of-validity epoch seconds
            polys_avail = dly_obj.get_polys_avail()  # The two polys in FPGA
            epochs_programmed = []
            is_delay_valid = False
            earliest_poly_idx = -1  # ie none found (use none -> linter err)
            for poly_info in polys_avail:
                if poly_info is None:
                    epochs_programmed.append(None)
                else:
                    start_time = poly_info.epoch
                    valid_time = poly_info.valid_secs
                    epochs_programmed.append(start_time)
                    if start_time <= sps_pkt_time <= (start_time + valid_time):
                        is_delay_valid = True
                    # Is this polynomial the earliest epoch (ie likely in-use)
                    if (earliest_poly_idx == -1) or (
                        (earliest_poly_idx != -1)
                        and (start_time < epochs_programmed[earliest_poly_idx])
                    ):
                        earliest_poly_idx = len(epochs_programmed) - 1
            # make list of delay values for each station in subarray
            # corresponding to the start of the earliest delay poly (ie in use)
            stn_delay_ns = []
            if earliest_poly_idx != -1:
                dly_detail = polys_avail[earliest_poly_idx].poly
                for stn_dly_info in dly_detail:
                    stn_dly = {
                        "stn": stn_dly_info["station_id"],
                        # TODO: add substation into stats info?
                        "ns": stn_dly_info["xypol_coeffs_ns"][0],
                    }
                    stn_delay_ns.append(stn_dly)

            # copy start-of-validity times to published structure
            # structure is list-of-dicts, one dict in list per subarray
            if subarray_id not in sb_idx:
                sb_start_times.append(
                    {"subarray_id": subarray_id, "beams": []}
                )
                sb_idx[subarray_id] = len(sb_start_times) - 1
            subscription_ok = self._subscr_monitor.is_ok(subarray_id, beam_id)
            sb_start_times[sb_idx[subarray_id]]["beams"].append(
                {
                    "beam_id": beam_id,
                    "valid_delay": is_delay_valid,
                    "subscription_valid": subscription_ok,
                    # "delay_q_len": len(self._poly_q[name]),
                    "delay_start_secs": epochs_programmed,
                    # Do we really need to publish this bulky info?
                    # ie list can have up to 1024 dict entries
                    "stn_delay_ns": stn_delay_ns,
                }
            )
        # add info about current packet numbers
        # info only correct if at least one subarray is scanning
        if len(self._subs_scanning) != 0:
            extra_info = {
                "current_secs": self._highest_pkt_secs,
                "pkt_no_spread": self._pkt_spread,
            }
            sb_start_times.append(extra_info)

        # update attribute via the callback
        self._stats_cbk(sb_start_times)

    def _program_regs(self, all_subarrays_info, cpr_regs):
        """
        Program FPGA registers if scanning subarrays has changed

        :param all_subarrays_inf: dict of all subarray info from allocator's
                "internal_subarray" atrribute
        :param cpr_regs: compressed register descriptions from allocator's
                "internal_alveo" attribute
        """

        all_subs_configured, all_subs_scanning_new = get_subarray_lists(
            all_subarrays_info
        )
        subs_handled = get_configured_subarrays(cpr_regs)  # subs for this FPGA

        # Configured Subarrays this FPGA computes
        subs_configured = [s for s in all_subs_configured if s in subs_handled]
        self.logger.info("Subarrays: %s", subs_configured)
        # Scanning subarrays this FPGA computes
        subs_scanning_new = [
            s for s in all_subs_scanning_new if s in subs_handled
        ]
        self.logger.info("Subarrays scanning: %s", subs_scanning_new)

        # Get list of subscriptions for all configured subarrays
        sub_dly_poly_sbda = get_delay_uris(
            all_subarrays_info, subs_configured, self.logger
        )
        # Make sure we're subscribed to polys for all configured subarrays
        self._subscribe_delay_polys(sub_dly_poly_sbda)

        # No registers to update if list of scanning subarrays unchanged
        if subs_scanning_new == self._subs_scanning:
            # but remove delay-poly subscriptions for deconfigured subarrays
            self._prune_delay_subscriptions(subs_configured)
            self._prune_prepared_polys(subs_configured)
            return

        self.logger.info(
            "Scanning subarrays change %s -> %s",
            self._subs_scanning,
            subs_scanning_new,
        )

        # Which subarrays are ending scans?
        ending_ids = [
            sa for sa in self._subs_scanning if sa not in subs_scanning_new
        ]
        if len(ending_ids) != 0:
            self.logger.info("Subarrays stopping scan %s", str(ending_ids))
            # Remove ending subarrays from vchans, free up vchans
            n_scanning = len(subs_scanning_new)
            if not self._waiting_for_sps_data:
                # SPS data never started, no need to stop
                self._stop_subarrays(
                    all_subarrays_info, n_scanning, ending_ids
                )
            if n_scanning == 0:
                self._waiting_for_sps_data = False
                self._unstarted_cpr_regs = None
                self._unstarted_all_subs = None

        # which subarrays are starting scans?
        starting_ids = [
            sa for sa in subs_scanning_new if sa not in self._subs_scanning
        ]
        if len(starting_ids) != 0:
            self.logger.info("Subarrays starting scan %s", str(starting_ids))
            # If no subarrays previously running then we have to init FPGA
            init_from_none = len(self._subs_scanning) == 0
            # Merge starting subarrays into vchans
            start_ok = self._start_subarrays(
                all_subarrays_info,
                init_from_none,
                starting_ids,
                cpr_regs,
            )
            if not start_ok:
                # no SPS data: didn't manage to start anything. Postpone
                subs_scanning_new = self._subs_scanning
                self._unstarted_cpr_regs = cpr_regs
                self._unstarted_all_subs = all_subarrays_info
            else:
                self._unstarted_cpr_regs = None
                self._unstarted_all_subs = None

        # remove delay subscriptions for subarrays no longer configured
        self._prune_delay_subscriptions(subs_configured)
        self._prune_prepared_polys(subs_configured)

        # clean up published subarray delay-poly info
        if len(subs_scanning_new) == 0:
            self._stats_cbk([])

        # save current working parameters
        # self._spead_start_info = new_spead_start_info
        self._subs_configured = subs_configured
        self._subs_scanning = subs_scanning_new

    def _stop_subarrays(self, all_subarrays_info, n_scanning, ending_ids):
        """Stop one or more subarrays being processed."""
        self.logger.info("Begin flip-to-REMOVE %s", str(ending_ids))
        buf_num = self.fpga.wait_for_flip_get_next_buf()
        self.fpga.wait_for_packetiser_flip_complete()

        self._reg_tab.release_vchans(ending_ids)
        # Program VCT,Demap,Subarra-beam Tables with existing - stopping subs
        words, n_ch, self._vchan_table = self._reg_tab.get_vct()
        self.fpga.prgm_vct(buf_num, words, n_ch)
        self.fpga.prgm_demap(buf_num, self._reg_tab.get_vc_demap())
        self.fpga.prgm_sbt(buf_num, self._reg_tab.get_sbt())
        ent = self._reg_tab.get_sbt_entries(self.fpga.get_num_mxc())
        self.fpga.prgm_num_sbt_entries(buf_num, ent)
        # Program packetiser
        sbt_info = self._reg_tab.get_sbt_info(self.fpga.get_num_mxc())
        if all_subarrays_info is not None:
            self.fpga.prgm_pktzr(buf_num, sbt_info, all_subarrays_info)
        # program info for spead-end packets
        self.fpga.prgm_pktzr_spead_end(
            buf_num, self._saved_sbt_info, ending_ids
        )
        pktzr_stop = is_pktzr_stopping(self._saved_sbt_info, sbt_info)
        self._saved_sbt_info = sbt_info
        self.fpga.flip_to_remove(buf_num)
        if n_scanning == 0:  # Complete stop of processing?
            self.logger.info("FPGA shutdown & pktzr stop")
            self.fpga.send_ends_and_stop()
            self._reg_tab.check_no_vchans()  # assert no vchans used
            return
        if pktzr_stop:  # Do we need to shutdown a packetiser?
            self.fpga.wait_for_packetiser_flip_complete()
            self.logger.info("pktzr stopping")
            self.fpga.pktzr_enable(sbt_info)

    def _start_subarrays(
        self, all_subarrays_info, init_from_none, starting_ids, cpr_regs
    ):
        """Start additional subarrays being processed."""
        txt = f"Begin flip-to-ADD {starting_ids} from_none={init_from_none}"
        self.logger.info(txt)
        if init_from_none:
            self.fpga.prep_for_start()  # asserts lfaa_reset
            # TODO recover if data has started ?
        buf_num = self.fpga.wait_for_flip_get_next_buf()
        self.fpga.wait_for_packetiser_flip_complete()

        self._reg_tab.assign_vchans(starting_ids, cpr_regs)
        # Program VCT,Demap,Subarra-beam Tables with existing + starting subs
        words, n_ch, self._vchan_table = self._reg_tab.get_vct()
        self.fpga.prgm_vct(buf_num, words, n_ch)
        self.fpga.prgm_demap(buf_num, self._reg_tab.get_vc_demap())
        self.fpga.prgm_sbt(buf_num, self._reg_tab.get_sbt())
        ent = self._reg_tab.get_sbt_entries(self.fpga.get_num_mxc())
        self.fpga.prgm_num_sbt_entries(buf_num, ent)
        if init_from_none:
            # we also need current VCT programmed to have ct1 data flowing
            other_buf = buf_num ^ 0x1
            self.fpga.prgm_vct(other_buf, words, n_ch)
            self.fpga.prgm_demap(other_buf, self._reg_tab.get_vc_demap())
            self.fpga.prgm_sbt(other_buf, self._reg_tab.get_sbt())
            self.fpga.prgm_num_sbt_entries(other_buf, ent)
            # wait until we get data (otherwise flip won't complete)
            self.fpga.start()  # release ingest reset, start data latch-on
            ok = self.fpga.wait_for_data()
            if not ok:
                self.logger.warning("FPGA startup postponed. No SPS data")
                self._reg_tab.release_vchans(starting_ids)
                self._waiting_for_sps_data = True
                return False
            self.logger.info("FPGA startup OK, SPS data arriving")
        self._waiting_for_sps_data = False
        # Program packetiser
        sbt_info = self._reg_tab.get_sbt_info(self.fpga.get_num_mxc())
        if is_pktzr_starting(self._saved_sbt_info, sbt_info):
            self.logger.info("pktzr starting")
            self.fpga.pktzr_enable(sbt_info)
        self._saved_sbt_info = sbt_info
        self.fpga.prgm_pktzr(buf_num, sbt_info, all_subarrays_info)
        if init_from_none:
            # spead-start at FPGA startup: configure both packetiser buffers
            other_buf = buf_num ^ 0x1
            self.fpga.prgm_pktzr(other_buf, sbt_info, all_subarrays_info)
        # Send spead-init packets
        self.fpga.send_spead_start(starting_ids, sbt_info, all_subarrays_info)
        # Remove all spead-ends before flipping otherwise packetiser would
        # send them. Even though we're adding subarrays!
        self.fpga.prgm_pktzr_spead_end(buf_num, [[], []], [])
        # Only flip AFTER enabling ingest, or flip-active bits won't work
        self.fpga.flip_to_add(buf_num)
        return True

    def _subscribe_delay_polys(self, sub_dly_poly_sbda) -> None:
        """
        Make sure we have Tango event subscriptions for delay polynomials
        of all subarray beams.

        :param sub_dly_poly_sbda: list of all delay polys for all subarrays
        :return: list of subarray-ids corresponding all active subarrays
        """
        for itm in sub_dly_poly_sbda:
            subarray_id, beam_id, dev, attr = itm
            # use "subarray_id.beam_id" as key for subscriptions
            beam_poly_name = f"{subarray_id}.{beam_id}"
            # subscribe to events if not already subscribed
            if beam_poly_name not in self._subarray_delay_subs:
                self.logger.info(
                    "Subscribe Delay subarray_%d, beam_%d dev=%s attr=%s",
                    subarray_id,
                    beam_id,
                    dev,
                    attr,
                )
                self._subarray_delay_subs[
                    beam_poly_name
                ] = DelayPolySubscriber(self.logger)
                self._subarray_delay_subs[beam_poly_name].subscribe(dev, attr)

    def _prune_delay_subscriptions(self, configured_subarray_ids):
        """
        Unsubscribe from delays for any subarray not in the argument list

        :param subarray_ids: ids of all in-use (configured) subarrays
        """
        unused_subarray_beams = []
        subscr_modified = False
        for name, subscriber_obj in self._subarray_delay_subs.items():
            s_id, b_id = name.split(".")
            if int(s_id) in configured_subarray_ids:
                # don't touch delay subscription if subarray is configured
                continue
            # remove delay subscription because subarray no longer configured
            self.logger.info("Unsub Delay subarray_%s beam_%s", s_id, b_id)
            subscriber_obj.unsubscribe()
            unused_subarray_beams.append(name)
            subscr_modified |= self._subscr_monitor.remove(
                int(s_id), int(b_id)
            )
        for name in unused_subarray_beams:
            # free delay-subscriber objects to allow GC
            self._subarray_delay_subs.pop(name)
        if subscr_modified:
            self._subscr_stats_cbk(self._subscr_monitor.get())

    def _prune_prepared_polys(self, configured_subarray_ids):
        """
        When subarray is no longer configured, Remove subarray-beams from:
        - queue of incoming polynomials (self._poly_q)
        - delay polynomials ready for use in dual-buffer (self.polys_avail)

        :param scanning_subarray_ids: list of IDs of scanning subarrays
        """
        beams_to_remove = []
        for name in self._polys_avail:
            s_id, _ = name.split(".")
            s_id = int(s_id)
            if s_id not in configured_subarray_ids:
                beams_to_remove.append(name)
        for name in beams_to_remove:
            # These two always deleted together
            self._poly_q.pop(name)
            self._polys_avail.pop(name)

    def cleanup(self):
        """
        Final Tasks before exit of FPGA monitoring thread
        """

        # stop any scanning subarrays
        scanning_ids = [sa for sa in self._subs_scanning]
        if len(scanning_ids) > 0:
            self.logger.info(
                "Stopping compute for subarrays: %s", str(scanning_ids)
            )
            n_remaining_scanning = 0
            self._stop_subarrays(None, n_remaining_scanning, scanning_ids)

        # unsubscribe all delay-polynomial attribute subscriptions
        configured_subarrays = []  # no subarrays to keep
        self._prune_delay_subscriptions(configured_subarrays)
        self._prune_prepared_polys(configured_subarrays)  # optional, really
        # Clear packet counter registers
        self._prev_vchan_pkt_nos = None  # unnecessary: thread exiting
        self.fpga.zero_packet_counts()  # needed: fpga image may be re-used

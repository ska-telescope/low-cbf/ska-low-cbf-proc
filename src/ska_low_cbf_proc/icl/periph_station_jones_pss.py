# -*- coding: utf-8 -*-
#
# Copyright (c) 2022 CSIRO Space and Astronomy.
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more information.


import numpy as np
from ska_low_cbf_fpga import FpgaPeripheral


class FpgaStationJonesPss:
    """
    An interface to PSS station Jones peripheral for one SigProc pipeline
        **** FPGA peripheral name: PSSbeamformerP_stationJones ****

    A PSS station jones peripheral for pipeline P contains a RAM block
    RAM contents are:
      4kwords Station Jones buffer 0  (1024 stns x 4 32-bit words)
      4kwords station Jones buffer 1  (1024 stns x 4 32-bit words)
      2kwords weights buffer 0 (1024 stns x 16-bit weights)
      2kwords weights buffer 1 (1024 stns x 16-bit weights)

    Concept is that one Jones buffer will be in active use and the other
    buffer can be updated with newer Jones information.
    """

    JONES_BUF_OFFSET = 0
    JONES_BUF_ENTRIES = 1024  # 1k VCT entries
    WORDS_PER_JONES_ENTRY = 4  # 4x 32-bit words for each Jones Matrix
    JONES_BUF_WORDS = JONES_BUF_ENTRIES * WORDS_PER_JONES_ENTRY

    WTS_BUF_OFFSET = 8192
    WTS_BUF_WORDS = 512  # 1k x 2-byte (0.5 Word) entries (= 512 words)

    def __init__(self, fpga_periphs: FpgaPeripheral, logger):
        self.regs = fpga_periphs  # Access to FPGA peripheral registers
        self.logger = logger

    def write_weighted_stn_jones(self, bufno: int, words: list[int]) -> None:
        """
        Write new Jones data to one of the buffers

        :param bufno: which buffer to write to
        :param words: list containing 4 integers for every station
        """
        assert (
            len(words) % self.WORDS_PER_JONES_ENTRY
        ) == 0, f"Expected multiple of {self.WORDS_PER_JONES_ENTRY}, got {len(words)}"

        # FPGA driver requires numpy array of data
        np_words = np.asarray(words, dtype=np.uint32)

        if bufno != 0:
            bufno = 1
        start = self.JONES_BUF_OFFSET + bufno * self.JONES_BUF_WORDS
        end = start + len(words)

        if self.regs is not None:
            self.regs.data[start:end] = np_words
        else:
            print(f"[{start}:{end}]: {np_words}")

    def write_stn_weights(self, bufno: int, words: list[int]):
        """
        Write new station weights to one of the buffers

        :param bufno: which of the two buffers to write to
        :param words: list of 16-bit int weights, one per station
            Two 16-bit weights words go in each word written to FPGA
        """
        # zero pad to even number of words since wts are 2 per word
        if len(words) % 2 != 0:
            words.append(0)
        n_stns = len(words)
        n_words = n_stns // 2

        np_words = np.asarray(words, dtype=np.uint32)
        # group 2 entries into each word to program (TODO: check order)
        np_2dim = np.reshape(np_words, (n_words, 2))
        buf_words = (np_2dim[:, 1] << 16) + np_2dim[:, 0]

        # place to write depends on which buffer is written
        if bufno != 0:
            start = self.WTS_BUF_OFFSET
        else:
            start = self.WTS_BUF_OFFSET + self.WTS_BUF_WORDS
        end = start + n_words

        self.regs.data[start:end] = buf_words

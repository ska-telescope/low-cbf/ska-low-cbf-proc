# -*- coding: utf-8 -*-
#
# Copyright (c) 2022 CSIRO Space and Astronomy.
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more information.

import time

import numpy as np
from ska_low_cbf_fpga import FpgaPeripheral


class FpgaCt2Pss:
    """
    An interface to PSS second corner turn
        **** FPGA peripheral name: "CT2" ****
    CT2 includes
        - frame counters indicating frame being read from CT2 HBM
        - 2x JonesMatrix validity registers (jones_bufferN_*)
        - 2x PSS Beam Delay validity registers (poly_bufferN_*)
        - beamsenabled + numberofbeams registers
    """

    POLY_BUF_SIZE = 20 * 1024  # 20 words per vchan in FPGA polynomial buffer

    def __init__(self, fpga_periph: FpgaPeripheral, logger):
        self.regs = fpga_periph  # Access to FPGA peripheral registers
        self.logger = logger

    def write_dly_validity_for_beams(
        self,
        buf,
        validity_words,
        set_valid: bool = True,
    ) -> None:
        """
        Write start/duration of validity info for one of the poly buffers

        :param buf: Which of the two fpga buffers to write to [0,1]
        :param validity_words: 4x 32-bit integers representing poly validity
            Words: valid_frame_low, valid_frame_hi, valid_duration, offset_ns
        """
        assert (
            len(validity_words) == 4
        ), f"expecting 4 words, got {len(validity_words)}"

        np_words = np.asarray(validity_words, dtype=np.uint32)
        if buf == 0:
            self.regs.poly_buffer0_valid_frame_low[0:4] = np_words
            if set_valid:
                self.regs.poly_buffer0_info_valid.value = 0x1
        else:
            self.regs.poly_buffer1_valid_frame_low[0:4] = np_words
            if set_valid:
                self.regs.poly_buffer1_info_valid.value = 0x1

    def write_dly_invalid(self, buf, wait: bool = True) -> None:
        """
        Set the flag that marks a polynomial buffer invalid

        By default wait >53ms afterwards so the FPGA is guaranteed to pick up
        the change and avoid modifying the polynomials while in use by FPGA

        :param buf: Which of the two fpga buffers to write to [0,1]
        :param wait: Whether to wait and ensure FPGA stopped using buffer
        """
        if buf == 0:
            self.regs.poly_buffer0_info_valid.value = 0x0
        else:
            self.regs.poly_buffer1_info_valid.value = 0x0
        if wait:
            time.sleep(0.060)

    def write_station_jones_validity_for_buffer(
        self, bufno: int, validity_words: list
    ) -> None:
        """
        Write time-of-validity for one of the station-Jones buffers


        :param buf: Which of the two FPGA Jones buffers to write to
        :param validity_words: 3x 32-bit integers representing validity
            Words: valid_frame_low, valid_frame_hi, valid_duration
        """
        assert (
            len(validity_words) == 3
        ), f"expecting 3 words, got {len(validity_words)}"
        np_words = np.asarray(validity_words, dtype=np.uint32)
        if bufno == 0:
            self.regs.jones_buffer0_valid_frame_low[0:3] = np_words
        else:
            self.regs.jones_buffer1_valid_frame_low[0:3] = np_words

    def write_beam_jones_validity_for_buffer(
        self, bufno: int, validity_words: list
    ) -> None:
        """
        Write time-of-validity for one of the beam-Jones buffers


        :param buf: Which of the two FPGA Jones buffers to write to
        :param validity_words: 3x 32-bit integers representing validity
            Words: valid_frame_low, valid_frame_hi, valid_duration
        """
        assert (
            len(validity_words) == 3
        ), f"expecting 3 words, got {len(validity_words)}"
        np_words = np.asarray(validity_words, dtype=np.uint32)
        if bufno == 0:
            self.regs.beamjones_buffer0_valid_frame_low[0:3] = np_words
        else:
            self.regs.beamjones_buffer0_valid_frame_low[0:3] = np_words

    def read_ct2_counts(self) -> list[int]:
        """
        Read the HBM buffer frame counts from CT2.

        Highest one of the two will be the current frame being written in.
        Lowest one will be the one being output to the beamformers.

        :return: Counters from CT2
        """
        # TODO this quick single-read fails:
        # np_words = self.regs.hbmbuf0cornerturncount_low[0:4]
        # cnt0 = np_words[0] + (np_words[1] << 32)
        # cnt1 = np_words[2] + (np_words[3] << 32)
        # TODO FIXME The long slow way:
        w0 = self.regs.hbmbuf0cornerturncount_low.value
        w1 = self.regs.hbmbuf0cornerturncount_high.value
        w2 = self.regs.hbmbuf1cornerturncount_low.value
        w3 = self.regs.hbmbuf1cornerturncount_high.value
        cnt0 = w0 + (w1 << 32)
        cnt1 = w2 + (w3 << 32)
        return [cnt0, cnt1]

    def clear_ct2_counts(self) -> None:
        """
        Write corner turn counts to zero.

        To ensure previous scan counts don't mislead poly loading
        """
        self.regs.hbmbuf0cornerturncount_low.value = 0x0
        self.regs.hbmbuf0cornerturncount_high.value = 0x0
        self.regs.hbmbuf1cornerturncount_low.value = 0x0
        self.regs.hbmbuf1cornerturncount_high.value = 0x0

    def set_num_beams(self, num_beams: int) -> None:
        """
        Configure how many PSS beams this pipeline should produce

        :param num_beams: number of beams
        """
        self.regs.beamsenabled.value = num_beams

    def hbm_reset_and_wait_for_done(
        self, sleep_secs=0.005, timeout_count=100
    ) -> None:
        """
        Hold CT2 HBM in reset while configuration is changed

        :param idx: PST firmware pipeline number
        """
        self.regs.hbm_reset.value = 0x1  # reset CT2 HBM
        cnt = 0
        hbm_status = self.regs.hbm_reset_status.value
        while ((hbm_status & 0x1) == 0) and (cnt < timeout_count):
            time.sleep(sleep_secs)
            cnt += 1
            hbm_status = self.regs.hbm_reset_status.value
        if cnt > timeout_count:
            self.logger.warning(
                "CT2 HBM reset timeout. Status: %s", hex(hbm_status)
            )

    def hbm_run(self) -> None:
        """Release CT2 HBM reset"""
        self.regs.hbm_reset.value = 0x0  # Release CT2 HBM reset

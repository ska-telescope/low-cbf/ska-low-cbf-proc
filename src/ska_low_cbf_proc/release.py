# pylint: disable=invalid-name, redefined-builtin
# -*- coding: utf-8 -*-
"""Release information for ska_low_cbf_proc"""

import importlib.metadata

name = __package__
version = importlib.metadata.version(__package__)
version_info = version.split(".")
description = "SKA Low CBF Processor Device"
author = "CSIRO"
author_email = "andrew dot bolin at csiro dot au"
license = """CSIRO Open Source Licence"""
url = """https://www.skatelescope.org/"""
copyright = """CSIRO"""

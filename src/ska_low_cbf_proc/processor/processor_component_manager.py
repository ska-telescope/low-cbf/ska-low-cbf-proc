# -*- coding: utf-8 -*-
#
# (c) 2022 CSIRO Space and Astronomy.
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement
# See LICENSE.txt for more info.

"""
Component manager for the SKA-Low CBF processor tango device
"""

import gc
import json
import logging
import os
import re
import threading
import time
from dataclasses import dataclass

import numpy
from schema import Or, Schema
from ska_control_model import PowerState
from ska_low_cbf_fpga import (
    WORD_SIZE,
    ArgsWordType,
    FpgaPersonality,
    IclField,
    create_driver_map_info,
    log_to_file,
    mem_parse,
    stop_log_to_file,
)
from ska_low_cbf_fpga.download import DownloadError, get_file_fetcher
from ska_tango_base.poller import PollingComponentManager

from ska_low_cbf_proc.icl.correlator.corr_fpga import CorrFpga
from ska_low_cbf_proc.icl.correlator.fpga_mgr_cor import FpgaMgrCor
from ska_low_cbf_proc.icl.pss.fpga_mgr_pss import FpgaMgrPss
from ska_low_cbf_proc.icl.pss.pss_fpga import PssFpga
from ska_low_cbf_proc.icl.pst.fpga_mgr_pst import FpgaMgrPst
from ska_low_cbf_proc.icl.pst.pst_fpga import PstFpga
from ska_low_cbf_proc.processor.alveo_states import AlveoState


@dataclass
class PollRequest:
    """Class for holding poll request specification."""

    writes: dict
    user_attribute_reads: list
    peripheral_reads: list


@dataclass
class FirmwareRequest:
    """Details of a requested firmware package."""

    personality: str
    version: str
    source: str
    platform: int

    @classmethod
    def from_str(cls, request_string: str):
        """
        Parse a firmware request string into a FirmwareRequest.

        :param request_string: A firmware request, in the format of:
        - personality only ("vis")
        - personality and version ("vis:1.2.3")
        - personality, version, source ("vis:1.2.3:gitlab")
        - URL only ("http://example.com/cnic.tar.gz")

        Personality can use the 'Allocator' terminology or FW repo name.
        Source defaults to "nexus", meaning the Central Artefact Repository (CAR).

        Future: we could accept URLs too, would need modification of downloader
        """
        personality_translate = {
            "vis": "corr",
        }
        """Translate Allocator firmware name to actual firmware personality"""

        personality = request_string
        version = ""  # str for FileFetcher
        source = "nexus"  # default to CAR
        platform = 3  # default XRT platform/shell version
        # ^^^ formerly known as DEFAULT_XILINX_PLATFORM

        colons = request_string.count(":")
        components = [i.lower() for i in request_string.split(":")]
        if request_string.startswith("http"):
            # a URL along the lines of http://some-host.com/fw.tar.gz
            # URL stored in version
            # source used to determine FileFetcher
            version = source = request_string
            # extract personality type from tarball name
            if m := re.search(r".+/([^\W\d_]+)_", request_string):
                # trim tarball personality prefix (correlator_u55...)
                personality = m.group(1)[:4]
        elif colons == 1:
            personality, version = components
        elif colons == 2:
            personality, version, source = components
        elif colons > 2:
            raise ValueError(
                f"Can't decode firmware request: {request_string}"
            )

        if personality in personality_translate:
            # translate from Allocator terminology if required
            personality = personality_translate[personality]

        return cls(personality, version, source, platform)


class ProcessorComponentManager(PollingComponentManager[PollRequest, dict]):
    """A component manager for the SKA-Low CBF processor."""

    FIELD_SEPARATOR = "__"
    # FIXME review this quick-n-dirty mapping from personality name to type
    # TODO add other class types as they become available:
    PERSONALITY_CLASS: dict[str, type[FpgaPersonality]] = {
        "pst": PstFpga,
        "corr": CorrFpga,
        "pss": PssFpga,
    }

    # TODO - extract this from firmware package somehow
    #  (may require extra data to be added to packages?)
    PERSONALITY_MEMORIES = {
        PstFpga: mem_parse((3 * "2Gi:1Gi:")[:-1]),  # CT1 mem, CT2 mem
        CorrFpga: mem_parse("3Gi:3Gi:3Gi:512Mi:512Mi:4Gi"),
        PssFpga: mem_parse("1Gi:1Gi:1Gi:1Gi"),
    }

    EXPOSED_PERIPHERALS = {
        "corr": [
            "lfaadecode100g",
        ],
        "pst": [],
        "cnic": [],
        "pss": [],
    }
    """Personality's peripherals we'll be polling for e.g. determining the heath state.
    """

    MAX_SUBARRAYS = 8
    UNUSED = 2  # a kind of 3-rd state boolean indicating subarray polynomial is not used
    MAX_BEAM_ID = 500

    def __init__(
        self,
        logger,
        communication_state_callback,
        component_definition_callback,
        component_state_callback,
        fpga_usage_cbk,
        mode_stats_cbk,
        subarray_stats_cbk,
        io_stats_cbk,
        subscr_stats_cbk,
        poll_rate=5.0,
    ):
        self._component_definition_callback = component_definition_callback
        self._fpga_usage_cbk = fpga_usage_cbk
        self._mode_stats_cbk = mode_stats_cbk
        self._subarray_stats_cbk = subarray_stats_cbk
        self._io_stats_cbk = io_stats_cbk
        self._subscr_stats_cbk = subscr_stats_cbk

        self._core = None
        self._hw_info = None  # FPGA hardware monitoring
        self._simulation_mode = None
        self.personality = None

        self._write_values = {}
        self._file_fetcher = None

        super().__init__(
            logger,
            communication_state_callback,
            component_state_callback,
            poll_rate,
            processor_state=None,
            poll_time=None,
        )

        self.create_core(
            personality_class=None,
            init_mode=True,
        )

        # None or Dict of params related to personality/firmware/vct_entries
        self._saved_evt_info = None
        # None or Dict of PST beam params
        self._beam_info = None
        # This Alveo's serial number string - defaults to 12 zeros
        self._serial_no = os.getenv("SERIAL_NUM", "000000000000")

        # personality download variables and lock to be held when accessing
        self._dl_lock = threading.Lock()
        self._dl_name = None  # Name of personality: None or string
        self._dl_requested = False  # download requested?
        self._dl_in_progress = False  # download in progress?
        self._cur_fw = None  # Firmware currently running on FPGA
        self._cur_cpr_regs = None  # compressed registers for FPGA
        self._cur_subarray = None  # latest subarray definitions

        # fpga monitoring/command thread and its command queue
        self._fpga_mgr = None  # object managing the fpga

        # Holds any memory config set by debugSetMemConfig command
        self._debug_mem_cfg = ""

        self._alveo_state = AlveoState(
            self.firmware_load,
            self.firmware_unload,
            self.fpga_processing_start,
            self.fpga_processing_stop,
            self.fpga_register_update,
            self.logger,
        )

    def create_core(
        self,
        simulation_mode: bool | None = None,
        personality_class: type[FpgaPersonality] | None = None,
        init_mode: bool = False,
        expose_icl: bool = True,
    ):
        """
        Create our _core FpgaPersonality object
        :param simulation_mode: True = Simulated FPGA; False = Real FPGA.
        None = retain current simulation mode.
        :param personality_class: Derived type of FpgaPersonality to use.
        None = re-use the type currently in use.
        :param init_mode: Dirty hack for startup condition
        :param expose_icl: Expose the ICL to the control system
        (via component definition callback)
        """
        if simulation_mode is not None:
            self._simulation_mode = simulation_mode

        if personality_class is None:
            if self._core:
                personality_class = type(self._core)

        # cleanup attributes before they are gone
        # FIXME - bit of a hack... might be a better way?
        if not init_mode:
            self._clear_definition()
        self._drop_xrt_driver()
        fpgamap_path = getattr(self._file_fetcher, "map_file_path", None)
        xcl_file = getattr(self._file_fetcher, "image_file_path", None)
        mem_config = None
        # ska-low-cbf-fpga v0.17.2 introduced hierarchical loggers, which upset
        # Tango. So we break the chain here. (Maybe hierarchy was a mistake?)
        fpga_logger = logging.getLogger("FPGA")
        if personality_class is not None:
            if self._debug_mem_cfg == "":
                mem_config = self.PERSONALITY_MEMORIES[personality_class]
            else:  # for debug, use the memory definition provided
                mem_config = mem_parse(self._debug_mem_cfg)
        driver, args_map, self._hw_info = create_driver_map_info(
            fpgamap_path=fpgamap_path,
            mem_config=mem_config,
            xcl_file=xcl_file,
            logger=fpga_logger,
            simulate=self._simulation_mode,
        )

        if args_map is not None and driver is not None:
            self._core = personality_class(
                driver,
                args_map,
                hardware_info=self._hw_info,
                logger=fpga_logger,
            )
            if expose_icl:
                self._update_definition()
        self.logger.info(
            f"Created a {self._core.__class__.__name__} core, "
            f"using {driver.__class__.__name__} driver"
        )

    @property
    def simulation_mode(self) -> bool:
        """
        Are we simulating an FPGA? No, code depends on register behaviours
        which are too complex to simulate
        """
        return False

    def off(self, task_callback):
        """
        Turn the component off.

        :param task_callback: callback to be called when the status of
            the command changes
        """
        raise NotImplementedError(
            "ProcessorComponentManager cannot turn power on/off its component."
        )

    def standby(self, task_callback):
        """
        Put the component into low-power standby mode.

        :param task_callback: callback to be called when the status of
            the command changes
        """
        raise NotImplementedError(
            "ProcessorComponentManager cannot turn power on/off its component."
        )

    def on(self, task_callback):  # pylint: disable=invalid-name
        """
        Turn the component on.

        :param task_callback: callback to be called when the status of
            the command changes
        """
        raise NotImplementedError(
            "ProcessorComponentManager cannot turn power on/off its component."
        )

    def reset(self, task_callback):
        """
        Reset the component (from fault state).

        :param task_callback: callback to be called when the status of
            the command changes
        """
        raise NotImplementedError(
            "ProcessorComponentManager cannot (yet) reset its component."
        )

    def polling_started(self) -> None:
        """
        Hook called by base class polling routines that is called as soon as
        adminMode set online and polling commences. TODO remove
        """
        return None

    def get_request(self) -> PollRequest:
        """
        Return the reads and writes to be executed in the next poll.TODO remove

        :return: reads and writes to be executed in the next poll.
        """
        return PollRequest({}, [], [])

    def select_personality(
        self,
        request: FirmwareRequest,
        expose_icl: bool = True,
    ) -> bool:
        """
        Download the specified personality from internet, load into FPGA.

        :param request: Details of firmware to load
        :param expose_icl: Expose the ICL to the control system
        (via component definition callback)
        :returns: success (True) or failure (False)
        """
        personality = request.personality
        platform = request.platform
        source = request.source
        version = request.version
        if not source:
            source = "nexus"
        self.logger.info(
            f"ProcessorComponentManager got a request to download "
            f"{personality} personality, version {version}, source {source}"
        )
        if personality in self.PERSONALITY_CLASS:
            self.personality = personality
            # TODO: stop polling during FPGA image change?
            cache_dir = os.getenv("CACHE_DIR", default="")
            ffetcher = get_file_fetcher(source)
            try:
                self._file_fetcher = ffetcher(
                    personality,
                    version,
                    platform_version=platform,
                    cache_dir=cache_dir,
                    fpga_type=os.getenv("FPGA_TYPE"),
                )
            except DownloadError as ex:
                self.logger.error(f"FPGA Download error: {ex}")
                return False
            self.logger.info(f"got {self._file_fetcher.image_file_path}")
            self.create_core(
                personality_class=self.PERSONALITY_CLASS[personality],
                simulation_mode=False,
                expose_icl=expose_icl,
            )
        else:
            self.logger.error(f"personality {personality} not implemented")
            return False
        return True

    def _name_is_valid(self, name):
        if self.FIELD_SEPARATOR in name:
            # we use the FIELD_SEPARATOR to demarcate peripheral/field
            # so we can't allow it here
            self.logger.error(
                f"Invalid name '{name}': contains field separator "
                f"'{self.FIELD_SEPARATOR}'."
            )
            return False
        return True

    def _update_definition(self):
        def get_definition(field: IclField):
            """Get definition details from an FPGA field"""
            return {
                "write": field.user_write,
                "type": int if field.type_ == ArgsWordType else field.type_,
                "length": field.length,
                "format": field.format,
            }

        if self._component_definition_callback is None:
            return

        definition = {}

        if self._core:
            for field_name in self._core.user_attributes:
                if not self._name_is_valid(field_name):
                    continue
                field = getattr(self._core, field_name)
                self.logger.debug(f"Getting definition for {field_name}")
                definition[field_name] = get_definition(field)

            for peripheral in self.EXPOSED_PERIPHERALS[self.personality]:
                if not self._name_is_valid(peripheral):
                    continue
                for field_name in self._core[peripheral].user_attributes:
                    if not self._name_is_valid(field_name):
                        continue
                    field = getattr(self._core[peripheral], field_name)
                    full_name = (
                        f"{peripheral}{self.FIELD_SEPARATOR}{field_name}"
                    )
                    self.logger.debug(
                        f"Getting definition for {peripheral}.{field_name}"
                    )
                    definition[full_name] = get_definition(field)
        # Add Alveo temperature, power
        definition.update(
            {
                "fpga_temperature": {
                    "write": None,
                    "type": int,
                    "length": 1,
                },
                "fpga_power": {
                    "write": None,
                    "type": float,
                    "length": 1,
                },
            }
        )

        self._component_definition_callback(definition)

    def _clear_definition(self):
        if self._component_definition_callback is not None:
            self._component_definition_callback({})

    def _drop_xrt_driver(self):
        """Remove XRT driver object as it may hold an Alvio card lock.
        Normally called before switching FPGA personality.
        """
        if self._core is not None:
            del self._core
            self._core = None
            gc.collect()

    @property
    def xrt_info(self):
        """Gives processor device access to XRT"""
        return self._hw_info

    def get_extra_alveo_values(self) -> dict:
        """Get Alveo temperature/power from XrtInfo properties"""
        values = {}
        hw = self._hw_info
        values["fpga_temperature"] = hw.fpga_temperature.value if hw else 0
        values["fpga_power"] = hw.fpga_power.value if hw else 0
        return values

    def read_register(self, json_params: str):
        """Read fpga register(s)"""
        # Validate and extract command parameters
        params = json.loads(json_params)
        param_schema = Schema({"name": str, "offset": int, "length": int})
        param_schema.validate(params)
        assert (
            params["name"].count(".") == 1
        ), f"register name [{params['name']}] must contain '.' as separator"
        periph_name, reg_name = params["name"].split(".")
        offset = max(0, params["offset"])
        length = max(0, params["length"])
        # Perform FPGA read operation
        peripheral = self._core[periph_name]
        reg_addr = peripheral[reg_name].address + offset * WORD_SIZE
        read_rslt = self._core.driver.read(reg_addr, length)
        if length == 1:
            # read will have returned integer, convert to list of one
            return json.dumps([read_rslt])
        # convert returned np.array of np.uint32 to list of ints
        return json.dumps([rslt.item() for rslt in read_rslt])

    def write_register(self, json_params: str):
        """Write a fpga register or set of consecutive registers"""
        # Validate and extract command parameters
        params = json.loads(json_params)
        param_schema = Schema(
            {"name": str, "offset": int, "value": Or(int, [int])}
        )
        param_schema.validate(params)
        assert (
            params["name"].count(".") == 1
        ), f"register name [{params['name']}] must contain '.' as separator"
        periph_name, reg_name = params["name"].split(".")
        offset = max(0, params["offset"])
        if isinstance(params["value"], list):
            value = numpy.asarray(params["value"], dtype=numpy.uint32)
        else:
            value = max(0, params["value"])
        # Perform FPGA write operation
        peripheral = self._core[periph_name]
        reg_addr = peripheral[reg_name].address + offset * WORD_SIZE
        self._core.driver.write(reg_addr, value)

    def read_peripheral_register(self, periph: str, reg: str):
        """Read the specified FPGA peripheral's register.
        This is for 'internal' clients (supposedly knowing what they are doing).
        NOTE it is the caller's responsibility to check if the FPGA firmware
        is loaded.

        :param periph: FPGA peripheral (containing the register) name
        :param reg:    FPGA register name
        :return:       FPGA register value
        """
        return self._core[periph][reg].value

    def set_mem_config(self, mem_str: str):
        """Install a memory config string for debug"""
        if not isinstance(mem_str, str):
            self.logger.error("mem config argument must be string")
            return
        self._debug_mem_cfg = mem_str

    def dump_hbm(self, json_params: str):
        """dump HBM contents as binary file on disk"""
        params = json.loads(json_params)
        param_schema = Schema({"hbm": int, "dir": str})
        param_schema.validate(params)
        hbm_idx = params["hbm"]
        tgt_dir = params["dir"]
        if tgt_dir == "":
            tgt_dir = "."

        if self._core is None:
            self.logger.error("HBM dump error: No FPGA is loaded")
            return
        if not (0 <= hbm_idx <= len(self._core.driver._mem_config)):
            self.logger.error("HBM %s does not exist", hbm_idx)
            return
        if not os.path.isdir(tgt_dir):
            self.logger.error("Dirctory '%s' does not exist", tgt_dir)
            return

        file_name = os.path.join(tgt_dir, f"dump_hbm_{hbm_idx:02}.bin")
        hbm_bytes = self._core.driver._mem_config[hbm_idx].size
        thr = threading.Thread(
            target=self._hbm_dump_thread, args=(hbm_idx, hbm_bytes, file_name)
        )
        thr.start()

    def _hbm_dump_thread(self, hbm_idx, hbm_bytes, file_name):
        """Thread to write content of a HBM to binary file"""
        self.logger.info("Reading HBM %s", hbm_idx)
        # Read in 0.5Gb lumps to avoid problems on some servers
        # and because HBM sizes are always multiples of 0.5Gbytes
        page_bytes = 1 << 29
        start_byte = 0
        raw = numpy.empty(hbm_bytes, dtype=numpy.uint8)
        time_0 = time.time()
        for _ in range(0, hbm_bytes, page_bytes):
            raw[
                start_byte : start_byte + page_bytes
            ] = self._core.driver.read_memory(
                hbm_idx, page_bytes, start_byte
            ).view(
                dtype=numpy.uint8
            )
            start_byte += page_bytes
        time_1 = time.time()

        self.logger.info("Writing HBM_%s to %s", hbm_idx, file_name)
        raw.tofile(file_name)
        time_2 = time.time()

        read_rate_m = hbm_bytes / (time_1 - time_0) / 1e6
        txt = f" [ {read_rate_m:.3f} Mbyte/sec read"
        write_rate_m = hbm_bytes / (time_2 - time_1) / 1e6
        txt += f", {write_rate_m:.3f} Mbyte/sec write]"
        self.logger.info("Write HBM completed %s", txt)

    def poll(self, poll_request: PollRequest) -> dict:
        # return read_values
        return {}  # TODO Temporarily hide health errors by not reading regs

    def poll_succeeded(self, poll_response: dict) -> None:
        """
        Handle the receipt of new polling values.

        This is a hook called by the poller when values have been read
        during a poll.

        :param poll_response: response to the pool, including any values
            read.
        """
        super().poll_succeeded(poll_response)

        self.logger.debug(
            f"Handing results of successful poll: {poll_response}."
        )

        # TODO: Always-on device for now.
        power = PowerState.ON

        # TODO: Evaluate fault status. For now, we set it to False
        fault = False
        self.logger.debug(f"Calculated fault status is {fault}.")

        self._update_component_state(power=power, fault=fault, **poll_response)

    # Tango-processor-specific code follows below =============================

    def alloc_alveo_evt(self, attr_name, evt_json, quality):
        """
        Handler called when event is pushed for "internal_alveo" attribute
        This event's data summarises register configuration for Alveo FPGA

        The data includes firmware name, so the event may trigger firmware
        to be loaded or unloaded.

        :param evt_json: json string encoding a dictionary that is keyed by
        alveo serial_number.
        """
        self.logger.info(
            f"Got event from attribute {attr_name}, quality {quality}"
        )
        evt_dict = json.loads(evt_json)
        # belt-and-braces insurance: should never occur
        if not isinstance(evt_dict, dict):
            self.logger.warning(f"Ignoring non-dict evt data: {evt_json}")
            return

        if self._serial_no not in evt_dict:
            # this FPGA not in use
            self._cur_cpr_regs = None
            self._alveo_state.register_update({"fw": None, "regs": None})
            self._fpga_usage_cbk(False)  # Tell Tango device fpga is not used
            return

        # (note alveo_internal_regs is dict with "fw" and "regs" keys)
        alveo_internal_regs = evt_dict[self._serial_no]
        self.logger.info(f"alveo_register_config: {alveo_internal_regs}")
        self._alveo_state.register_update(alveo_internal_regs)
        self._cur_cpr_regs = alveo_internal_regs
        self._fpga_usage_cbk(True)  # Tell Tango device fpga is used
        return

    def alloc_subarray_evt(self, attr_name, evt_json, quality):
        """
        Handler called when event is pushed for "internal_subarray" attribute
        This event's data indicates subarray scanning/not-scanning

        :param evt_json: json string encoding a dictionary that is keyed by
        subarray_id.
        """
        self.logger.info(
            f"Got event from attribute {attr_name}, quality {quality}"
        )
        evt_dict = json.loads(evt_json)
        # belt-and-braces insurance: should never occur
        if not isinstance(evt_dict, dict):
            self.logger.warning(f"Ignoring non-dict evt data: {evt_json}")
            return

        self._cur_subarray = evt_dict
        self._alveo_state.subarray_update(evt_dict)
        return

    @property
    def is_fpga_in_use(self) -> bool:
        """
        :return: whether FPGA is in use (i.e. image loaded or loading)
        """
        return self._alveo_state.is_fpga_used()

    def set_fpga_enabled(self, is_enabled: bool) -> None:
        """
        Set whether FPGA operation is enabled or disabled (by adminMode)
        """
        self._alveo_state.fpga_admin_enable(is_enabled)

    def firmware_load(self, request: str) -> bool:
        """
        Callback used by state machine to trigger a firmware load operation
        Download firmware named in request and load into FPGA
        This is expected to be a slow operation and success is not certain

        :param request: is eg "vis:0.0.2-main.48fe36e6:gitlab"
        (for details see ``FirmwareRequest.from_str`)
        :return: boolean indicating success
        """
        self.logger.info(f"Load firmware: {request}")
        self._mode_stats_cbk(request, False)
        download_request = FirmwareRequest.from_str(request)
        success = self.select_personality(download_request, expose_icl=False)
        if success:
            self._mode_stats_cbk(
                request,
                True,
                {
                    "fw_version": self._core.fw_version.value,
                    "fw_personality": self._core.fw_personality.value,
                },
            )
        return success

    def firmware_unload(self) -> None:
        """
        Callback used by state machine to trigger a firmware unload operation
        Remove Firmware from FPGA
        Xilinx API provides no way to do this: hide the idle fpga
        """
        self._mode_stats_cbk(None, False)

    def fpga_processing_start(self, request: str) -> None:
        """
        Callback called by state machine when FPGA should begin computations
        Start FPGA computation by running the thread that programs
        the FPGA registers
        """
        firmware_info = FirmwareRequest.from_str(request)
        self._start_manager_thread(firmware_info.personality)

    def fpga_processing_stop(self) -> None:
        """
        Callback called by state machine when FPGA should cease all compute
        Stop FPGA computation (and output) by stopping the thread that
        normally programs the FPGA registers
        """
        self._fpga_mgr.stop_manager_thread()
        self._fpga_mgr = None
        self.logger.info("FPGA manager thread stopped")

    def fpga_register_update(self, subarrays, regs):
        """
        Callback called by state machine to tell FPGA manager to update FPGA
        registers - due to subarray changed scanning or changed configuration
        """
        mgr_command = {
            "subarray": subarrays,
            "regs": regs,
        }
        self._fpga_mgr.update_state(mgr_command)

    def _start_manager_thread(self, fw_name):
        """
        Start FPGA manager thread which is responsible for
        making changes to FPGA registers
        """
        # Choose appropriate class & method to manage the FPGA registers
        fpga_manager_class = {
            "pst": FpgaMgrPst,
            "corr": FpgaMgrCor,
            "pss": FpgaMgrPss,
        }
        if fw_name not in fpga_manager_class:
            self.logger.info(f"No FPGA manager for firmware '{fw_name}'")
            self._fpga_mgr = None  # checked by caller
        else:
            self._fpga_mgr = fpga_manager_class[fw_name](
                self._core,
                self._alveo_state.fpga_driver_fail,
                self._subarray_stats_cbk,
                self._io_stats_cbk,
                self._subscr_stats_cbk,
                self.logger,
            )

        if self._fpga_mgr is not None:
            self._fpga_mgr.start_manager_thread()
            self.logger.info("FPGA manager thread started")

    def log_to_file(self, filename: str):
        """
        Start logging FPGA register transactions to a file.

        :param filename: file name to log to
        """
        if self._core is None:
            raise RuntimeError("No FPGA core active")
        log_to_file(self._core.driver.logger, filename)

    def stop_log_to_file(self) -> None:
        """
        Stop logging FPGA register transactions to file(s).
        """
        if self._core is None:
            raise RuntimeError("No FPGA core active")
        stop_log_to_file(self._core.driver.logger)

    def update_delays_valid(
        self, delay_stats: list
    ) -> (list[dict], list[int]):
        """Compile a summary of valid delays per subarray-beam.

        :param delay_stats: a list of dictionaries as returned by
                            FpgaMgrCor._update_delay_stats
        :return: a tuple; a list of dicts (see LowCbfProcessor._station_delay_valid
                 comment for structure) and a list of integers, one per subarray
                 summarising if all delays in subarray are valid (0:INVALID, 1:VALID
                 2:UNKNOWN)
        """
        SUBID_KEY = "subarray_id"
        BEAMID_KEY = "beam_id"
        BEAMS_KEY = "beams"
        VALID_KEY = "valid_delay"
        SUBSCR_KEY = "subscription_valid"
        PST_KEY = "pst"
        log = self.logger
        top_container = []
        station_delays_ok = [self.UNUSED] * self.MAX_SUBARRAYS
        beam_keys = (BEAMID_KEY, VALID_KEY, SUBSCR_KEY)
        for subarr_dict in delay_stats:
            # skip over non-delay entries
            if "current_secs" in subarr_dict:
                continue
            # ensure we have all expected keys
            if not all([k in subarr_dict for k in (SUBID_KEY, BEAMS_KEY)]):
                log.error(f"malformed subarray entry {subarr_dict}")
                continue
            subarray_id = subarr_dict[SUBID_KEY]
            # check range
            if subarray_id < 1 or subarray_id > self.MAX_SUBARRAYS:
                log.error(
                    f"subarray {subarray_id} is out of range [1, {self.MAX_SUBARRAYS}]"
                )
                continue
            # collect this subarray's beams
            new_subarr_dict = {BEAMS_KEY: []}
            new_subarr_dict[SUBID_KEY] = subarray_id
            # a flag indicating poly. delays across all beams are valid:
            all_beams_ok = True
            for beam_dict in subarr_dict[BEAMS_KEY]:
                # ensure we have all expected keys
                if not all([_ in beam_dict for _ in beam_keys]):
                    log.error(
                        f"malformed beam entry in subarray {subarray_id}"
                    )
                    continue
                beam_id = beam_dict[BEAMID_KEY]
                # check beam ID is in valid range
                if not 0 < beam_id <= self.MAX_BEAM_ID:
                    log.error(
                        f"beam ID {beam_id} is out of range [1, {self.MAX_BEAM_ID}]"
                    )
                    continue
                valid_delay = beam_dict[VALID_KEY]
                subscr_valid = beam_dict[SUBSCR_KEY]
                new_subarr_dict[BEAMS_KEY].append(
                    {
                        BEAMID_KEY: beam_id,
                        VALID_KEY: valid_delay,
                        SUBSCR_KEY: subscr_valid,
                    }
                )
                # PST speciffic delays (if any)
                pst_delays_valid = True
                if PST_KEY in beam_dict:
                    new_subarr_dict[BEAMS_KEY][-1][PST_KEY] = beam_dict[
                        PST_KEY
                    ]
                    # check all PST delays and subscriptions are valid
                    for pst_beam in beam_dict[PST_KEY]:
                        pst_delays_valid &= (
                            pst_beam[VALID_KEY] and pst_beam[SUBSCR_KEY]
                        )
                        if not pst_delays_valid:
                            break
                all_beams_ok = (
                    all_beams_ok
                    and valid_delay
                    and subscr_valid
                    and pst_delays_valid
                )
            top_container.append(new_subarr_dict)
            station_delays_ok[subarray_id - 1] = int(all_beams_ok)
        return top_container, station_delays_ok

    def update_subscr_stats(
        self, current: dict, new: dict, summary: list
    ) -> (bool, dict, list):
        """
        Re-evaluate the validity of delays given the newly reported ('new')
        delay subscription validity.

        :param current: the current state of delays validity
        :param new: the newly reported delays validity
        :param summary: the summary of subarray delays validity
        :return: a tuple; bool indicate if the curent state has changed
                          dic  contains all the current state details
                          list a new subarray delays validity summary
        """
        modified = False
        # a copy of the current situation
        subarray_summary = summary.copy()
        for sub_id, affected_beams in new.items():
            # find a maching subarray
            for delay_subscription in current:
                if delay_subscription["subarray_id"] != sub_id:
                    continue
                # update beam.subscription_valid
                for beam in delay_subscription["beams"]:
                    if beam["beam_id"] not in affected_beams:
                        continue
                    if beam["subscription_valid"]:
                        modified = True
                        beam["subscription_valid"] = False
                        subarray_summary[sub_id - 1] = 0
        return modified, current, subarray_summary

    def combined_delays_summary(
        self, station: list, pst: list, pss: list
    ) -> list[int]:
        """Depending on loaded personality we may need to combine various types of
        polynomial delay summaries e.g. station and PST offset delays

        :param station: station delays
        :param pst:     PST offset delays
        :param pss:     PSS offset delays
        :return:  subarray delays validity summary
        """
        if self.personality == "corr":
            return station
        elif self.personality == "pst":
            return [min(a, b) for a, b in zip(station, pst)]
        elif self.personality == "pss":
            # TBD: PSS ofset delays; probably similar to PST
            pass

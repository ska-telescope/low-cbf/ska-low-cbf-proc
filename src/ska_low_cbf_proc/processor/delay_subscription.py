# -*- coding: utf-8 -*-
#
# (c) 2023 CSIRO Space and Astronomy.
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement
# See LICENSE.txt for more info.

"""Classes that subscribe to delay polynomial sources."""

import json
import logging
import math
import random
from abc import ABC
from dataclasses import dataclass
from enum import IntEnum
from json import JSONDecodeError
from queue import Queue
from threading import Lock, Thread, Timer

import tango
from tango import DevFailed, DeviceProxy, EventData, EventSystemFailed

PROCESS_EVTS_IN_CALLER_CONTEXT = False


class Evt(IntEnum):
    """Events relevant to delay subscription states."""

    ON_ENTRY = 0
    ON_EXIT = 1
    SUBSCRIBE = 2
    TIMEOUT = 3
    UNSUBSCRIBE = 4
    SUBSCRIBE_FAILURE = 5


class EvtHndlr(ABC):
    """Event handling logic."""

    def __init__(self, logger: logging.Logger):
        """
        Create an Event Handler.

        :param logger: for processing log messages.
        """
        self._cur_state = None
        self._evt_queue = Queue()
        self._logger = logger
        self._lock = Lock()  # protects self._is_processing
        self._is_processing = False

    def initial_state(self, initial_state):
        """Set starting state for handler."""
        self.transition(initial_state)
        self._process_evts()

    def transition(self, next_state):
        """Transition from one event handling state to another."""
        if self._cur_state is not None:
            self._cur_state(Evt.ON_EXIT, None)
        self._cur_state = next_state
        self._cur_state(Evt.ON_ENTRY, None)

    def add_evt(self, evt, evt_data):
        """Add an event to the queue of events to be processed."""
        self._evt_queue.put((evt, evt_data))
        # Ensure only one thread of execution processes events
        # at a time to get run-to-completion in-order behaviour
        with self._lock:
            if self._is_processing:
                return
            self._is_processing = True

        if PROCESS_EVTS_IN_CALLER_CONTEXT:
            # This can lead to deadlocks if caller is Tango event handler
            self._process_evts()
        else:
            # Use separate thread of execution to process queued events
            thr = Thread(
                target=self._process_evts,
                args=(),
            )
            thr.start()

    def _process_evts(self):
        """Process all events in queue until queue is empty."""
        self._lock.acquire()
        while not self._evt_queue.qsize() == 0:
            self._lock.release()

            # get next event & run current state's handling for the event
            evt, evt_data = self._evt_queue.get()
            self._cur_state(evt, evt_data)

            self._lock.acquire()
        self._is_processing = False
        self._lock.release()


@dataclass
class DelayPolyData:
    """Structure holding data associated with delay polynomials"""

    epoch: int  # seconds after J2000 that polynomial is valid
    first_valid_blk_no: int  # Integration block (0.849s) that poly is valid
    offset_sec: float  # time from 'epoch' to 'first_valid_blk_no'
    poly: dict  # poly info - data from attribute that SKA publishes
    valid_secs: float  # length of time after 'epoch' that polynomial is valid
    valid_blks: int  # number of blocks polynomial is valid


class DelayPolySubscriber(EvtHndlr):
    """Class handling subscriptions to delay polynomial devices."""

    MAX_QUEUED_RAW_POLYS = 10  # polys direct from Tango
    MAX_QUEUED_DELAY_POLYS = 12  # polys after grooming for time, dupes,

    def __init__(self, logger):
        """
        Create a Delay Polynomial Subscriber.

        :param logger: for processing log messages.
        """
        super().__init__(logger)

        self._dev_name = None  # Name of tango device we subscribe to
        self._tango_proxy = None  # proxy for device we subscribe to
        self._attr_name = None  # Name of attribute we subscribe to
        self._subscription_id = None  # id of successful subscription
        self._timer = None  # for retry of failed subscription
        self._raw_poly_q = Queue()  # Queue of delay polynomials from Tango
        self._subscription_failed = False
        self._poly_q = []  # Ordered (by time) list of groomed polynomials
        self._polys_avail = [None, None]  # The two polys to go in registers

        # Start in "idle state"
        self.initial_state(self._state_idle)
        logger.info("DelayPolySubscriber init complete")

    # Methods to handle events for each state

    def _state_idle(self, evt: Evt, evt_data):
        """Handle events in idle state."""
        if evt == Evt.SUBSCRIBE:
            self._dev_name, self._attr_name = evt_data
            self._do_try_subscribe()
            return
        # ON_ENTRY - nothing to do
        # ON_EXIT - nothing to do
        # TIMEOUT (impossible) - ignore
        # SUBSCRIBE_FAILURE - not possible in idle
        # UNSUBSCRIBE - ignore

    def _state_retry_wait(self, evt: Evt, _):
        """Retry event subscription."""
        if evt == Evt.ON_ENTRY:
            # start timer thread
            retry_secs = 10.0 + 2.0 * random.random()
            self._timer = Timer(retry_secs, self._do_retry)
            self._timer.start()
            return
        if evt == Evt.TIMEOUT:
            del self._timer
            self._timer = None
            self._do_try_subscribe()
            return
        if evt == Evt.UNSUBSCRIBE:
            self._timer.cancel()
            self._timer = None
            self.transition(self._state_idle)
            return
        # ON_EXIT - nothing to do
        # SUBSCRIBE_FAILURE - not possible in retry_wait
        # SUBSCRIBE - ignore

    def _state_subscribed(self, evt: Evt, _):
        """Handle Events received when successfully subscribed."""
        if evt == Evt.SUBSCRIBE_FAILURE:
            self.transition(self._state_retry_wait)
            return
        if evt == Evt.UNSUBSCRIBE:
            self.transition(self._state_idle)
            return
        if evt == Evt.ON_EXIT:
            self._logger.info(
                "Unsubscribing %s, id=%s",
                self._attr_name,
                self._subscription_id,
            )
            self._tango_proxy.unsubscribe_event(self._subscription_id)
            self._subscription_id = None
            del self._tango_proxy
            self._tango_proxy = None
            return
        # ON_ENTRY - nothing to do
        # SUBSCRIBE - ignore
        # TIMEOUT - ignore

    def _do_try_subscribe(self):
        """Attempt to subscribe to delay polynomial."""
        self._logger.info(
            "Try subscribing to %s/%s ...", self._dev_name, self._attr_name
        )
        try:
            # Attempt to get device proxy for subscription target
            self._tango_proxy = DeviceProxy(self._dev_name)
        except DevFailed:
            self._logger.info("Subscription fail: no Tango device)")
            self.transition(self._state_retry_wait)
            return

        try:
            # Attempt to subscribe to attribute
            self._subscription_id = self._tango_proxy.subscribe_event(
                self._attr_name,
                tango.EventType.CHANGE_EVENT,
                self._tango_event_receiver,
                stateless=True,
            )
        except EventSystemFailed:
            # Tango docs say this never occurs if stateless=True
            del self._tango_proxy
            self._tango_proxy = None
            self._logger.info("Subscription fail: Tango evt sys fail")
            self.transition(self._state_retry_wait)
            return

        # Successful subscription
        self._logger.info("Subscription success: id %s", self._subscription_id)
        self._logger.info("Tango proxy %s", self._tango_proxy)
        self._subscription_failed = False
        self.transition(self._state_subscribed)

    def _do_retry(self):
        """
        Retry subscribing to a delay polynomial attribute.

        Called on timeout in the retry state, for another attempt.
        """
        self.add_evt(Evt.TIMEOUT, None)

    def _tango_event_receiver(self, tango_data: EventData):
        """Process a tango delay-poly attribute update."""
        if tango_data.err:
            for err in tango_data.errors:
                self._logger.warning(
                    "Device:%s Attr:%s Error:%s Descr:%s",
                    tango_data.device,
                    tango_data.attr_name,
                    tango_data.event,
                    err.desc,
                )
                # EventData.event attribute is a bit vague about the nature of the failure
                # but the err.desc message changes depending on circumstances
                if tango_data.event == "change":
                    self._subscription_failed = True
            self.add_evt(Evt.SUBSCRIBE_FAILURE, None)
            return

        # delay attribute should be a JSON string
        try:
            value = json.loads(tango_data.attr_value.value)
            # self._logger.info("Delay update from %s", tango_data.attr_name)
        except JSONDecodeError:
            attr_name = f"{self._dev_name}.{self._attr_name}"
            self._logger.warning("Ignored non-JSON delay from " + attr_name)
            return

        # TODO could add reasonableness checks on the delay value ??
        self._raw_poly_q.put(value)

        # Limit number of polynomials queued
        while self._raw_poly_q.qsize() > self.MAX_QUEUED_RAW_POLYS:
            _ = self._raw_poly_q.get()

    # Interface to external callers

    def subscribe(self, tango_dev_name, tango_attr_name):
        """
        Subscribe to a tango delay device attribute.

        :param tango_dev_name: FQDN of device for subscription
        :param tango_attr_name: name of device attribute for subscription
        """
        self.add_evt(Evt.SUBSCRIBE, (tango_dev_name, tango_attr_name))

    def unsubscribe(self):
        """Unsubscribe from delay poly source (required for new subscription)."""
        self.add_evt(Evt.UNSUBSCRIBE, None)

    @property
    def failed(self) -> bool:
        """"""
        return self._subscription_failed

    def _get_next_raw_poly(
        self, fps_numerator: int, fps_denominator: int
    ) -> DelayPolyData | None:
        """
        Get the next polynomial from queue of raw polys direct from Tango.

        :param fps_numer: Numerator of frames-per-second fraction
        :param fps_denom: Denominator of frames-per-second fraction

        [ for info:
            std Correlator: numerator = 390625, denominator=331776
            PST: numerator= 309625, denominator = 23328
                (1/59.71968ms filterbank frames per second)
        ]

        :return: Delay polynomial inf, or None
        """
        if self._raw_poly_q.qsize() == 0:
            return None
        value = self._raw_poly_q.get()
        # extract the relevant parts of the SKA polynomial
        epoch = value["start_validity_sec"]
        poly = value["station_beam_delays"]
        valid_secs = value["validity_period_sec"]

        # COR 390625/331776 filterbank blocks/second ~= 1.1773
        # PST 390625/20736 filterbank blocks/second ~= 18.838
        first_valid_blk_no = math.ceil(
            (epoch * fps_numerator) / fps_denominator
        )
        offset_sec = (
            first_valid_blk_no * fps_denominator
        ) / fps_numerator - epoch

        valid_blks = int((valid_secs * fps_numerator) / fps_denominator)

        return DelayPolyData(
            epoch, first_valid_blk_no, offset_sec, poly, valid_secs, valid_blks
        )

    def queue_subscribed_polys(
        self, highest_blk_no: int, fps_numerator, fps_denominator, beam_name
    ) -> None:
        """
        Drain polynomials from raw Tango subscription queue and:
        - discard polys that are too old (compared to highest_blk_no),
        - discard oldest polys when there are too many to queue
        - discard later polys that have duplicate validity times

        :param highest_blk_no: highest filterbank block number encountered so far
        :param fps_numerator: numerator of the filterbank blks-per-second fraction
        :param fps_denominator: denominator of filterbank blks-per-second fraction
        """

        # pull all available polynomials into a groomed queue
        while True:
            poly_info = self._get_next_raw_poly(fps_numerator, fps_denominator)
            if poly_info is None:
                break
            if poly_info.epoch < 1.0:
                # Discard tmc-cspSubarrayLeafNode placeholder values
                continue
            self._poly_q.append(poly_info)
            # show polynomial summary info in logs
            # txt = f"New DelayPoly, valid T={poly_info.epoch} sec"
            # txt += f" [filter_blk = {poly_info.first_valid_blk_no}"
            # txt += f", sec_offset={poly_info.offset_sec:.2f}]"
            # self._logger.info(txt)

        # Check there is a delay polynomial available
        if len(self._poly_q) == 0:
            self._logger.info("No delay polys received for %s", beam_name)
            return

        # limit maximum poly queue size, discard oldest
        while len(self._poly_q) > self.MAX_QUEUED_DELAY_POLYS:
            self._poly_q.pop(0)

        # check polys for old, duplicates or epoch regress
        while len(self._poly_q) > 1:
            # Discard first poly when second poly starts to be used
            # (Expected normal operation)
            blkno2 = self._poly_q[1].first_valid_blk_no
            if blkno2 < highest_blk_no:
                self._poly_q.pop(0)
                continue
            # Drop first poly if DelayPoly Epochs went backward in time
            # (can occur when delay-poly-generator epoch is manually set)
            epo1 = self._poly_q[0].epoch
            blkno1 = self._poly_q[0].first_valid_blk_no
            last_blkno = self._poly_q[-1].first_valid_blk_no
            if last_blkno < blkno1:
                self._logger.info(
                    "DelayPoly epoch regress, discard EPOCH=%s blkno=%s",
                    epo1,
                    blkno1,
                )
                self._poly_q.pop(0)
                continue
            # remove duplicate poly (should never occur)
            if blkno1 == blkno2:
                self._logger.info(
                    "Discard duplicate poly blkno=%s [epo=%s]",
                    blkno1,
                    epo1,
                )
                self._poly_q.pop(0)
                continue
            # no duplicates, none too old -> completed checks
            break
        return

    def prep_polys_for_use(self, beam_name, highest_blk_no) -> bool:
        """
        Get first two delay polynomials (from self._poly_q) that should be
        currenly used for each beam and update/save in self._polys_avail
        """
        was_poly_updated = False

        # check that at least one polynomial has been received and queued
        if len(self._poly_q) == 0:
            return was_poly_updated

        # Get block numbers of polynomials currently ready for use
        blk_nos_avail = []
        for poly_info in self._polys_avail:
            if poly_info is None:
                blk_nos_avail.append(None)
            else:
                blk_nos_avail.append(poly_info.first_valid_blk_no)

        # Program first queued polynomial into FPGA if not present already
        first_poly = self._poly_q[0]
        blkno1 = first_poly.first_valid_blk_no
        if blkno1 not in blk_nos_avail:
            was_poly_updated = True
            # Fill empty buffer if any
            if blk_nos_avail[0] is None:
                self._polys_avail[0] = first_poly
                blk_nos_avail[0] = first_poly.first_valid_blk_no
            elif blk_nos_avail[1] is None:
                self._polys_avail[1] = first_poly
                blk_nos_avail[1] = first_poly.first_valid_blk_no
            else:
                # None empty, replace oldest poly with this one
                if blk_nos_avail[1] > blk_nos_avail[0]:
                    self._polys_avail[0] = first_poly
                    blk_nos_avail[0] = first_poly.first_valid_blk_no
                else:
                    self._polys_avail[1] = first_poly
                    blk_nos_avail[1] = first_poly.first_valid_blk_no

        # Do we program second queued polynomial into FPGA? (usual case)
        if len(self._poly_q) > 1:
            second_poly = self._poly_q[1]
            blkno2 = second_poly.first_valid_blk_no
            if blkno2 not in blk_nos_avail:
                # Fill empty buffer if any
                if blk_nos_avail[0] is None:
                    self._polys_avail[0] = second_poly
                    was_poly_updated = True
                elif blk_nos_avail[1] is None:
                    was_poly_updated = True
                    self._polys_avail[1] = second_poly
                else:
                    #  If first queued poly is active, time to write second
                    if blkno1 < highest_blk_no:
                        was_poly_updated = True
                        # don't overwrite the first queued poly
                        if blk_nos_avail[1] == blkno1:
                            self._polys_avail[0] = second_poly
                        else:
                            self._polys_avail[1] = second_poly

        # TODO: remove this para when certain debug is no longer needed
        # debug print start-validity for register writes
        # if was_poly_updated:
        #     txt = f"{beam_name}: New delays: filter_valid=["
        #     for count, poly_info in enumerate(self._polys_avail):
        #         if poly_info is None:
        #             txt += " None"
        #         else:
        #             txt += f" {poly_info.first_valid_blk_no} "
        #         if count == 0:
        #             txt += ","
        #     txt += f"] filter_now={highest_blk_no:.2f}"
        #     self._logger.info(txt)

        return was_poly_updated

    def get_polys_avail(self) -> list:
        """
        Return the polynomials that should be programmed into
        buffer[0] and buffer[1] of the FPGA
        """
        return self._polys_avail

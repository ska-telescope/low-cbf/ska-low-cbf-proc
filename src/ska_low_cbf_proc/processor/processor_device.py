# -*- coding: utf-8 -*-
#
# (c) 2025 CSIRO Astronomy and Space.
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement
# See LICENSE.txt for more info.

"""
FPGA accelerator card register interface wrapper.
LowCbfProcessor implements a TANGO interface to AlveoCL.
"""
import json
import logging
import os
import time
from threading import Lock
from typing import Any, Iterable

import numpy as np
import tango
import tango.server
from ska_control_model import CommunicationStatus, HealthState, SimulationMode
from ska_tango_base import SKABaseDevice
from ska_tango_base.commands import DeviceInitCommand, FastCommand, ResultCode
from ska_tango_base.control_model import AdminMode, TestMode
from tango import AttReqType, AttributeProxy, AttrQuality
from tango.server import attribute, command, device_property

from ska_low_cbf_proc import release
from ska_low_cbf_proc.processor.device_proxy import MccsDeviceProxy
from ska_low_cbf_proc.processor.fpga_hw_health import HwHealth
from ska_low_cbf_proc.processor.function_health import (
    ATTR_DRIVER_OK,
    ATTR_FW_LOADED,
    FunctionHealth,
)
from ska_low_cbf_proc.processor.process_health import (
    ATTR_DELAY_SUBSCR_OK,
    ATTR_DELAY_VALID,
    ATTR_SPEAD_PACKETS,
    ProcessingHealth,
)
from ska_low_cbf_proc.processor.processor_component_manager import (
    ProcessorComponentManager,
)

__all__ = ["LowCbfProcessor", "main"]

# numbering subarrays is 1 based, so we'll often use MAX_SUBARRAYS-1 in code
MAX_SUBARRAYS = ProcessorComponentManager.MAX_SUBARRAYS
UNUSED = ProcessorComponentManager.UNUSED
MAX_BEAM_ID = ProcessorComponentManager.MAX_BEAM_ID
MAX_BEAMS = 64  # Max. beams per subarray (for attribute array dimensions)

# how often to read various health check points (in seconds)
HW_HEALTH_CHECK_PERIOD = 15
FUNC_HEALTH_CHECK_PERIOD = 5
HW_HEALTH_CONFIG_FILE = "u55c_hw_mon.yaml"
FUNC_HEALTH_CONFIG_FILE = "u55c_func_mon.yaml"
PROC_HEALTH_CONFIG_FILE = "u55c_proc_mon.yaml"


def overridable(func):
    """Decorator to apply test mode overrides to attributes."""
    attr_name = func.__name__

    def override_attr_in_test_mode(self, *args, **kwargs):
        """Override attribute when test mode is active and value specified."""
        if (
            self._test_mode == TestMode.TEST
            and attr_name in self._attr_overrides
        ):
            return _override_value_convert(
                attr_name, self._attr_overrides[attr_name]
            )

        # Test Mode not active, normal attribute behaviour
        return func(self, *args, **kwargs)

    return override_attr_in_test_mode


def _override_value_convert(attr_name: str, value: Any) -> Any:
    """Automatically convert types for attr overrides (e.g. enum label -> int)."""
    enum_attrs = {
        "healthState": HealthState,
        "health_hardware": HealthState,
        "health_process": HealthState,
        "health_function": HealthState,
    }
    """Map attribute name to enum class"""
    if attr_name in enum_attrs and type(value) is str:
        return enum_attrs[attr_name][value]

    # default to no conversion
    return value


class LowCbfProcessor(SKABaseDevice):
    """
    Low CBF FPGA control & monitoring TANGO Device.
    """

    def __init__(self, *args, **kwargs):
        # Initialise the Tango device
        self._personality = None
        # list of delay poly statistics, to be published as Tango attribute
        self._stats_delay = []
        # dict of FPGA mode statistics, published as Tango attribute
        self._stats_mode = {
            "ready": False,
            "firmware": None,
        }
        # dict of IO packet statistics, published via Tango attribute
        self._stats_io = {}
        self._extra_mode_info = {}
        self._station_delay_valid = []
        """An array of dictionaries summarising station delay polinomial validity
        per subarray-beam. When applicable (beamformer personality) includes
        validity of PST delays associated with each station beam.
        """
        # Example:
        # [
        #   {
        #     "subarray_id": 1,
        #     "beams":
        #     [
        #       {
        #         "beam_id": 1,
        #         "valid_delay": True,
        #         "subscription_valid": True,
        #          pst : [   ... available ONLY when PST FW is loaded
        #            {
        #                "name":              "delay_device_name",
        #                "valid_delay":        true,
        #                "subscription_valid": true,
        #            },
        #          ]
        #       },
        #       {
        #         "beam_id": 2,
        #         "valid_delay": True,
        #         "subscription_valid": True
        #       }
        #     ]
        #  },
        #  {
        #     "subarray_id": 2,
        #     "beams": [ .....

        self._subarray_delays_summary = [UNUSED] * MAX_SUBARRAYS
        """This is the top level summary we pass onto Subarray device"""

        self._station_delays_summary = [UNUSED] * MAX_SUBARRAYS
        self._pst_offset_delays_summary = [UNUSED] * MAX_SUBARRAYS
        self._pss_offset_delays_summary = [UNUSED] * MAX_SUBARRAYS

        self.hw_health_mon = None  # hardware health monitor object
        self._hwname_to_attrname = {}  # key=mon point name, value=attr name
        self._hwlims = {}  # key = mon point name, value=(min, max) tuple
        self._attr_overrides = {}  # key=attr name, value=override for param
        self._cur_attr_value = {}
        """Current health attrib value: key = mon point name, value=current hw value"""
        self._last_hwvalue = {}  # key = mon point name, value=current hw value
        self._last_fnvalue = {}
        """Last reported functional parameter (health related) values; used
        when override is changed
        key = observed point name, value=current value"""

        # summary of 3 health categories
        self._health_hardware = HealthState.OK
        self._lock = Lock()

        self._registered_wi_allocator = False
        self._is_fpga_used = False
        self._saved_admin_mode = None

        # Initialise the Tango device
        # note: InitCommand (see below) runs during the call
        super().__init__(*args, **kwargs)

    # Properties (value in database, loaded by helm chart)
    allocator_device = device_property(
        dtype=("DevString",),
        # mandatory=True, # TODO tests fail if this is added, How to add it?
        # TODO FIXME Helm chart should overwrite this default but doesn't
        default_value="low-cbf/allocator/0",
        doc="Tango device with allocation info",
    )

    # If device properties exist they override (change) environment variables
    stn_delay_sign = device_property(
        dtype=str,
        mandatory=False,
        default_value="",
        doc="Sign for station-beam delay poly values? (pos/neg/'')",
    )

    pst_delay_sign = device_property(
        dtype=str,
        mandatory=False,
        default_value="",
        doc="Sign for PST beam delay poly values? (pos/neg/'')",
    )

    pss_delay_sign = device_property(
        dtype=str,
        mandatory=False,
        default_value="",
        doc="Sign for PSS beam delay poly values? (pos/neg/'')",
    )

    pst_delay_format = device_property(
        dtype=str,
        mandatory=False,
        default_value="",
        doc="PST delay poly format? (full/diff/'')",
    )

    pss_delay_format = device_property(
        dtype=str,
        mandatory=False,
        default_value="",
        doc="PSS delay poly format? (full/diff/'')",
    )

    HEALTH_CLEANUP_PERIOD = 3
    """How often to run health cleanup code (e.g. DEGRADED times out to OK)
    in seconds"""

    # --------------
    # Initialization
    # --------------
    def init_command_objects(self):
        """
        Initialises the command handlers for commands supported by this device.
        """
        super().init_command_objects()
        self.register_command_object(
            "StartRegisterLog",
            self.StartRegisterLogCommand(
                tango_device=self, logger=self.logger
            ),
        )
        self.register_command_object(
            "StopRegisterLog",
            self.StopRegisterLogCommand(tango_device=self, logger=self.logger),
        )

    def create_component_manager(self):
        """
        Create and return a component manager for this device.

        :return: a component manager for this device.
        """
        processor_component_manager = ProcessorComponentManager(
            self.logger,
            self._communication_state_changed,
            self._component_definition_changed,
            self._component_state_changed,
            self.fpga_usage,
            self.update_mode_stats,
            self.update_delay_stats,
            self.update_io_stats,
            self.update_subscr_stats,
        )
        return processor_component_manager

    class InitCommand(
        DeviceInitCommand
    ):  # pylint: disable=too-few-public-methods
        """Initialisation command class for this ProcessorDevice."""

        def do(self):  # pylint: disable=arguments-differ
            """Initialises the attributes and properties of ProcessorDevice."""
            self._device._version_id = release.version
            self._device._build_state = (
                f"{release.name}, {release.version}, {release.description}"
            )

            self._device.logger.info(
                "stn_delay_sign property: %s", self._device.stn_delay_sign
            )
            if self._device.stn_delay_sign != "":
                # override environment variable with property value
                os.environ["STN_DELAY_SIGN"] = self._device.stn_delay_sign

            self._device.logger.info(
                "pst_delay_sign property: %s", self._device.pst_delay_sign
            )
            if self._device.pst_delay_sign != "":
                os.environ["PST_DELAY_SIGN"] = self._device.pst_delay_sign

            self._device.logger.info(
                "pss_delay_sign property: %s", self._device.pss_delay_sign
            )
            if self._device.pss_delay_sign != "":
                os.environ["PSS_DELAY_SIGN"] = self._device.pss_delay_sign

            self._device.logger.info(
                "pst_delay_format property: %s",
                self._device.pst_delay_format,
            )
            if self._device.pst_delay_format != "":
                os.environ["PST_DELAY_FORMAT"] = self._device.pst_delay_format

            self._device.logger.info(
                "pss_delay_format property: %s",
                self._device.pss_delay_format,
            )
            if self._device.pss_delay_format != "":
                os.environ["PSS_DELAY_FORMAT"] = self._device.pss_delay_format

            self._device._attribute_quality = AttrQuality.ATTR_INVALID
            self._device._processor_state = {}

            # set initial value of AdminMode (before allocator subscribe)
            # default ONLINE, but if env var is set but invalid use OFFLINE
            admin_env = os.getenv("INITIAL_ADMINMODE", default="online")
            try:
                initial_admin_mode = AdminMode[admin_env.upper()]
            except KeyError:
                initial_admin_mode = AdminMode.OFFLINE  # invalid env var
                self.logger.warning("invalid INITIAL_ADMINMODE %s", admin_env)
            self._device.processor_update_admin_mode(initial_admin_mode)
            self._device._is_fw_loaded = False

            # If we have real alveo hardware (with a non-zero serial number),
            # allocator will exist and we can subscribe to allocation events
            serial_no = self._device.get_serialNumber()
            self.logger.info(f"alveo_serial_no = {serial_no}")
            isolated = "ISOLATED" in os.environ
            if (serial_no != "000000000000") and not isolated:
                self._device.subscribe_to_allocator(
                    self._device.allocator_device
                )
                # since processors are always last to deploy, the allocator
                # should be up and ready to receive command from here
                self._device._register_with_allocator()

            for attr in (
                "healthStateJson",
                "simulationMode",
                "testMode",
                "stats_delay",
                "stats_io",
                "subarrayDelaysSummary",
            ):
                self._device.set_change_event(attr, True, False)

            for attr in (
                "beamIds",
                "serialNumber",
                "stationDelayValid",
                "stats_mode",  # contains firmware, fw_version
                "subarrayIds",
                ATTR_FW_LOADED,
                "healthState",
                "health_hardware",
                "health_function",
                "health_process",
            ):
                self._device.set_change_event(attr, True, False)
                self._device.set_archive_event(attr, True, False)
            self._device._personality = (
                None  # pylint: disable=protected-access
            )
            self._device._health_registers = (
                {}
            )  # # pylint: disable=protected-access
            self._device._health_reg_loaded: bool = False
            self._device._reg_health_state = (
                {}
            )  # pylint: disable=protected-access
            self._device._subarrayIds = []  # pylint: disable=protected-access
            self._device._subarray_beams = np.zeros(
                (MAX_SUBARRAYS, MAX_BEAMS), dtype=int
            )
            self._device._healthState = (
                HealthState.OK
            )  # pylint: disable=protected-access

            self._device._process_delay_poly_valid = True
            self._device._process_delay_subscription_ok = True

            # hardware health monitoring thread
            if serial_no != "000000000000":
                self._device.hw_health_mon = HwHealth(
                    HW_HEALTH_CONFIG_FILE,
                    self._device.component_manager.xrt_info,
                    self._device.hw_attr_update,
                    self._device.logger,
                )
                mon_lims = self._device.hw_health_mon.get_hwmon_lims()
                self._device.logger.info("HW limits: %s", str(mon_lims))
                self._device.hw_attrs_setup(mon_lims)
                # configure/launch function health monitoring:
                self._device.fn_health_mon = FunctionHealth(
                    FUNC_HEALTH_CONFIG_FILE,
                    self._device,
                    self._device.logger,
                )
                # configure processing health monitoring:
                self._device.proc_health_mon = ProcessingHealth(
                    PROC_HEALTH_CONFIG_FILE,
                    self._device,
                    self._device.logger,
                )
                # launch threads AFTER all health monitors are created:
                self._device.hw_health_mon.start_hw_monitoring(
                    HW_HEALTH_CHECK_PERIOD
                )
                self._device.fn_health_mon.start_monitoring(
                    FUNC_HEALTH_CHECK_PERIOD
                )

            attr = tango.server.attribute(
                name=ATTR_DRIVER_OK,
                dtype="bool",
                label="FPGA driver OK",
                fget=self._device._read_driver_ok,
            ).to_attr()
            attr.set_change_event(True, False)
            attr.set_archive_event(True, False)
            self._device.add_attribute(attr, self._device._read_driver_ok)

            for name, label in (
                (ATTR_DELAY_SUBSCR_OK, "Delay poly. subscription ok"),
                (ATTR_DELAY_VALID, "Delay polynomials valid"),
                (ATTR_SPEAD_PACKETS, "SPS SPEAD packets arriving"),
            ):
                attr = tango.server.attribute(
                    name=name,
                    dtype="bool",
                    label=label,
                ).to_attr()
                attr.set_change_event(True, False)
                attr.set_archive_event(True, False)
                if serial_no != "000000000000":
                    self._device.add_attribute(
                        attr, self._device._read_proc_health_attr
                    )
            if serial_no != "000000000000":
                self._device.summarise_health_state()
            self._completed()
            message = "LowCbfProcessor init complete"
            self._device.logger.info(message)
            return ResultCode.OK, message

    def hw_attrs_setup(self, mon_lims: dict) -> None:
        """
        Create hardware-monitoring attributes

        :param mon_lims: dictionary of min, max, label values
                         key=monitoring point name
        """
        for name, cfg in mon_lims.items():
            attr_name = "hardware_" + name
            self._hwname_to_attrname[name] = attr_name
            self._hwlims[attr_name] = (cfg["min"], cfg["max"])
            self._cur_attr_value[attr_name] = 0.0
            attr = tango.server.attribute(
                name=attr_name,
                dtype=float,
                access=tango.AttrWriteType.READ,
                label=cfg.get("label", attr_name),
                unit=cfg.get("units", ""),
                fget=self._health_attr_read,
                fset=None,
            )
            self.logger.info("add hw attribute: %s", attr_name)
            self.add_attribute(attr)
            self.set_change_event(attr_name, True, False)
            # self.set_archive_event(attr_name, True, False)

    def _health_attr_read(self, health_attrib: tango.Attribute):
        """Read the value of a health-monitoring attribute"""
        attribute_name = health_attrib.get_name().lower()
        assert attribute_name in self._cur_attr_value, "Unknown attribute"
        health_attrib.set_value(self._cur_attr_value[attribute_name])

    def hw_attr_update(self, values: dict[str, float]) -> None:
        """
        Update values of hardware-monitoring attributes (and health)

        :param values: dict key=monitoring point name, value=hardware value
        """
        self._last_hwvalue = values
        within_range = True
        for name, value in values.items():
            attr_name = self._hwname_to_attrname[name]
            # is this value being overridden for test?
            if attr_name in self._attr_overrides:
                value = self._attr_overrides[attr_name]

            lim_min, lim_max = self._hwlims[attr_name]
            within_range = within_range and (lim_min <= value <= lim_max)
            if self._cur_attr_value[attr_name] == value:
                continue
            self._cur_attr_value[attr_name] = value
            self.push_change_event(attr_name, value)

        # Update health
        health = HealthState.OK if within_range else HealthState.FAILED
        if self._health_hardware != health:
            self._health_hardware = health
            self.push_change_event("health_hardware", health)
            self.push_archive_event("health_hardware", health)
            self.summarise_health_state()

    def _register_with_allocator(self):
        """Call when registering this processor (FSP) with Allocator"""
        assert hasattr(
            self, "_allocator_proxy"
        ), "First run SubscribeToAllocator command"

        self._allocator_proxy.InternalRegisterAlveo(
            json.dumps(
                {
                    "serial": self.get_serialNumber(),
                    "fqdn": self.get_name(),
                    "hw": "u55c",  # TODO get from FPGA somehow
                    "status": self._admin_mode,
                }
            )
        )
        self._registered_wi_allocator = True

    # -------------------------
    # Tango Command definitions
    # -------------------------
    @command(dtype_in=None, dtype_out=None)
    def Register(self):
        """
        Manually register this Processor==FSP with Allocator
        (This command only used for laptop tests after setting serial number)
        """
        self._register_with_allocator()

    @command(
        dtype_in="DevString",
        doc_in="File Name",
    )
    def StartRegisterLog(self, argin):
        """
        Start register transaction logging.

        :param argin: filename
        """
        command_object = self.get_command_object("StartRegisterLog")
        return command_object(argin)

    @command(dtype_in=None)
    def StopRegisterLog(self):
        """Stop register transaction logging."""
        command_object = self.get_command_object("StopRegisterLog")
        return command_object()

    @command(dtype_in=str, dtype_out=None)
    def SubscribeToAllocator(self, argin):
        """
        Temp method called to cause Processor to subscribe to events from
        a Tango device that has attributes providing allocation info

        :param argin: A string containing a Tango device name. Device must have
            the three ``internal_XXX`` attributes eg ``low-cbf/allocator/0``
        """
        device_name = argin
        self.subscribe_to_allocator(device_name)

    def subscribe_to_allocator(self, tango_device_name) -> None:
        """
        Implementation of subscription to allocation attributes on allocator

        :param tango_device_name: eg 'low-cbf/allocator/0'
        """
        self.logger.info(f"Subscribe alloc events from {tango_device_name}")
        self._allocator_proxy = MccsDeviceProxy(
            tango_device_name, self.logger, connect=False
        )

        alloc_evt_hndlrs = (
            ("internal_alveo", self.component_manager.alloc_alveo_evt),
            ("internal_subarray", self.alloc_subarray_evt),
        )
        for attr_name, hndlr in alloc_evt_hndlrs:
            self._allocator_proxy.evt_sub_on_connect(attr_name, hndlr)
        # try:
        #     # self.logger.info("Before subscribe")
        self._allocator_proxy.connect(max_time=120.0)
        # except:
        #     self.logger.info("Connect to allocator failed")

    def alloc_subarray_evt(self, attr_name, evt_json, quality):
        """
        Handler called when event is pushed for "internal_subarray" attribute
        This event's data indicates subarray scanning/not-scanning

        :param evt_json: json string encoding a dictionary that is keyed by
        subarray_id.
        """
        # forward to component manager (main subscriber)
        self.component_manager.alloc_subarray_evt(attr_name, evt_json, quality)
        # ProcessingHealth also wants to know about subarray obsState SCANNING
        if attr := getattr(self, "proc_health_mon", None):
            attr.subarray_event(evt_json)

    @command(
        dtype_in=str,
        doc_in="JSON dict with name, offset, length fields",
        dtype_out=str,
    )
    def DebugRegRead(self, argin):
        """
        Read a FPGA register, or set of contiguous registers

        :param argin: JSON-encoded dictionary, eg:
            ``{"name":"reg_name", "offset": 0, "length": 1}``
        :return: JSON-encoded list of 32-bit hex register values
        """
        return self.component_manager.read_register(argin)

    @command(
        dtype_in=str,
        doc_in="dict with name, offset, value fields",
        dtype_out=None,
    )
    def DebugRegWrite(self, argin):
        """
        Write a FPGA regoster

        ``argin`` example:

        .. code:: python

           {
               "name":   "reg_name",
               "offset": 0,
               "value":  0
           }

        :param argin: JSON-encoded dictionary
        """
        self.component_manager.write_register(argin)

    @command(
        dtype_in=str,
        doc_in="dict with hbm number & output directory fields",
        dtype_out=None,
    )
    def DebugDumpHbm(self, argin):
        """
        Capture HBM contents and write to file

        :param argin: JSON dictionary ``{"hbm": N, "dir": name}``
        """
        self.component_manager.dump_hbm(argin)

    @command(
        dtype_in=str,
        doc_in="memory sharing string eg '3Gi:3Gi:3Gi:512Mi:512Mi'",
        dtype_out=None,
    )
    def DebugSetMemConfig(self, argin):
        """
        Set the memory description string used for the FPGA.

        ``argin`` example '3Gi:3Gi:3Gi:512Mi:512Mi'
        Must be done before loading FPGA.
        Empty string to undo: then next personality will use "usual" strings
        """
        self.component_manager.set_mem_config(argin)

    # -------------------
    # SKA Command Objects
    # -------------------

    class StartRegisterLogCommand(FastCommand):
        """
        Command class to start register logging
        """

        def __init__(self, tango_device, logger):
            super().__init__(logger=logger)
            self._tango_device = tango_device

        def do(self, argin):
            filename = argin
            self.logger.info(
                "Logging FPGA register transactions to %s", filename
            )
            self._tango_device.component_manager.log_to_file(filename)
            # attempt to prevent register log from reaching Tango logger
            # (doesn't work... upstream bug?)
            self._tango_device.logger.setLevel(logging.INFO)
            for handler in self._tango_device.logger.handlers:
                self.logger.info(f"Setting {handler} to INFO level")
                handler.setLevel(logging.INFO)
                self.logger.info(f"It's now: {handler}")
            return ResultCode.OK, "Started register logging"

    class StopRegisterLogCommand(FastCommand):
        """
        Command class to stop register logging.
        """

        def __init__(self, tango_device, logger):
            super().__init__(logger=logger)
            self._tango_device = tango_device

        def do(self):
            self.logger.info("Ceasing to log FPGA register transactions")
            self._tango_device.component_manager.stop_log_to_file()
            return ResultCode.OK, "Stopped register logging"

    # ------------------
    # Attributes methods
    # ------------------

    # We override SKA-Tango-BASE adminMode attribute to provide the option
    # to disallow adminMode changes while the processor is in use.
    @attribute(dtype=AdminMode, memorized=True, hw_memorized=True)
    def adminMode(self: SKABaseDevice) -> AdminMode:
        """
        Read the Admin Mode of the device.

        :return: Admin Mode of the device
        """
        return self._admin_mode

    @adminMode.write  # type: ignore[no-redef]
    def adminMode(self: SKABaseDevice, value: AdminMode) -> None:
        """
        Set the Admin Mode of the device. Overide of ska-tango-base
        to prevent being set offline while in use

        :param value: requested adminMode of the device.
        """
        if value == self._admin_mode:
            return
        self.processor_update_admin_mode(value)  # processor-specific AdminMode

    def fpga_usage(self, is_used: bool):
        """
        Callback: called when FPGA usage is updated (from allocator event).
        """
        self._is_fpga_used = is_used
        # can delete any saved adminmode if fpga becomes unused
        if not is_used:
            self._saved_admin_mode = None

    def processor_update_admin_mode(self, value):
        """
        Modified adminMode attribute write for LowCBF that is able to
        disallow adminMode change when the Processor FPGA is in use
        """
        # Special handling of AdminMode writes when FPGA is in use
        if self._is_fpga_used:
            # Note: If here, current adminmode must be engineering or online.
            # First, does adminMode env var allow any changes at all?
            env_var = os.getenv("ADMIN_CHANGE_WHILE_USED", default="deny")
            if env_var.lower() != "allow":
                raise ValueError("Can't change adminMode while FPGA is in use")

            if value == AdminMode.OFFLINE:  # allow OFFLINE
                # Save current adminmode so we can return to it
                self._saved_admin_mode = self._admin_mode
            elif value == self._saved_admin_mode:  # allow return to saved
                self._saved_admin_mode = None
            else:  # Deny all other adminmode changes
                if self._saved_admin_mode is None:
                    raise ValueError(
                        "Can't change adminMode when FPGA is in use"
                    )
                else:
                    txt = (
                        f"OFFLINE in adminmode {self._saved_admin_mode}, "
                        "can only return to same adminmode"
                    )
                    raise ValueError(txt)

        # copy of ska_control_model code block
        if value == AdminMode.NOT_FITTED:
            self.admin_mode_model.perform_action("to_notfitted")
        elif value == AdminMode.OFFLINE:
            self.admin_mode_model.perform_action("to_offline")
            self.component_manager.stop_communicating()
        elif value == AdminMode.ENGINEERING:
            self.admin_mode_model.perform_action("to_engineering")
            self.component_manager.start_communicating()
        elif value == AdminMode.ONLINE:
            self.admin_mode_model.perform_action("to_online")
            self.component_manager.start_communicating()
        elif value == AdminMode.RESERVED:
            self.admin_mode_model.perform_action("to_reserved")
        else:
            raise ValueError(f"Unknown adminMode {value}")

        # Tell alveo if it's in an adminMode where it should operate
        administratively_enabled = self._admin_mode in [
            AdminMode.ONLINE,
            AdminMode.ENGINEERING,
        ]
        self.component_manager.set_fpga_enabled(administratively_enabled)

        # convey new adminMode to allocator by re-registering
        serial_no = self.get_serialNumber()
        isolated = "ISOLATED" in os.environ
        if (serial_no != "000000000000") and not isolated:
            # only re-register if first registration has been completed
            if self._registered_wi_allocator:
                self._register_with_allocator()

    @attribute(dtype=SimulationMode)
    def simulationMode(self) -> SimulationMode:
        """
        Simulation mode determined by our underlying FPGA abstraction object

        :return: ``SimulationMode.TRUE`` if FPGA is simulated,
                 ``SimulationMode.FALSE`` otherwise
        """
        return SimulationMode(self.component_manager.simulation_mode)

    @attribute(dtype=str)
    def serialNumber(self) -> str:
        """Get the FPGA's serial number (defaults to 12 zeroes)"""
        return self.get_serialNumber()

    @serialNumber.setter
    def serialNumber(self, serial_no: str) -> None:
        """Set Alveo serial number (for testing)"""
        self.component_manager._serial_no = serial_no

    def get_serialNumber(self):
        """non-tango-attribute method to read serial number"""
        return self.component_manager._serial_no

    @attribute(dtype=str)
    def stats_delay(self) -> str:
        """Get internal per-subarray delay polynomial statistics"""
        return json.dumps(self._stats_delay)

    def update_delay_stats(self, new_stats: list) -> None:
        """
        Update attribute with latest subarray statistics
        One list entry for each subarray-beam
        FIXME describe data structure
        """
        assert isinstance(new_stats, list), "Subarray stats should be list"
        changed = new_stats != self._stats_delay
        self._stats_delay = new_stats
        stats_delay_str = json.dumps(new_stats)
        if changed:
            self.push_change_event("stats_delay", stats_delay_str)
            self._update_subarray_IDs(new_stats)
            self._update_delays_valid(new_stats)

    def update_subscr_stats(self, subscr: dict) -> None:
        """Called when a failure in delay polynomial subscription is detected.
        See FpgaMgrCorr._failed_delay_subscr comments for dictionary structure.
        In the same breath it calculates a summary (delays valid/invalid) for each
        subarray and propagates changes (if any).
        """
        (
            modified,
            new_stats,
            new_summary,
        ) = self.component_manager.update_subscr_stats(
            self._station_delay_valid, subscr, self._station_delays_summary
        )
        if not modified:
            return
        # publish the details to subscribers
        json_str = json.dumps(self._station_delay_valid)
        self.push_change_event("stationDelayValid", json_str)
        self.push_archive_event("stationDelayValid", json_str)
        self._update_subarray_delays_summary(new_summary)

    @attribute(dtype=str, doc="Subarray-Beam delay polynomial validity (JSON)")
    def stationDelayValid(self) -> str:
        """Indicates whether subarray-beam delay polynomials are valid;
        see the comment at
        :py:attr:`~.LowCbfProcessor._station_delay_valid`
        for format definition
        """
        return json.dumps(self._station_delay_valid)

    def _update_delays_valid(self, delay_stats: list) -> None:
        """Re-evaluate and update delay polynomial validity"""
        (
            station_delay_valid,
            station_delays_ok,
        ) = self.component_manager.update_delays_valid(delay_stats)
        if self._station_delay_valid != station_delay_valid:
            self._station_delay_valid = station_delay_valid
            json_str = json.dumps(station_delay_valid)
            self.push_change_event("stationDelayValid", json_str)
            self.push_archive_event("stationDelayValid", json_str)
            self._update_subarray_delays_summary(station_delays_ok)
            # forward delay poly. details to ProcessingHealth
            self.proc_health_mon.attr_update(station_delay_valid)

    @attribute(
        dtype=["DevUShort"],
        max_dim_x=MAX_SUBARRAYS,
        doc="Delay validity value per subarray",
    )
    def subarrayDelaysSummary(self) -> list:
        """
        Return an array (one entry for each Subarray) of delay polynomial validity
        indicators for subarray beams processed here. Permitted values are

        - 0: INVALID
        - 1: VALID
        - 2: UNUSED

        This is consumed by LowCbfSubarray device
        """
        return self._subarray_delays_summary

    def _update_subarray_delays_summary(self, value: list) -> None:
        """Update delay validity per subarray"""
        if self._station_delays_summary != value:
            self._station_delays_summary = value
            # what we report to Subarray is a combination of constituent
            # delay validity summaries
            combined_summary = self.component_manager.combined_delays_summary(
                self._station_delays_summary,
                self._pst_offset_delays_summary,
                self._pss_offset_delays_summary,
            )
            if self._subarray_delays_summary != combined_summary:
                self._subarray_delays_summary = combined_summary
                self.push_change_event(
                    "subarrayDelaysSummary", combined_summary
                )

    @attribute(dtype=str)
    def stats_mode(self) -> str:
        """Get fpga mode statistics (eg firmware loaded)

        returned value example

        .. code:: python

           {
               "ready":      true,
               "firmware":   "vis::gitlab",
               "fw_version": "0.1.0",
               "fw_personality": "CORR"
           }

        :return: JSON string
        """
        mode_stats = {key: self._stats_mode[key] for key in self._stats_mode}
        mode_stats.update(self._extra_mode_info)
        return json.dumps(mode_stats)

    def update_mode_stats(
        self, fw_name: str, is_ready: bool, extra=None
    ) -> None:
        """
        Update Tango "stats_mode" attribute with latest FPGA mode statistics

        "fw_name" and "is_ready" keys are always present. A dictionary of extra
        key-values may also be published (eg firmware version, build type, etc)
        """
        self._stats_mode["firmware"] = fw_name
        self._stats_mode["ready"] = is_ready
        if isinstance(extra, dict):
            self._extra_mode_info = extra
        else:
            self._extra_mode_info = {}
        mode_stats = self._stats_mode.copy()
        mode_stats.update(self._extra_mode_info)
        values = json.dumps(mode_stats)
        self.push_change_event("stats_mode", values)
        self.push_archive_event("stats_mode", values)
        # update 'function_firmware_loaded' attribute
        if self._is_fw_loaded != is_ready:
            self._is_fw_loaded = is_ready
            self.push_change_event(ATTR_FW_LOADED, is_ready)
            self.push_archive_event(ATTR_FW_LOADED, is_ready)

    @attribute(dtype=str)
    def stats_io(self) -> str:
        """Get internal IO statistics - packets in and out

        returned value example

        .. code:: python

           {
               "fpga_uptime":    96,
               "total_pkts_in":  0,
               "spead_pkts_in":  0,
               "total_pkts_out": 0,
               "vis_pkts_out":   0
           }

        :return: JSON string
        """
        return json.dumps(self._stats_io)

    def update_io_stats(self, new_stats: dict) -> None:
        """
        Update attribute with latest IO statistics
        """
        assert isinstance(new_stats, dict), "IO statistics should be dict"
        self._stats_io = new_stats
        self.push_change_event("stats_io", json.dumps(new_stats))

    def _read_processor_attribute(self, attr):
        """
        Get value from list and update
        """
        try:
            name = attr.get_name()
        except AttributeError as error:
            self.logger.error(f"attribute error {error}")
            return

        if (
            name not in self._processor_state
            or self._processor_state[name] is None
        ):
            return
        (value, read_time) = self._processor_state[name]
        self.logger.debug(f"updating {name} with: {value} t:{read_time}")
        attr.set_value_date_quality(value, read_time, self._attribute_quality)

    def _write_processor_attribute(self, attr):
        """
        Write a value to the FPGA
        """
        raise ValueError("Use DebugRegWrite to modify registers")

    @attribute(dtype=("DevUShort",), max_dim_x=64)
    def subarrayIds(self):
        """
        Subarrays handled by this processor.

        :return: an array of subarrays (short integers)
        """
        return self._subarrayIds

    @attribute(dtype=((int,),), max_dim_y=MAX_SUBARRAYS, max_dim_x=MAX_BEAMS)
    def beamIds(self) -> np.array((MAX_SUBARRAYS, MAX_BEAMS), dtype=int):
        """
        Return subarray beams handled by this processor.

        :return: a 2D array of beam IDs per subarray
        """
        return self._subarray_beams

    def _update_subarray_IDs(self, delay_stats):
        """Update the lists of subarrays and beams.
        Notify subscribers in case of any change.
        """
        parent_subarrays = []
        beams = np.zeros((MAX_SUBARRAYS, MAX_BEAMS), dtype=int)
        for entry in delay_stats:
            if "subarray_id" not in entry:
                continue
            subarray_id = entry["subarray_id"]
            parent_subarrays.append(subarray_id)

            if subarray_id < 1 or subarray_id > MAX_SUBARRAYS:
                self.logger.error(
                    f"subarray {subarray_id} is out of range [1, {MAX_SUBARRAYS}]"
                )
                continue
            if "beams" not in entry:
                continue
            # collect this subarray's beams
            for i, bm in enumerate(entry["beams"]):
                if i < MAX_BEAMS:
                    beams[subarray_id - 1][i] = bm["beam_id"]
                else:
                    self.logger.error(
                        f"MAX_BEAMS ({MAX_BEAMS}) number exceeded: {i}"
                    )

        parent_subarrays = sorted(parent_subarrays)
        if (
            self._subarrayIds != parent_subarrays
        ):  # pylint: disable=access-member-before-definition
            self.logger.info("SUBARRAYS CHANGED")
            self._subarrayIds = parent_subarrays
            self.push_change_event("subarrayIds", parent_subarrays)
            self.push_archive_event("subarrayIds", parent_subarrays)

        if not np.array_equal(
            self._subarray_beams, beams
        ):  # pylint: disable=access-member-before-definition
            self.logger.info("BEAMS CHANGED")
            self._subarray_beams = beams
            self.push_change_event("beamIds", beams)
            self.push_archive_event("beamIds", beams)

    # NOTE: the 'label' property can/will be used by Taranta
    @attribute(
        dtype=HealthState,
        doc="hardware health",
        label="Health category - hardware",
    )
    @overridable
    def health_hardware(self) -> HealthState:
        "Summary of hardware health components"
        return self._health_hardware

    @attribute(
        dtype=HealthState,
        doc="functional health",
        label="Health category - functional",
    )
    @overridable
    def health_function(self) -> HealthState:
        "Summary of 'functional' health components"
        return self.fn_health_mon.health

    @attribute(
        dtype=HealthState,
        doc="processing health",
        label="Health category - processing",
    )
    @overridable
    def health_process(self) -> HealthState:
        "Summary of 'process' health components"
        return self.proc_health_mon.health

    @attribute(dtype=bool, label="FPGA firmware loaded")
    def function_firmware_loaded(self) -> bool:
        """Is FPGA firmware (personality) loaded

        :return: True if firmware is loaded, False otherwise
        """
        return self.is_fw_loaded

    # We override ska_tango_base testMode attribute to flush our value overrides
    # when leaving test mode.
    @SKABaseDevice.testMode.write
    def testMode(self: SKABaseDevice, value: TestMode) -> None:
        """
        Set device Test Mode.

        Resets our test mode override values when leaving test mode.
        """
        if value == TestMode.NONE:
            overrides_being_removed = list(self._attr_overrides.keys())
            self._attr_overrides = {}
            self._push_events_overrides_removed(overrides_being_removed)
            # re-calculate health
            self.hw_attr_update(self._last_hwvalue)
            self.fn_health_mon.attr_update()
            self.proc_health_mon.override_change()
            self.summarise_health_state()

        self._test_mode = value

    def _push_events_overrides_removed(
        self, attrs_to_refresh: Iterable[str]
    ) -> None:
        """
        Push true value events for attributes that were previously overridden.

        :param attrs_to_refresh: Names of our attributes that are no longer overridden
        """
        for attr_name in attrs_to_refresh:
            # Read configuration of attribute
            attr_cfg = self.get_device_attr().get_attr_by_name(attr_name)
            manual_event = (
                attr_cfg.is_change_event() or attr_cfg.is_archive_event()
            )

            if not manual_event:
                continue

            # Read current state of attribute
            attr = AttributeProxy(f"{self.get_name()}/{attr_name}").read()
            # when attr.AttrQuality is INVALID the attr.value is None
            # can throw a data type exception if not tested for None
            if attr_cfg.is_change_event() and attr.value is not None:
                self.push_change_event(attr_name, attr.value)
                # FIXME - replace above call with the below
                #  - needs latest ska-tango-base
                # push_change_event(attr_name, attr.value, attr.time, attr.quality)
            # when attr.AttrQuality is INVALID the attr.value is None
            # can throw a data type exception if not tested for None
            if attr_cfg.is_archive_event() and attr.value is not None:
                self.push_archive_event(attr_name, attr.value)
                # FIXME - replace above call with the below
                #  - needs latest ska-tango-base
                # push_archive_event(attr_name, attr.value, attr.time, attr.quality)

    @attribute(
        dtype=str,
        doc="Attribute value overrides (JSON dict)",
    )
    def test_mode_overrides(self: SKABaseDevice) -> str:
        """
        Read the current override configuration.

        :return: JSON-encoded dictionary (attribute name: value)
        """
        return json.dumps(self._attr_overrides)

    # TODO @test_mode_overrides.is_allowed looks nice, but doesn't work
    #  - maybe needs newer pytango?
    def is_test_mode_overrides_allowed(self, request_type: AttReqType) -> bool:
        """
        Control access to test_mode_overrides attribute.

        Writes to the attribute are allowed only if test mode is active.
        """
        if request_type == AttReqType.READ_REQ:
            return True
        return self._test_mode == TestMode.TEST

    @test_mode_overrides.write  # type: ignore[no-redef]
    def test_mode_overrides(self: SKABaseDevice, value_str: str) -> None:
        """
        Write new override configuration.

        :param value_str: JSON-encoded dict of overrides (attribute name: value)
        """
        value_dict = json.loads(value_str)
        assert isinstance(value_dict, dict), "expected JSON-encoded dict"
        overrides_being_removed = (
            self._attr_overrides.keys() - value_dict.keys()
        )
        self._attr_overrides = value_dict
        self._push_events_overrides_removed(overrides_being_removed)

        for attr_name, value in value_dict.items():
            value = _override_value_convert(attr_name, value)
            # it seems OK to call push_ even if we haven't set_<type>_event ¯\_(ツ)_/¯
            self.push_change_event(attr_name, value)
            self.push_archive_event(attr_name, value)

        # re-run last health update so overrides take effect immediately
        self.hw_attr_update(self._last_hwvalue)
        self.fn_health_mon.attr_update()
        self.proc_health_mon.override_change()
        self.summarise_health_state()

    def _read_driver_ok(self, attr: tango.Attribute):
        "Is XRT driver functional (quality dependant on whether FW is loaded)"
        (
            value,
            mod_time,
            qual,
        ) = self.fn_health_mon.function_driver_ok.value_date_quality
        attr.set_value_date_quality(value, mod_time, qual)

    def _read_proc_health_attr(self, attr: tango.Attribute):
        """Read process_delays_subscription_ok and process_delay_poly_valid attributes;
        AttrQuality gets a refresh too.
        """
        name = attr.get_name()
        delay_attr = getattr(self.proc_health_mon, name)
        value, mod_time, qual = delay_attr.value_date_quality
        attr.set_value_date_quality(value, mod_time, qual)

    @property
    def is_fw_loaded(self) -> bool:
        "Is FPGA firmware loaded? used by device server and its components"
        return self._attr_overrides.get(ATTR_FW_LOADED, self._is_fw_loaded)

    def get_override(self, attr_name: str, default=None):
        """
        Read a value from our overrides, use a default value when not overridden.

        Used where we use possibly-overridden internal values within the device server
        (i.e. reading member variables, not via the Tango attribute read mechanism).

        e.g.
        ``my_thing = self.get_override("thing", self._my_thing_true_value)``
        """
        if (
            self._test_mode != TestMode.TEST
            or attr_name not in self._attr_overrides
        ):
            return default
        return _override_value_convert(
            attr_name, self._attr_overrides[attr_name]
        )

    # ----------
    # Callbacks
    # ----------
    def _communication_state_changed(self, communication_state):
        if communication_state == CommunicationStatus.ESTABLISHED:
            self._attribute_quality = AttrQuality.ATTR_VALID
        else:
            self._attribute_quality = AttrQuality.ATTR_INVALID
        super()._communication_state_changed(communication_state)

    def _component_definition_changed(self, definition):
        # get a list of known attributes (they may not have been initialised)
        my_attributes = self.get_polled_attr()
        for attribute_name in self._processor_state:
            if attribute_name in my_attributes:
                self.remove_attribute(attribute_name)
        self._processor_state.clear()

        for attribute_name, attribute_data in definition.items():
            self._processor_state[attribute_name] = None
            access = (
                tango.AttrWriteType.READ_WRITE
                if attribute_data["write"]
                else tango.AttrWriteType.READ
            )
            data_type = (
                (attribute_data["type"],)
                if attribute_data["length"] > 1
                else attribute_data["type"]
            )

            attr = tango.server.attribute(
                name=attribute_name,
                dtype=data_type,
                access=access,
                label=attribute_name,
                max_dim_x=attribute_data["length"],
                fread="_read_processor_attribute",
                fwrite="_write_processor_attribute",
            ).to_attr()
            attr.set_archive_event(True, False)

            # TODO: Creating hundreds of attributes at once is killing our
            # throughput. It is common to encounter "failed to obtain monitor
            # lock" DevFailed here. We might need to re-think this.
            attempts = 3
            for attempt in range(1, attempts + 1):
                try:
                    self.add_attribute(
                        attr,
                        self._read_processor_attribute,
                        self._write_processor_attribute,
                        None,
                    )
                    self.set_change_event(attribute_name, True, False)
                    break
                except tango.DevFailed as fail:
                    self.logger.error(
                        f"Failed to add {attribute_name}: {repr(fail)}"
                    )
                    time.sleep(1)
                    if attempt == attempts:
                        raise
        if self._personality != self.component_manager.personality:
            self.logger.info("PERSONALITY CHANGED")
            self._personality = self.component_manager.personality
            # self._health_registers = fpga_reg_checks[self._personality.lower()]
            # self._health_reg_loaded = False

    def _component_state_changed(
        self, fault=None, power=None, poll_time=None, processor_state=None
    ):
        super()._component_state_changed(fault=fault, power=power)

        if not processor_state:
            return

        for name, value in processor_state.items():
            changed = (
                self._processor_state[name] is None
                or self._processor_state[name][0] != value
            )
            val = processor_state[name]
            self._processor_state[name] = (val, poll_time)
            if changed:
                self.push_change_event(name, val)
                self.push_archive_event(name, val)
        # self._check_health(processor_state)

    def summarise_health_state(self):
        """Recalculate overall processor health based on three health categories"""
        # this could be called from multiple threads, including a Tango client
        # serialise access to it
        with self._lock:
            health = max(
                self.get_override("health_hardware", self._health_hardware),
                self.get_override(
                    "health_function", self.fn_health_mon.health
                ),
                self.get_override(
                    "health_process", self.proc_health_mon.health
                ),
            )
            if health != self._health_state:
                self._update_health_state(health)


# ----------
# Run server
# ----------
def main(args=None, **kwargs):
    """Main function of the LowCbfProcessor module."""
    return tango.server.run((LowCbfProcessor,), args=args, **kwargs)


if __name__ == "__main__":
    main()

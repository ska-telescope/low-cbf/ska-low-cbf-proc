ska-low-cbf-proc
================

The ska-low-cbf-proc repository contains code for two Tango devices.
The Alveo device is used to monitor the operational parameters of the Alveo card, such as power, temperature, voltages, etc.
The Processor device is used to load a FPGA image onto the Alveo card, and to interact with the registers in the FPGA design that control its operation.
Each Alveo card can have one of each of the Tango devices attached.


.. toctree::
    :maxdepth: 2
    :caption: Contents:

.. toctree::
    :maxdepth: 2
    :caption: FPGA Tango Device

    low_cbf_processor


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

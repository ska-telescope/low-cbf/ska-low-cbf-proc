LowCbfProcessor
###############

**LowCbfProcessor** is a Tango device server for monitoring and control of registers in the Low.CBF signal processing FPGAs.
Processors are shared by and may perform correlation or beamforming for multiple subarrays simultaneously.

The processor device uses the
`ska-low-cbf-fpga <https://developer.skao.int/projects/ska-low-cbf-fpga/>`__
python package to represent registers within the FPGA design as "fields"
and groupings of fields ("peripherals") it can interact with.
The ska-low-cbf-fpga package is a general framework for interacting with registers in Low.CBF FPGA designs.
Registers within a FPGA are read from the address map file associated with each FPGA design (fpgamap_NNNNNNNN.py).

The processor device repository contains code to control and monitor two specific FPGA design personalities
    - Low.CBF PST Beamformer FPGAs
    - Low.CBF correlator FPGAs
    - Low.CBF PSS Beamformer FPGAs

Processor communication with Allocator
--------------------------------------

Processor Tango devices subscribe to Allocator events that represent the desired state of the processor device.
On receipt of an event, a processor device will attempt to conform itself to the desired state.
It will translate the attribute parameters into appropriate register values for the FPGA design personality.
There are two attributes to which a Processor subscribes:

    - "internal_alveo" conveys per-Alveo data
    - "internal_subarray" conveys per-subarray settings that are used globally by Alveos

The Allocator "internal_alveo" attribute conveys information about the internal register settings of every Alveo.
The attribute data is a JSON-encoded dictionary, using Alveo serial numbers as the key.
If an Alveo card's serial number is not present then the Alveo is not currently in use by any subarray.
Each Alveo uses only the data listed under its own ID. The data includes the FPGA personality the Alveo should run and an abbreviated
description of its register settings.

The Allocator's "internal_subarray" attribute conveys information about the currently operating subarrays, including delay polynomial sources.
The attribute data is a JSON-encoded dictionary using Subarray ID [1-16] as key. If a subarray's key is not present the the subarray is not in use.
Values for each subarray provide information about subarray stations, beams, frequencies, and destinations for the Alveo's output products.
The information is potentially used by, and common to, every Alveo.

Delay polynomials
-----------------

The processor subscribes to tango attributes that provide station-beam delay polynomials (currently a 5th order) for each station contributing to a station beam.
It examines time in the incoming SPS packets and chooses polynomials with appropriate start-of-validity to use for delay calculations from an internal queue of polynomias it received
The "stats_delay" attribute provides information about delay polynomials in the FPGA registers and whether the polynomials are valid (ie not being used before start-of-validity and not being used after end-of-validity).


Design
------

Processors load a FPGA executable for the particular Personality (correlator, beamformer, etc) they are to run.
The executable is downloaded from SKA CAR or Gitlab. Since the download takes longer time than a Tango command is allowed to execute,
firmware download and load is handled by a separate thread, allowing the Tango thread to return before Tango times it out.
Because FPGA personality can change at any time, code is designed to receive requests to download a new personality any time, even while a download is in progress.

Register values in an Alveo are only updated when any subarray that the Processor is handling changes between scanning and not-scanning states
and when incoming packets contain time that matches more recent delay polynomial validity.
Scanning state is derived from "internal_subarray" events, and any event that indicates scanning state has changed cause registers to be reprogrammed immediately.

Data from "internal_alveo" events however is simply recorded for use later, i.e. when scanning state changes. The SKA observing state machine ensures that a subarray receives
configuration (affecting "internal_alveo" data) before it begins to scan,
so the last recorded version of "internal_alveo" event data always contains the required Alveo register configuration to be applied when an "internal_subarray" event is received.

adminMode
---------

The processor's adminMode Tango attribute affects the operation of the processor device and FPGA in several ways:
    - The Processor FPGA will only produce output packets when adminMode is ONLINE or ENGINEERING
    - Processor output can be temporarily suspended by setting adminMode OFFLINE
    - Processor output can be resumed from the temporarily suspended state by setting adminMode back to ONLINE or ENGINEERING
        - ``adminMode`` will only accept a transition back to ONLINE or ENGINEERING state it was suspended from (but the Processor exits the suspended state if the subarray is de-configured while still suspended)

In all other respects processor adminMode behaves the same as the standard SKA adminMode state machine. The processor code uses the SKA adminMode state machine base class,
but has added checks to implement the extra behaviour described above. The effect of changes is that the usual "offline" state has been split
into three sub-states shown in the diagram below. The substates ensure that adminMode only goes back to the state it was suspended from.

.. image:: ../diagrams/processor-adminmode.png
    :alt: state diagram for processor adminMode


Tango attribute/command list
----------------------------
.. include:: processor_device_attr_cmd.rst

Dynamically created Tango attibutes
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
These attributes are created for each Alveo on startup.

Functional Health
~~~~~~~~~~~~~~~~~

.. attribute:: LowCbfProcessor.health_function
               
   Indicates the device's functional health state.

   Its value is the worst case scenario of constituent attributes (``function_*`` below)
   
   Contributes towards ``healthState`` attribute.
   
   `See also health overview <https://developer.skao.int/projects/ska-low-cbf/en/latest/health.html#health-of-low-cbf-hardware-devices>`_

.. attribute:: LowCbfProcessor.function_firmware_loaded
               
   Indicates whether FPGA firmware is loaded into Alveo card.

   Contributes towards ``health_function`` attribute.

.. attribute:: LowCbfProcessor.function_driver_ok
               
   Indicates whether FPGA driver is communcating with Alveo card.

   Contributes towards ``health_function`` attribute.
   
Hardware Health
~~~~~~~~~~~~~~~

.. attribute:: LowCbfProcessor.health_hardware
               
   Indicates the device's hardware health state.

   Its value is the worst case scenario of constituent attributes (``hardware_*`` below)

   Contributes towards ``healthState`` attribute.
               
   `See also health overview <https://developer.skao.int/projects/ska-low-cbf/en/latest/health.html#health-of-low-cbf-hardware-devices>`_
 
.. attribute:: LowCbfProcessor.hardware_fpga_temperature

   Indicates if FPGA temperature is within operating limits.

.. attribute:: LowCbfProcessor.hardware_fpga_power

   Indicates if FPGA power consumption is within limits.

.. attribute:: LowCbfProcessor.hardware_hbm_temperature

   Indicates if HBM (high bandwidth memory) temperature is within operating limits.

.. attribute:: LowCbfProcessor.hardware_power_supply_12v_voltage

   Indicates if 12 V power rail voltage is within operating limits.

.. attribute:: LowCbfProcessor.hardware_power_supply_12v_current

   Indicates if 12 V power rail current is within operating limits.

.. attribute:: LowCbfProcessor.hardware_pcie_12v_voltage

   Indicates if PCIe bus 12 V power rail voltage is within operating limits.

.. attribute:: LowCbfProcessor.hardware_pcie_12v_current

   Indicates if PCIe bus 12 V power rail current is within operating limits.
   
Process Health
~~~~~~~~~~~~~~
.. attribute:: LowCbfProcessor.health_process

   Indicates the device's process health state.
   
   Its value is the worst case scenario of constituent attributes (``process_*`` below)

   Contributes towards ``healthState`` attribute.

   
   `See also health overview <https://developer.skao.int/projects/ska-low-cbf/en/latest/health.html#health-of-low-cbf-hardware-devices>`_
 
.. attribute:: LowCbfProcessor.process_delay_subscription_ok
               
   Indicates correctness of arrival of delay polynomials
   
.. attribute:: LowCbfProcessor.process_delay_poly_valid
               
   Indicates correctness of delay polynomial values

   Contributes towards ``health_process`` attribute.
   

.. attribute:: LowCbfProcessor.process_spead_packets_ok
               
   Indicates SPS SPEAD packets are arriving at FPGA.
   
   Contributes towards ``health_process`` attribute.
   

Test Mode
---------------

If Test mode is active, the value of some attributes can be temporarily changed to any desired value for testing.

Continuous Integration
----------------------

The current CI tests of this device use DeviceTestContext and do not require a full Tango system.

Environment Variables
---------------------

Some runtime behaviours can be configured through environment variables.

- ``INITIAL_ADMINMODE`` variable can be used to select the default value of ``adminMode`` Tango attribute on startup.
- ``ALLOW_ADMIN_CHANGE_WHILE_USED`` allows ``adminMode`` to be altered at any time if set to true.
  If false (the default), ``adminMode`` is unable to be changed unless the FPGA is not in use by any subarray.
- ``FPGA_XRT_TIMEOUT`` allows the timeout (milliseconds) for each FPGA register read or write to be extended
  (default 5 if not set)
- ``FPGA_POST_FW_LOAD_DELAY`` specifies time in seconds after firmware load before register reads will time out.
  (Allows for busy server CPU after loading a FPGA)
- ``CACHE_DIR`` allows firmware to be cached on-server, saving download bandwidth. Our
  Helm chart sets this to /app, meaning downloads will be cached in the pod's ephemeral
  storage. For best results, override this to something persistent & shared between
  pods (e.g. a volume mount on the FPGA host server).
- ``STN_DELAY_SIGN`` can be set "pos" or "neg" to change to allow station delay
  polynomials to be applied with positive or negative sign
- ``PST_DELAY_SIGN`` can be set "pos" or "neg" to change to allow PST beam delay
  polynomials to be applied with positive or negative sign
- ``PSS_DELAY_SIGN`` can be set "pos" or "neg" to change to allow PSS beam delay
  polynomials to be applied with positive or negative sign
- ``PST_DELAY_FORMAT`` can be set "diff" or "full" to describe whether PST delay polynomials
  are supplied as differences from station beam, or as full delay polynomials independent
  of any reference to the station beam. In the latter case the cadence of station beam
  and PST beam polynomials must be identical.
- ``PSS_DELAY_FORMAT`` can be set "diff" or "full" to describe whether PSS delay polynomials
  are supplied as differences from station beam, or as full delay polynomials independent
  of any reference to the station beam. In the latter case the cadence of station beam
  and PSS beam polynomials must be identical.
  ``ISOLATED`` allows processor to run without an allocator connection (software testing)
  ``CLEAR_PST_DELAYS_BEFORE_SCANS`` When defined (with any value), ensures that PST
  delay polynomials from the prior scan are NOT used as the initial delay polymomials for
  the next scan, even if they are still valid. This may be helpful if a Low.CBF subarray
  is configured then scanned multiple times, but between scans the beam pointing directions 
  are changed by manipulating the delay polynomials provided externally to Low.CBF.
  Note that delay polynomials are always cleared for the first scan after a subarray has
  been reconfigured, and this setting does not override that behaviour.

An example of the Helm chart keys that parent charts would need to override to achieve
correct processor operation is given in the test-parent chart's values.yaml file:

.. literalinclude:: ../../charts/test-parent/values.yaml
    :start-at: # Helm overrides for
    :end-before: # end of PSI-low

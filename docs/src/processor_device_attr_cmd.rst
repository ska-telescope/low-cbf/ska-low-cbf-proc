Processor device Tango attributes
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. automethod:: processor.processor_device::LowCbfProcessor.adminMode
    :noindex:
.. automethod:: processor.processor_device::LowCbfProcessor.beamIds
    :noindex:
.. automethod:: processor.processor_device::LowCbfProcessor.function_firmware_loaded
    :noindex:
.. automethod:: processor.processor_device::LowCbfProcessor.serialNumber
    :noindex:
.. automethod:: processor.processor_device::LowCbfProcessor.simulationMode
    :noindex:

    INTERNAL use only

.. automethod:: processor.processor_device::LowCbfProcessor.stationDelayValid
    :noindex:
.. automethod:: processor.processor_device::LowCbfProcessor.stats_delay
    :noindex:
.. automethod:: processor.processor_device::LowCbfProcessor.stats_io
    :noindex:
.. automethod:: processor.processor_device::LowCbfProcessor.stats_mode
    :noindex:
.. automethod:: processor.processor_device::LowCbfProcessor.subarrayDelaysSummary
    :noindex:
.. automethod:: processor.processor_device::LowCbfProcessor.subarrayIds
    :noindex:
.. automethod:: processor.processor_device::LowCbfProcessor.test_mode_overrides
    :noindex:

    INTERNAL use only


Processor device Tango commands
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. automethod:: processor.processor_device::LowCbfProcessor.DebugDumpHbm 
    :noindex:

    INTERNAL use only

.. automethod:: processor.processor_device::LowCbfProcessor.DebugRegRead 
    :noindex:
       
    INTERNAL use only

.. automethod:: processor.processor_device::LowCbfProcessor.DebugRegWrite 
    :noindex:

    INTERNAL use only

.. automethod:: processor.processor_device::LowCbfProcessor.DebugSetMemConfig 
    :noindex:

    INTERNAL use only

.. automethod:: processor.processor_device::LowCbfProcessor.Register 
    :noindex:

    INTERNAL use only

.. automethod:: processor.processor_device::LowCbfProcessor.StartRegisterLog 
    :noindex:

    INTERNAL use only

.. automethod:: processor.processor_device::LowCbfProcessor.StopRegisterLog 
    :noindex:

    INTERNAL use only

.. automethod:: processor.processor_device::LowCbfProcessor.SubscribeToAllocator 
    :noindex:

    INTERNAL use only

.. automethod:: processor.processor_device::LowCbfProcessor.gethwdata 
    :noindex:

    INTERNAL use only


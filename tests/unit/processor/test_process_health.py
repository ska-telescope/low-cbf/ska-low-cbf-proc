# -*- coding: utf-8 -*-
#
# (c) 2024 CSIRO Space and Astronomy.
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement
# See LICENSE.txt for more info.

"""
Test ProcessingHealth class
"""

import json
import logging
from time import sleep

import pytest
from ska_control_model import HealthState
from tango import AttrQuality

from ska_low_cbf_proc.processor.process_health import (
    ATTR_DELAY_SUBSCR_OK,
    ATTR_DELAY_VALID,
    ATTR_SPEAD_PACKETS,
    DEFAULT_PERIOD_SEC,
    ProcessingHealth,
)

PROC_HEALTH_CONFIG_FILE = "u55c_proc_mon.yaml"

# allow for two polling periods (plus a bit)  when checking SPS SPEAD counter
TWO_POLL_PERIODS = DEFAULT_PERIOD_SEC * 2 + 0.5


class LowCbfProcessorStub:
    """Processor stub"""

    def __init__(self, override=""):
        self.override = json.loads(override) if override else {}
        self.fw_loaded = False
        self.component_manager = ProcComponentMgrStub()
        # capture the details push_change_event() is publishing
        self.change_events = {}
        # capture the details push_archive_event() is publishing
        self.archive_events = {}

    def push_change_event(self, name: str, value) -> None:
        """Stub to catch change events"""
        self.change_events[name] = value

    def push_archive_event(self, name: str, value) -> None:
        "Stub to catch archive events"
        self.archive_events[name] = value

    def summarise_health_state(self) -> None:
        """Combine health of monitored components"""

    def get_override(self, name: str):
        """Get override value"""
        return self.override.get(name, None)

    def is_fw_loaded(self) -> bool:
        "Is FPGA firmware loaded."
        return self.fw_loaded


class ProcComponentMgrStub:
    """ProcessorComponentManager stub"""

    def __init__(self):
        "constructor ..."
        self.fpga = {}
        self.counter = 0

    def read_peripheral_register(self, periph: str, reg: str):
        "Read FPGA register"
        if peripheral := self.fpga.get(periph):
            if register := peripheral.get(reg):
                value = register[self.counter]
                self.counter += 1
                return value


# logger passed to ProcessingHealth ctor
logger = logging.Logger("Unit test logger")

SUBARRAY_ID = 4
JSON_STR_SCANNING = '{ "%d": { "scanning": true  } }' % SUBARRAY_ID
JSON_STR_NOT_SCANNING = '{ "%d": { "scanning": false } }' % SUBARRAY_ID

delay_attrs = (ATTR_DELAY_VALID, ATTR_DELAY_SUBSCR_OK)


@pytest.fixture
def process_health():
    """instance of ProcessingHealth with LowCbfProcessor stub"""
    return ProcessingHealth(
        PROC_HEALTH_CONFIG_FILE, LowCbfProcessorStub(), logger
    )


def bool_to_str(val: bool) -> str:
    "Covert bool to a lower case string (needed by JSON)"
    return str(val).lower()


def test_initial_values(process_health):
    "Check values after initialisation."
    assert process_health.health == HealthState.OK
    assert process_health.subarr_scanning == {}
    for name in delay_attrs:
        attr = getattr(process_health, name)
        assert attr.quality == AttrQuality.ATTR_INVALID
        assert attr.is_override is False
        assert attr.health == HealthState.OK


def test_subarray_scanning(process_health):
    "Test subarray SCANNING/NOT_SCANNING processing"
    process_health.subarray_event(JSON_STR_SCANNING)
    assert process_health.subarr_scanning == {f"{SUBARRAY_ID}": True}

    process_health.subarray_event(JSON_STR_NOT_SCANNING)
    assert process_health.subarr_scanning == {f"{SUBARRAY_ID}": False}

    process_health.subarray_event("{}")
    assert process_health.subarr_scanning == {}


@pytest.mark.parametrize(
    "stn_valid, pst_valid", [(False, True), (True, False), (False, False)]
)
def test_delay_poly_valid(process_health, stn_valid: bool, pst_valid: bool):
    "Check invalid station/pst delay polynomials are handled correctly"
    poly_values = """[
      {
        "subarray_id": %d,
        "beams": [
          {
            "beam_id": 1,
            "valid_delay": %s,
            "subscription_valid": true,
            "pst": [
              {
                "name": "low-cbf/delaypoly/0/pst_s04_b01_1",
                "valid_delay": %s,
                "subscription_valid": true
              },
             ]
          }
        ]
      }
    ]"""

    json_str = poly_values % (
        SUBARRAY_ID,
        bool_to_str(stn_valid),
        bool_to_str(pst_valid),
    )
    process_health.attr_update(json_str)
    expected = stn_valid and pst_valid
    attr = getattr(process_health, ATTR_DELAY_VALID)
    assert attr.value == expected
    # health == OK because not scanning yet:
    assert attr.health == HealthState.OK
    assert process_health.health == HealthState.OK

    process_health.subarray_event(JSON_STR_SCANNING)
    # health == FAILED as we are scanning now
    assert attr.health == HealthState.FAILED
    assert process_health.health == HealthState.FAILED
    # check subscribers are notified:
    assert process_health._proc_device.change_events[ATTR_DELAY_VALID] is False
    assert (
        process_health._proc_device.archive_events[ATTR_DELAY_VALID] is False
    )


@pytest.mark.parametrize(
    "stn_valid, pst_valid", [(False, True), (True, False), (False, False)]
)
def test_poly_subscription_valid(process_health, stn_valid, pst_valid):
    "Test various combinations of station an pst beam poly. subscription failures"
    poly_values = """[
      {
        "subarray_id": %d,
        "beams": [
          {
            "beam_id": 1,
            "valid_delay": true,
            "subscription_valid": %s,
            "pst": [
              {
                "name": "low-cbf/delaypoly/0/pst_s04_b01_1",
                "valid_delay": true,
                "subscription_valid": %s
              },
             ]
          }
        ]
      }
    ]"""

    json_str = poly_values % (
        SUBARRAY_ID,
        bool_to_str(stn_valid),
        bool_to_str(pst_valid),
    )
    process_health.attr_update(json_str)
    expected = stn_valid and pst_valid
    attr = getattr(process_health, ATTR_DELAY_SUBSCR_OK)
    assert attr.value == expected

    # health == OK because not scanning yet:
    assert attr.health == HealthState.OK
    assert process_health.health == HealthState.OK

    process_health.subarray_event(JSON_STR_SCANNING)
    # health == FAILED as we are scanning now
    assert attr.health == HealthState.FAILED
    assert process_health.health == HealthState.FAILED
    # check subscribers are notified:
    assert (
        process_health._proc_device.change_events[ATTR_DELAY_SUBSCR_OK]
        is False
    )
    assert (
        process_health._proc_device.archive_events[ATTR_DELAY_SUBSCR_OK]
        is False
    )


def test_delay_ok_override(process_health):
    "Check override of process_delay_valid works as expected"

    process_health.subarray_event(JSON_STR_SCANNING)
    attr = getattr(process_health, ATTR_DELAY_VALID)
    for i in (False, True):
        process_health._proc_device.override = {ATTR_DELAY_VALID: i}
        # signal we have an override change
        process_health.override_change()
        assert attr.value is i
        assert attr.health == HealthState.OK if i else HealthState.FAILED


def test_subscription_override(process_health):
    "Check override of process_delay_subscription_ok works as expected"

    process_health.subarray_event(JSON_STR_SCANNING)
    attr = getattr(process_health, ATTR_DELAY_SUBSCR_OK)
    for i in (False, True):
        process_health._proc_device.override = {ATTR_DELAY_SUBSCR_OK: i}
        # signal we have an override change
        process_health.override_change()
        assert attr.value is i
        assert attr.health == HealthState.OK if i else HealthState.FAILED


def test_spead_packet_override(process_health):
    "Check override of process_spead_packets_ok works as expected"
    process_health.subarray_event(JSON_STR_SCANNING)
    attr = getattr(process_health, ATTR_SPEAD_PACKETS)
    for i in (False, True):
        process_health._proc_device.override = {ATTR_SPEAD_PACKETS: i}
        # signal we have an override:
        process_health.override_change()
        assert attr.value is i
        assert attr.health == HealthState.OK if i else HealthState.FAILED


def test_spead_counter_incrementing(process_health):
    "Test monitoring of SPS SPEAD packets when the counter icreases."
    attr = getattr(process_health, ATTR_SPEAD_PACKETS)

    assert attr.quality == AttrQuality.ATTR_INVALID
    # set subarray to scanning to enable updates
    process_health.subarray_event(JSON_STR_SCANNING)
    assert attr.quality == AttrQuality.ATTR_VALID

    fpga_regs_inc = {"lfaadecode100g": {"spead_packet_count": list(range(10))}}
    process_health._proc_device.component_manager.fpga = fpga_regs_inc
    # need FW loaded for the polling loop to work
    assert not process_health._proc_device.is_fw_loaded()
    process_health._proc_device.fw_loaded = True
    assert process_health._proc_device.is_fw_loaded()

    assert attr.value is False
    sleep(TWO_POLL_PERIODS)  # wait for consecutive register updates
    assert attr.value is True
    assert attr.health == HealthState.OK
    # check subscribers are notified:
    assert (
        process_health._proc_device.change_events[ATTR_SPEAD_PACKETS] is True
    )
    assert (
        process_health._proc_device.archive_events[ATTR_SPEAD_PACKETS] is True
    )


def test_spead_counter_stuck(process_health):
    "Test monitoring of SPS SPEAD packets when the counter doesn't increase."
    attr = getattr(process_health, ATTR_SPEAD_PACKETS)

    assert attr.quality == AttrQuality.ATTR_INVALID
    # set subarray to scanning to enable updates
    process_health.subarray_event(JSON_STR_SCANNING)
    assert attr.quality == AttrQuality.ATTR_VALID

    # simulate FPGA where register does not increment
    fpga_regs_stuck = {"lfaadecode100g": {"spead_packet_count": [42] * 10}}
    process_health._proc_device.component_manager.fpga = fpga_regs_stuck
    # need FW loaded for the polling loop to work
    assert not process_health._proc_device.is_fw_loaded()
    process_health._proc_device.fw_loaded = True
    assert process_health._proc_device.is_fw_loaded()

    assert attr.value is False  # initial condition
    sleep(TWO_POLL_PERIODS)  # wait for consecutive register reads
    # incrementing == False:
    assert attr.value is False
    assert attr.health == HealthState.FAILED
    # check subscribers are notified:
    assert (
        process_health._proc_device.change_events[ATTR_SPEAD_PACKETS] is False
    )
    assert (
        process_health._proc_device.archive_events[ATTR_SPEAD_PACKETS] is False
    )

# -*- coding: utf-8 -*-
#
# (c) 2024 CSIRO Space and Astronomy.
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement
# See LICENSE.txt for more info.

"""
Test FirmwareRequest class
"""
import pytest

from ska_low_cbf_proc.processor.processor_component_manager import (
    FirmwareRequest,
)


@pytest.mark.parametrize("string", ["vis", "vis:1.2.3", "vis:1.2.3:gitlab"])
def test_from_str(string):
    FirmwareRequest.from_str(string)


def test_name_translate():
    # vis is an 'allocator' name, should be translated
    fr = FirmwareRequest.from_str("vis")
    assert fr.personality == "corr"
    # should default to CAR
    assert fr.source == "nexus"


def test_name_version():
    fr = FirmwareRequest.from_str("pst:1.2.3")
    assert fr.personality == "pst"
    assert fr.version == "1.2.3"
    assert fr.source == "nexus"


def test_name_version_source():
    fr = FirmwareRequest.from_str("vis:1.2.3:gitlab")
    assert fr.personality == "corr"
    assert fr.version == "1.2.3"
    assert fr.source == "gitlab"


def test_dev_version():
    # dev version exists only in gitlab
    fr = FirmwareRequest.from_str("vis:6.7.8-dev.12345678")
    # FirmwareRequest doesn't check for "dev" or "main"
    # so firmware source should still default to nexus
    assert fr.source == "nexus"


def test_url_host():
    url = "http://somehost.com/fw_tarball.tar.gz"
    fr = FirmwareRequest.from_str(url)
    assert fr.source == url
    assert fr.version == url


def test_url_correlator_fw():
    url = "http://somehost.com:8080/correlator_tarball.tar.gz"
    fr = FirmwareRequest.from_str(url)
    assert fr.source == url
    assert fr.version == url
    assert fr.personality == "corr"


def test_url_pst_fw():
    url = "http://somehost.com:8080/pst_tarball.tar.gz"
    fr = FirmwareRequest.from_str(url)
    assert fr.source == url
    assert fr.version == url
    assert fr.personality == "pst"

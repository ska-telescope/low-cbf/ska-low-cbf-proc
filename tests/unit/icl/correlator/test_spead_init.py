# -*- coding: utf-8 -*-
#
# Copyright (c) 2022 CSIRO Space and Astronomy.
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more information.
"""Test the SDP SPEAD init packet generation."""
import tempfile

import dpkt
import spead2
import spead2.recv

from ska_low_cbf_proc.icl.correlator.spead_init import get_sdp_init
from ska_low_cbf_proc.icl.correlator.spead_sdp import (
    heap_counter,
    visibility_frequency,
)


def write_pcap(out_file, template):
    """Given a SPEAD payload, add UDP, IP, and Ethernet headers; write PCAP."""
    writer = dpkt.pcap.Writer(out_file, snaplen=9000)
    packet = dpkt.ethernet.Ethernet(
        src=b"\x01\x01\x01\x01\x01\x01", dst=b"\x02\x02\x02\x02\x02\x02"
    )
    packet.data = dpkt.ip.IP(
        src=b"\x03\x03\x03\x03",
        dst=b"\x04\x04\x04\x04",
        p=dpkt.ip.IP_PROTO_UDP,
        sum=0,
    )
    packet.data.data = dpkt.udp.UDP(
        dport=41000, sport=41000, sum=0, ulen=8 + len(template)
    )
    packet.data.data.data = template  # UDP Payload
    writer.writepkt(bytes(packet), 0)  # t=0 ok for this


def extract_heaps_from_pcap(pcap_filename: str):
    """Extract all heaps and associated metadata from init packets."""
    thread_pool = spead2.ThreadPool()
    stream = spead2.recv.Stream(thread_pool)
    del thread_pool
    stream.add_udp_pcap_file_reader(pcap_filename, filter="")
    item_group = spead2.ItemGroup()
    all_heaps = {}
    for heap in stream:
        # General information about heap
        if heap.is_start_of_stream():
            all_heaps[heap.cnt] = {}
        items = item_group.update(heap)
        for item in items.values():
            all_heaps[heap.cnt][f"{item.name}"] = {}
            all_heaps[heap.cnt][f"{item.name}"][
                "type_and_shape"
            ] = f"{item.dtype}{item.shape}"
            if item.name == "ScaID":
                all_heaps[heap.cnt][f"{item.name}"]["value"] = int(item.value)
            else:
                all_heaps[heap.cnt][f"{item.name}"]["value"] = item.value
    return all_heaps


configuration = {
    "fine_start": 0,
    "n_fine_integrate": 24,
    "frequency_channel": 9216,
    "nb_baseline": 6,
    "scan_id": 123,
    "subarray": 1,
    "beam": 3,
    "coarse_start": 64,
    "subarray_id": 1,
    "integration_period": 0.8493465600000001,
    "freq_bandwidth": 5425.347222222223,
    "resolution": 32,
    "zoom_id": 0,
}


def test_sdp_init():
    """Confirm that SDP init packet can be created & decoded again."""
    frequency_visibility_hz = visibility_frequency(
        configuration["coarse_start"],
        configuration["fine_start"],
        configuration["n_fine_integrate"],
        configuration["frequency_channel"],
    )
    heap_id = heap_counter(
        subarray=configuration["subarray"],
        beam=configuration["beam"],
        visibility_channel=configuration["frequency_channel"],
        integration_id=0,
    )

    init_values = {
        "Epoch": 0,
        "Chann": configuration["frequency_channel"],
        "Basel": configuration["nb_baseline"],
        "ScaID": configuration["scan_id"],
        "SrcID": ord("L"),  # L for SKA-Low telescope
        "Hardw": 0xABAB,  # unique hardware id,
        "BeaID": configuration["beam"],
        "Subar": configuration["subarray"],
        "Integ": configuration["integration_period"],
        "Frequ": configuration["freq_bandwidth"],
        "Resol": configuration["resolution"],
        "FreHz": int(frequency_visibility_hz),
        "ZoomI": configuration["zoom_id"],
        "Firmw": 0x00_00_05_00,  # packed firmware version
        "VisFl": 0xCAFEF00D,  # visibility flags (arbitrary value for test)
    }
    offsets, template = get_sdp_init(
        n_baselines=configuration["nb_baseline"],
        heap_id=heap_id,
        values=init_values,
    )
    # these offsets should be a multiple of 8, by virtue of SPEAD packet design
    assert offsets["FreHz"] % 8 == 0, "FreqHz offset wrong?"
    assert offsets["Chann"] % 8 == 0, "Chann offset wrong?"

    # make sure start/end offsets are in the same 64B chunk, as required by FPGA
    assert (offsets["heap_counter"] - 1) // 64 == (
        offsets["heap_counter"] - 4
    ) // 64, "Heap Counter spans two 64B addresses"
    assert (offsets["FreHz"] - 1) // 64 == (
        offsets["FreHz"] - 4
    ) // 64, "FreHz spans two 64B addresses"
    assert (offsets["Chann"] - 1) // 64 == (
        offsets["Chann"] - 4
    ) // 64, "Chann spans two 64B addresses"

    with tempfile.NamedTemporaryFile() as pcap_file:
        write_pcap(pcap_file, template)
        pcap_file.seek(0)
        heaps = extract_heaps_from_pcap(pcap_file.name)

    assert heap_id in heaps
    assert len(heaps) == 1
    for field, expected_value in init_values.items():
        type_ = heaps[heap_id][field]["type_and_shape"]
        value = heaps[heap_id][field]["value"]
        if type_.startswith("uint"):
            assert value == expected_value
        elif type_ == "|S1()":
            assert value == bytes([expected_value])

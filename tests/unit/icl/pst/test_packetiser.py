# -*- coding: utf-8 -*-
#
# Copyright (c) 2021 CSIRO Space and Astronomy.
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement
# See LICENSE.txt for more info.

import pytest
from ska_low_cbf_fpga.args_fpga import WORD_SIZE
from ska_low_cbf_fpga.args_map import ArgsFieldInfo

from ska_low_cbf_proc.icl.pst.packetiser import Packetiser

packetiser_fields = {
    "data": ArgsFieldInfo(address=0x1234, length=128),
    "control_vector": ArgsFieldInfo(address=0x2345, length=1),
}


class TestPacketiser:
    def test_init(self, mock_driver):
        Packetiser(mock_driver, packetiser_fields)

    @pytest.mark.parametrize(
        "attribute, offset", [("src_ip", 8), ("dst_ip", 9)]
    )
    def test_ip_addr(self, attribute, offset, mock_driver):
        packetiser = Packetiser(mock_driver, packetiser_fields)
        mock_driver._read_value = 0  # make field read zero value from driver
        ip = getattr(packetiser, attribute)
        assert ip.value == "0.0.0.0"
        setattr(packetiser, attribute, "12.34.56.78")
        assert (
            packetiser_fields["data"].address + offset * WORD_SIZE
        ) in mock_driver.write_address_log
        assert (12 << 24) + (34 << 16) + (
            56 << 8
        ) + 78 in mock_driver.write_value_log

    @pytest.mark.parametrize(
        "attr, value, state",
        [("on", Packetiser.CTRL_ON, "On"), ("off", 0, "Off")],
    )
    def test_state(self, mock_driver, attr, value, state):
        packetiser = Packetiser(mock_driver, packetiser_fields)
        mock_driver._read_value = 0  # start with zero
        assert (
            packetiser_fields["control_vector"].address
            not in mock_driver.write_address_log
        )

        # nasty syntax, but if attr is "on", this calls "packetiser.on()"
        getattr(packetiser, attr)()

        assert (
            packetiser_fields["control_vector"].address
            in mock_driver.write_address_log
        )
        # check that writing the value corresponds to the control vector
        # address
        assert (
            mock_driver.write_value_log[
                mock_driver.write_address_log.index(
                    packetiser_fields["control_vector"].address
                )
            ]
            == value
        )
        mock_driver._read_value = (
            value  # make field read off value from driver
        )
        assert packetiser.state.value == state

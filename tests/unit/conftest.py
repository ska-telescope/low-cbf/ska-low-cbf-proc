# -*- coding: utf-8 -*-
#
# Copyright (c) 2021 CSIRO Space and Astronomy.
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement
# See LICENSE.txt for more info.

import logging
from collections import defaultdict

import pytest
from ska_low_cbf_fpga.args_fpga import ARGS_MAGIC, ArgsFpgaDriver
from ska_low_cbf_fpga.fpga_icl import IclFpgaField


class MockDriver(ArgsFpgaDriver):
    def _setup(self, read_value=ARGS_MAGIC):
        self.read_log = []
        self.write_address_log = []
        self.write_value_log = []
        self._read_value = read_value
        self.load_firmware_called = False
        self.init_called = True
        self._registers = defaultdict(self._default_register_value)

    def _default_register_value(self):
        return self._read_value

    def _load_firmware(self):
        self.load_firmware_called = True

    def _init_buffers(self):
        "Abstract method implementation"
        pass

    def read(self, source, length=None):
        if isinstance(source, IclFpgaField):
            length = source.length
            source = source.address
        self.read_log.append(source)
        if length is None:
            length = 1
        if length == 1:
            return self._registers[source]
        else:
            return [
                self._registers[addr]
                for addr in range(source, source + length)
            ]

    def write(self, destination, value):
        if isinstance(destination, IclFpgaField) or (
            hasattr(destination, "address") and hasattr(destination, "length")
        ):
            # length = destination.length
            destination = destination.address
        self.write_address_log.append(destination)
        self.write_value_log.append(value)
        self._registers[destination] = value

    def read_memory(self, index: int):
        pass

    def write_memory(self, index: int, values, offset: int = 0):
        pass


@pytest.fixture()
def mock_driver():
    mock_driver = MockDriver()
    mock_driver._read_value = 0
    yield mock_driver


@pytest.fixture()
def logger():
    """
    Fixture that returns a default logger for tests.

    The logger will be set to DEBUG level, as befits testing.

    :return: a logger
    """
    debug_logger = logging.Logger("Test logger")
    debug_logger.setLevel(logging.DEBUG)
    return debug_logger
